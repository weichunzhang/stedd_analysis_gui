function [fitresult, gof] = ProfileFit(intensity, XData, Fit_model, const)

% Single or double Gaussian fit a pre-defined line profile on an image.
%      fitresult : a fit object representing the fit.
%      gof : structure with goodness-of fit info.
%       intensity: intensity vector
%       XData: x axis of the line profiles.

    [xData, yData] = prepareCurveData( XData, intensity );
    % The prepareCurveData function reshapes x and y to columns for use with the fit function.

    switch Fit_model
        case 1 % double Gaussian for 3D-STED1 

        ft = 'a1*exp(-(x-b1)^2/(2*c1*c1)) + d1*exp(-(x-b1)^2/(2*e1*e1)) +y0'; % fit type

        opts = fitoptions( 'Method', 'NonlinearLeastSquares' );
        %opts.Display = 'Off';
        opts.StartPoint = [max(intensity) 0 max(XData)*0.2 max(intensity)*0.1 max(XData)*0.5 0];
        opts.Lower = [0 -Inf 0 0 0 -Inf];
        opts.Upper = [max(intensity) Inf Inf max(intensity) Inf max(intensity)];

        [fitresult, gof] = fit(xData, yData, ft, opts);

        case 2 % double Gaussian for 3D-STED1 beam with fixed sigma

        c2 = const;    
        ft = 'a1*exp(-(x-b1)^2/(2*c1*c1)) + d1*exp(-(x-b1)^2/(2*e1*e1)) +y0';
        opts = fitoptions( 'Method', 'NonlinearLeastSquares' );
        %opts.Display = 'Off';
        opts.StartPoint = [max(intensity) 0 max(XData)*0.2 max(intensity)*0.1 c2 0];
        opts.Lower = [0 -Inf 0 0 c2 -Inf];
        opts.Upper = [max(intensity) Inf Inf max(intensity) c2 max(intensity)]; % Sigma is fixed at the value from fitting STED2.

        [fitresult, gof] = fit( xData, yData, ft, opts);

        case 3 % single gaussian

        ft = 'a1*exp(-(x-b1)^2/(2*c1*c1)) + y0';

        opts = fitoptions( 'Method', 'NonlinearLeastSquares' );
        %opts.Display = 'Off';
        opts.StartPoint = [max(intensity) 0 max(XData)*0.5 0]; % Initial values for fitting.
        opts.Lower = [0 -Inf 0 -Inf]; 
        opts.Upper = [max(intensity) Inf Inf max(intensity)]; 
        [fitresult, gof] = fit( xData, yData, ft, opts); 


%         case 4 % single gaussian for gaussian final intensity
% 
%         STED_final = 'a1*exp(-((x-b1)/c1)^2) + y0';
%         ft = STED_final;
%         opts = fitoptions( 'Method', 'NonlinearLeastSquares' );
%         %opts.Display = 'Off';
%         opts.StartPoint = [max(intensity) 0 0.01 0];
%         opts.Upper = [max(intensity) max(XData)*0.2 0.2 2];
%         opts.Lower = [max(intensity)*0.9 min(XData)*0.2 0.05 0];  
%         [fitresult, gof] = fit( xData, yData, ft, opts);
    end   

end