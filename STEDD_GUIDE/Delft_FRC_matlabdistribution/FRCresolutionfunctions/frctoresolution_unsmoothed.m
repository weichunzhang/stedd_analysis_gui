%FRCTORESOLUTION   Compute the resolution from a FRC curve
%
% SYNOPSIS:
%   [resolution resolution_high resolution_low] = binlocalizations(frc_in,size)
%   It should be [resolution resolution_high resolution_low] =
%   frctoresolution(frc_in,size)? W.Z.
% PARAMETERS:
%   size
%     Size in pixels of the half data images from which frc_in was obtained

% (C) Copyright 2012               Quantitative Imaging Group
%     All rights reserved          Faculty of Applied Physics
%                                  Delft University of Technology
%                                  Lorentzweg 1
%                                  2628 CJ Delft
%                                  The Netherlands
% Robert Nieuwenhuizen, Oct 2012

% Modified from frctoresolution.m, but frc_var is calculated from
% the unsmoothed raw FRC curve.

function varargout = frctoresolution_unsmoothed(frc_in,sz)

% Check the format of frc_in
if ~isa(frc_in,'numeric')
    error('The FRC curve does not have the right data format.')
end
if ~isvector(frc_in)
    error('The FRC curve has the wrong dimensionality')
end
if isrow(frc_in)
    frc_in = frc_in';
end
    % Check the format of sz
if ~isa(sz,'numeric')
    error('The specified image size does not have the right data format.')
end
if ~isscalar(sz)
    if numel(sz)~= 2
        error('The specified image size does not have the right data format.');
    else
        sz = max(sz);
    end
end
if (sz ~= round(sz)) || ~(sz > 0) 
    error('The image size must be a positive integer.')
end

% Check that the curvefit toolbox function smooth exists
TB_curve=0;
try 
    TB_d=toolboxdir('curvefit');
    TB_curve=1;
catch
    warning('Curvefit toolbox not available. Using another not optimal smoothing method for FRC.')
end

%% Check for undesirable values in FRC curve
frc_in = real(frc_in);
% frc_in(isnan(frc_in)) = 0;
frc_in(frc_in>1) = 1;
frc_in(frc_in<-1) = -1;

%% Calculate the number of pixels in a Fourier ring
% Number of pixels in a Fourier ring
nr = double(radialsum(ones(sz)))';       

if length(nr) ~= length(frc_in)
   error('The specified image size could not have resulted in this FRC curve.'); 
end

% Calculate the effective width of pixels in a Fourier ring
if nargout >1
    n1 = find(frc_in<0, 1,'first' );
    frc_tmp = frc_in;
    frc_tmp(isnan(frc_tmp)) = 0;
    frc_tmp(isinf(frc_tmp)) = 0;
    ne_inv = mean(frc_tmp(n1:end).^2.*nr(n1:end));
end

%% Smoot the FRC curve 
% Least squares interpolation for curve smoothing
% sz the image size (length or width) in pixels. W.Z.
sspan = ceil(sz/20);      % Smoothing span
if (sz/20)<5
    sspan = 5;
end
sspan = sspan  + (1-mod(sspan,2));

if TB_curve
   p = pwd; % hack to avoid the function shadwoing by smooth from dip_image
   % DIPimage also has a smooth function. It smooth images.
   cd([TB_d filesep 'curvefit'])
   frc_in_smoothed = double(smooth(frc_in,sspan,'loess'));        % Loess (quadratic fit) 
   % Loess is locally estimated scatterplot smoothing W.Z.
   % Local regression using weighted linear least squares and a 1st degree polynomial model.
   cd(p)
else
   frc_in_smoothed = [double(gaussf(frc_in,.9))]';
end
q = (0:(length(frc_in_smoothed)-1))'/sz;                   % Spatial frequencies

% If the curve fitting toolbox is available, it uses smooth function, otherwise use another not optimal smoothing method for FRC.

%% Calculate intersections between the FRC curve and the threshold curve
% isects = polyxpoly(q,frc_in,q,thresholdcurve);
thresholdcurve = 1/7*ones(size(frc_in_smoothed)); % Here I can implement different kinds of thresholds. W.Z.
isects = isect(q,frc_in_smoothed,thresholdcurve); % frc_in is already smoothed. W.Z.

%% Find first intersection to obtain the resolution

% Throw away intersections at frequencies beyond the Nyquist frequency
isects = isects(isects<0.5);

if ~isempty(isects)
    % Find the first intersection where the FRC curve is decreasing
    isect_inds = 1+floor(sz*isects);    % Indices of the intersections
    for ii = 1:length(isect_inds)
        isect_ind = isect_inds(ii);
        if frc_in_smoothed(isect_ind+1) < frc_in_smoothed(isect_ind)
            resolution = 1/isects(ii);
            break
        end
    end
end

if ~exist('resolution','var')
    resolution = -1;
end

if resolution == -1
    varargout{1} = NaN;
    varargout{2} = 0;
    varargout{3} = 0.5;
    fprintf(' -- Could not find the resolution --\n')
    return
end

if nargout < 2
    varargout{1} = resolution;
    return
end

%% Calculate the resolution uncertainty

% Variance of the unsmoothed FRC values
% But there is almost no difference between smoothed and unsmoothed.
frc_var = (1+2*frc_in-frc_in.^2).*(1-frc_in).^2*ne_inv./nr;
% Equation S.35 in the SI. W.Z. nr is n in the quation. It is the number of
% pixels in a Fourier ring. The factor ne_inv arises to correct for the
% correlations between neighbouring FRC values.
% Indices of FRC values inside the smoothing window
indexvec = (ceil(isect_ind-sspan/2)):(floor(isect_ind+sspan/2));    
indexvec = indexvec(indexvec>0);
indexvec = indexvec(indexvec<=length(frc_in));
% The size of indexvec is the same as sspan. W.Z. index around the
% intersection (resolution).

% Calculate the covariance matrix of the LMS fit parameters. LMS = least
% mean square
S = diag(frc_var(indexvec));    % Approximate covariance matrix of the FRC values
X = cat(2,ones(size(indexvec')),q(indexvec),q(indexvec).^2);
C = inv(X'*X);
C = C*X'*S*X*C; % C is 3x3; W.Z.

% Estimate the variance of FRC(fres) resulting from the least mean
% squares fit. The factor ne_inv arises to correct for the
% correlations between neighbouring FRC values.
frc_newvar = ne_inv*(1./[1 resolution resolution^2])*C*(1./[1; resolution; resolution^2]);
% frc_newvar is one number. 1x1. W.Z.
if isnan(frc_newvar) || isinf(frc_newvar)
    frc_newvar = frc_var(isect_ind);
end

% Calculate the crossing between FRC + 1*stdev(FRC) and threshold
frc_high = frc_in_smoothed+sqrt(frc_newvar);
isects_high = isect(q,frc_high,thresholdcurve);
isects_high = isects_high(isects_high<0.5);
% Calculate the crossing between FRC - 1*stdev(FRC) and threshold 
frc_low = frc_in_smoothed-sqrt(frc_newvar);
isects_low = isect(q,frc_low,thresholdcurve);
isects_low = isects_low(isects_low<0.5);

% Find resolution + 1 standard deviation
if ~isempty(isects_high)
    isect_inds = 1+floor(sz*isects_high);
    for ii = 1:length(isect_inds)
        isect_ind = isect_inds(ii);
        if frc_high(isect_ind+1) < frc_high(isect_ind)
            resolution_high = 1/isects_high(ii);
            break
        end
    end
end
if ~exist('resolution_high','var')
    resolution_high = 0;
end

% Find resolution + - standard deviation
if ~isempty(isects_low)
    isect_inds = 1+floor(sz*isects_low);
    for ii = 1:length(isect_inds)
        isect_ind = isect_inds(ii);
        if frc_low(isect_ind+1) < frc_low(isect_ind)
            resolution_low = 1/isects_low(ii);
            break
        end
    end
end
if ~exist('resolution_low','var')
    resolution_low = 0.5;
end

%% Write outputs
varargout{1} = resolution;
varargout{2} = resolution_high;
varargout{3} = resolution_low;
