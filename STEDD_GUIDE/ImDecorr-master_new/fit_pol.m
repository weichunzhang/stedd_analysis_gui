function f = fit_pol(d,r,kc)

x = linspace(1, max(size(d(1,:))),max(size(d(1,:))));

l = d(:,6);
x = r(6,:);

%f = polyfit(r(3,:),d(:,3),5);

%func = 'a1*x^4+a2*x^3+a3*x^2+a4*x+a5+b1*exp(-k1*(x-b1)^2)';
%func = 'a0+a1*exp(-k1*(x-b1)^2)+a2*exp(-k2*(x-b2)^2)';
%func = 'a+b1*exp(-k1*x^t1)+b2*exp(-k2*x^t2)+b3*exp(-k3*x^t3)';

func = 'a+b1*exp(-k1*(x)^t1)+b2*exp(-k2*(x)^t2)';

%pol = fit(r0',d(:,6),func,'StartPoint',[ f(1) f(2) f(3) f(4)]);
%pol = fit(r(end,:)',d(:,end),func,'StartPoint',[ f(1) f(2) f(3) f(4) f(5)],'Lower',[-Inf,-Inf,-Inf,-Inf,0]);
%pol = fit(r(end,:)',d(:,end),func,'StartPoint',[1 1 1 1 1 1 1]);
%pol = fit(r(1,:)',d(:,1),func,'StartPoint',[0.4 1 1 1],'Upper',[Inf Inf Inf Inf],'Lower',[-Inf -Inf -Inf kc(1)]);
%pol = fit(x',l,func,'Upper',[100 100 100 100 100 100 100 100 100 100],'Lower',[-100 -100 -100 -100 -100 -100 -100 -100 -100 -100]);
pol = fit(x',l,func,'Upper',[3 3 3 3 3 3 3 3 3],'Lower',[-3 -3 -3 -3 -3 -3 -3 -3 -3]);

figure(5);

plot(pol);

hold on

pl = plot(x',l);

hold off

figure(6);

pl_fit = plot(pol);

end 