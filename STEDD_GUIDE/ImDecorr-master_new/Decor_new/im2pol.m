% imP = im2pol(imC)
% ---------------------------------------
%
% Transform an image from carthesian to polar coordinate
%
% Inputs:
%  imC        	Input image in carthesian coordinate
%
% Outputs:
%  imP        	Output image in polar coordinate or spherical coordinate

function imP = im2pol(imC)

rMin=0; 
rMax=1;

if length(size(imC))==2
    [Ny,Nx] = size(imC); 
    xc = (Ny+1)/2; yc = (Nx+1)/2; 
    sx = (Ny-1)/2; sy = (Nx-1)/2;

    Nr = 2*Ny; Nth = 2*Nx;

    dr = (rMax - rMin)/(Nr-1); 
    dth = 2*pi/Nth;


    r = rMin:dr:rMin+(Nr-1)*dr; 
    th = (0:dth:(Nth-1)*dth)'; 
    [r,th]=meshgrid(r,th); 

    x = r.*cos(th); y = r.*sin(th); 
    xR = x*sx + xc; yR = y*sy + yc;

    imP = interp2(imC, xR, yR,'cubic'); 

    imP(isnan(imP)) = 0;

elseif length(size(imC))==3
    
    [Nz,Ny,Nx] = size(imC); 
    xc = (Ny+1)/2; yc = (Nx+1)/2; zc = (Nz+1)/2;
    sx = (Ny-1)/2; sy = (Nx-1)/2; sz = (Nz-1)/2;
    
    
    Nr = 2*Ny; Nth = 2*Nx; Nfi = 2*Nz; 

    dr = (rMax - rMin)/(Nr-1); 
    dth = 2*pi/Nth;
    dfi = 2*pi/Nfi;


    r = rMin:dr:rMin+(Nr-1)*dr; 
    th = (0:dth:(Nth-1)*dth)';
    fi = (0:dfi:(Nfi-1)*dfi)';
    [r,th,fi]=meshgrid(r,th,fi); 

    x = r.*cos(th).*sin(fi); y = r.*sin(th).*sin(fi); z = r.*cos(fi);
    xR = x*sx + xc; yR = y*sy + yc; zR = z*sz + zc;

    imP = interp3(imC, xR, yR, zR,'cubic'); 

    imP(isnan(imP)) = 0;
    
end 
end