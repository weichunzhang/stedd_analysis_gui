function F = IntDecayMultiExp(X, tdata, IRF, weights)

%parameters
% X(1) IRF background
% X(2) IRF time shift
% X(3) intensity background
%-----------
% X(4) total amplitude
% X(5) tau1
%-----------
% X(6) f2
% X(7) tau2
%-----------
% X(8) f3
% X(9) tau3

%Calc. background corrected IRF1 
bIRF = IRF - X(1);

%Calc. time-shifted IRF1
IRF_tdata = tdata + X(2);     
tsIRF = interp1(tdata,bIRF,IRF_tdata,'linear');
vnan = isnan(tsIRF);
tsIRF(vnan) = 0;

%Calc. normalized IRF1
nIRF = tsIRF/sum(tsIRF);
nIRF(nIRF<0)=0;

% select parameters to fit
A = X(4);

nexp = (length(X)-3)/2;

f = X(3+2*(1:nexp)-1);
tau = X(3+2*(1:nexp));

if (nexp > 1)
    f(1) = min(1,max(0, 1 - sum(f(2:end))));
end

f = f/sum(f);


tdata_ar = repmat(tdata,1,nexp);
tau_ar = repmat(tau,length(tdata),1);
f_ar = repmat(f,length(tdata),1);

%disp([f1, f2, f3, f1+f2+f3]);
% intensity decay function
%Idecay = A * (f1*exp(-tdata./tau1) + f2*exp(-tdata/tau2) + f3*exp(-tdata/tau3));    
Idecay = A * sum(f_ar.*exp(-tdata_ar./tau_ar),2);    

% output data
ydata = conv(nIRF,Idecay) + X(3);
F = weights.*(ydata(1:length(tdata)));
   
  
    



