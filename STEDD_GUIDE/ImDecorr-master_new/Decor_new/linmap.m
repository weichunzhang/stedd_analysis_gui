% rsc = linmap(val,valMin,valMax,mapMin,mapMax)
% ---------------------------------------
%
% Performs a linear mapping of val from the range [valMin,valMax] to the range [mapMin,mapMax]
%
% Inputs:
%  val        	Input value
%  valMin		Minimum value of the range of val
%  valMax		Maximum value of the range of val
%  mapMin		Minimum value of the new range of val
%  mapMax		Maximum value of the new range of val
%
% Outputs:
%  rsc        	Rescaled value
%
% Example : rsc = linmap(val,0,255,0,1); % map the uint8 val to the range [0,1]


function  rsc = linmap(val,valMin,valMax,mapMin,mapMax)

if nargin == 3 % normalize the data between valMin and valMax
    mapMin = valMin;
    mapMax = valMax;
    valMin = min(val(:));
    valMax = max(val(:));
end

% convert the input value between 0 and 1
tempVal = (val-valMin)./(valMax-valMin);

% clamp the value between 0 and 1
map0 = tempVal < 0;
map1 = tempVal > 1;
tempVal(map0) = 0;
tempVal(map1) = 1;

% rescale and return
rsc = tempVal.*(mapMax-mapMin) + mapMin;

end