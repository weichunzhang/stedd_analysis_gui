function [er,er_res] = err_0(d,kc,r,r0,ID,pix,dt,Ng)

%First error is due to precission of finding curve that we are looking for 
%find difference of peaks positions of closest maximums

%derivative should be averaged to avoid error
%%
[m,id_m] = max(kc);
kc_1 = kc(2:end);

for k = 1:length(kc_1)
    
    [m_1,id_1] = max(kc_1);
    
    for i = 1:length(kc_1)
        
        if kc_1(i) ~= max(kc_1)
            
            kc_1(i) = kc_1(i);
            
        else 
            
            kc_1(i) = 0;
            
        end
    end
    
    [m_2,id_2] = max(kc_1);
    
    delta_kc(k) = m_1 - m_2;
    delta_cc(k) = abs(d(ID(id_1),id_1)-d(ID(id_2),id_2));
    
    
    dif(k) = delta_kc(k)/delta_cc(k);
    
    if dif(k) > 10 || dif(k) <= 0
         
    dif(k) = 0;
    
    end
    
end
 % change it   
dif_m = min(abs(dif(dif ~= inf)));
dif_m = 0;

for i=length(dif)-1:(-1):1
      
    if dif(i+1) ~= 0 && dif(i) ~= 0 && isnan(dif(i+1)) ~= 1 && isnan(dif(i)) ~= 1 && dif(i+1) ~= inf && dif(i) ~= inf && dif_m == 0
        dif_m = dif(i); 
    
    end
end
%%


%cross correlation coefficient at the position of the first peak for each
%curve, that is used to find closest to the curve with the best peak
%position

r_close_0 = [];
id_0 = [];
d_close = [];

[r_close_0,id_0] = min(abs(r0(:) - m));
d_close(1,1:Ng) = d(id_0,1:Ng);  
    
r_close = [];
id = [];


[r_close,id] = min(abs(r(:) - m));
d_close(1,Ng+1:2*Ng) = d(id,Ng+1:end);  

%calculate difference between cross correlation coefficients for area near
%to the best peak
delta = d_close - d_close(id_m);

for i = 1:length(delta)
    
    if delta(i) < 0
    delta(i) = delta(i);
    else
    delta(i) = 1;
    end
    
end

%find curvature closest to the best curvature
[~,idx] = min(abs(delta));
cur = d(:,idx);

if idx > Ng 
cur(:,2) = r;

elseif idx <= Ng
cur(:,2) = r0;

end
    
    
%find cross-correlation coefficients closest to the peak of the best curve,
%from the curve next to the best without peak
max_1 = r(ID(id_m));
cc_1 = d(ID(id_m),id_m);
[~,id_4] = min(abs(cur(:,2) - max_1));
max_2 = cur(id_4,2);
cc_2 = cur(id_4,1); 

%first error evaluation - difference between curve with the best peak and
%next curve
er_1 = dif_m*(cc_1 - cc_2);

%Second error is due to limited number of points on the curve

er_2 = r(ID(id_m))-r(ID(id_m)-1);

%Third error based on unclearnes of the peak position, may double first
%error

er_3 = dt*dif_m;


er = sqrt(er_1^2 + er_2^2 + er_3^2);

er_res = abs(-2*pix*er/(m)^2); 

end