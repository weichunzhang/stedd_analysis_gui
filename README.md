# STEDD_Analysis_GUI

STEDD_GUIDE is based on Matlab GUIDE. The core algorithm is written by Andrei. It can read raw bh spc data and build photon histogram and reconstruct images fairly fast.

STEDD_analysis_APP is based on Matlab app. It was originally written Benidikt and Julian. It reads Photon_bank.mat data which has to be converted from spc data with a separate program. 

image_operation_app is based on Matlab app. It can do addition (with a factor), subtraction (with a factor) and averaging of two images.