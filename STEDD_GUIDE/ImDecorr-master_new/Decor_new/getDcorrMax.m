% [ind,A] = getDcorrMax(d)
% ---------------------------------------
%
% Return the local maxima of the decorrelation function d
%
% Inputs:
%  d        	Decorrelation function
%
% Outputs:
%  ind        	Position of the local maxima
%  A			Amplitude of the local maxima


function [ind,A] = getDcorrMax(d)

[A,ind] = max(d);
t = d;
% arbitrary peak significance parameter imposed by numerical noise
% this becomes relevant especially when working with post-processed data
dt = 0.001;

while ind == length(t)
    t(end) = [];
    if isempty(t)
        A = 0;
        ind = 1;
    else
        [A,ind] = max(t);
        % check if the peak is significantly larger than the former minimum
        if t(ind) - min(d(ind:end)) > dt 
            break
        else
            t(ind) = min(d(ind:end));
            ind = length(t);
        end
    end
     
end