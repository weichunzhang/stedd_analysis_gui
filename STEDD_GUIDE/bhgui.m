function varargout = bhgui(varargin)
    % GUI for reading bh TCSPC .spc data and reconstruct the intensity
    % image according to the markers.
    % Written by Andrei Kobitski. Modified by Weichun Zhang.
    %
    % =============== Here are the instructions. =========
    % When loading the file, the program takes the time base from the .sdt file. If the sdt file is not available, the default value is 25 ns. 

% BHGUI MATLAB code for bhgui.fig
%      BHGUI, by itself, creates a new BHGUI or raises the existing
%      singleton*.
%
%      H = BHGUI returns the handle to a new BHGUI or the handle to
%      the existing singleton*.
%
%      BHGUI('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in BHGUI.M with the given input arguments.
%
%      BHGUI('Property','Value',...) creates a new BHGUI or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before bhgui_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to bhgui_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help bhgui

% Last Modified by GUIDE v2.5 10-Mar-2020 18:12:36

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @bhgui_OpeningFcn, ...
                   'gui_OutputFcn',  @bhgui_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
               
global bh_data;
global bh_info;


%add bio-formats path 
bf_path = '.\bfmatlab\';
if (exist(bf_path,'dir') == 7)
    % 7 ??? name is a folder.
    % Check if bf_path is a folder. https://www.mathworks.com/help/matlab/ref/exist.html?s_tid=doc_ta
    addpath(bf_path);
end

tiff_path = '.\saveastiff_4.4\';
if (exist(tiff_path,'dir') == 7)
    % 7 ??? name is a folder.
    % Check if bf_path is a folder. https://www.mathworks.com/help/matlab/ref/exist.html?s_tid=doc_ta
    addpath(tiff_path);
end

% functions for doing Fourier ring correlation
frc_path = '.\Delft_FRC_matlabdistribution\FRCresolutionfunctions\';
if (exist(frc_path,'dir') == 7)
    % 7 ??? name is a folder.
    % Check if bf_path is a folder. https://www.mathworks.com/help/matlab/ref/exist.html?s_tid=doc_ta
    addpath(frc_path);
end

% functions for decorrelation analysis
decorr_path = '.\ImDecorr-master\';
if (exist(decorr_path,'dir') == 7)
    % 7 ??? name is a folder.
    % Check if bf_path is a folder. https://www.mathworks.com/help/matlab/ref/exist.html?s_tid=doc_ta
    addpath(genpath(decorr_path)); % add also its subfolders
end

% Other useful functions
function_path = '.\functions\';
if (exist(function_path,'dir') == 7)
    % 7 ??? name is a folder.
    % Check if bf_path is a folder. https://www.mathworks.com/help/matlab/ref/exist.html?s_tid=doc_ta
    addpath(function_path);
end

if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end

% End initialization code - DO NOT EDIT


% --- Executes just before bhgui is made visible.
function bhgui_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to bhgui (see VARARGIN)

% Choose default command line output for bhgui
handles.output = hObject;

set(handles.StatusMsgStr,'String','Ready'); 
set(handles.DataFolderStr,'String',pwd);
set(handles.ImgRowTrig,'Value',1); % added 2019-09-13, selet between row and frame, Image Row Trigger.
UpdateFileList(handles);
run('C:\Program Files\DIPimage 2.9\dipstart.m'); % Load DIPimage for FRC calculation.
% Update handles structure
guidata(hObject, handles);

% UIWAIT makes bhgui wait for user response (see UIRESUME)
% uiwait(handles.figure1);

% --- Outputs from this function are returned to the command line.
function varargout = bhgui_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;

% --- Executes on button press in DataPathBtn.
function DataPathBtn_Callback(hObject, eventdata, handles)
% hObject    handle to DataPathBtn (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

%rdir = pwd;
FolderName = get(handles.DataFolderStr,'String');

if (exist(FolderName,'dir') ~= 7)
    FolderName = pwd;
end

FolderName = uigetdir(FolderName);
if (FolderName ~= 0)
    set(handles.DataFolderStr,'String',FolderName);
end

UpdateFileList(handles);
%cd(rdir);

% Added on 2019-09-13, but it looks not doing anything.
function DataFolderStr_Callback(hObject, eventdata, handles)
% hObject    handle to DataFolderStr (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of DataFolderStr as text
%        str2double(get(hObject,'String')) returns contents of DataFolderStr as a double

% --- Executes during object creation, after setting all properties.
function DataFolderStr_CreateFcn(hObject, eventdata, handles)
% hObject    handle to DataFolderStr (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function UpdateFileList(handles)

global bh_info;

% bh_info = struct('filename','','phIdx',[],'pxIdx',[],'lnIdx',[],'frIdx',[], 'tADC',[],...
%     'timebase',25, 'timebins',pow2(12),'gridx',512,'img_1',[],'img_2',[],'img_3',[]);
% In the new version, this is already done in the core function.
% 2019-09-13.

DataFiles = {};
FolderName = get(handles.DataFolderStr,'String');

FileExtList = get(handles.DataFilesExtList,'String');% File name extension (spc or sdt)
FileExtValue = get(handles.DataFilesExtList,'Value'); % Value is 1.0.

filelist = dir(char(fullfile(FolderName,FileExtList(FileExtValue))));

% 2019-09-13.
if (length(filelist) > 0)
    bh_info = bhcorefunc_mod('init_info',length(filelist));

    for f_idx = 1:length(filelist)
        bh_info(f_idx).filename = fullfile(FolderName,filelist(f_idx).name);
        DataFiles{f_idx} = filelist(f_idx).name;

        bh_info(f_idx).timebase = 25;
        bh_info(f_idx).timebins = pow2(12);
    end

    % if (~isempty(filelist))
    %     
    %     DataFiles = {};
    % end

    % if (get(handles.DataFilesSubDirChk,'Value'))
    %     folderlist = dir(FolderName);
    %     if (~isempty(folderlist.folder))
    %         
    %     end
    % end

    UpdateDataPlot(handles, bh_info(1));
end

% for f_idx = 1:length(filelist)
%     bh_info(f_idx).filename = fullfile(FolderName,filelist(f_idx).name);
%     DataFiles{f_idx} = filelist(f_idx).name;
% 
%     bh_info(f_idx).timebase = 25;
%     bh_info(f_idx).timebins = pow2(12);
% end

% if (~isempty(filelist))
%     
%     DataFiles = {};
% end

% if (get(handles.DataFilesSubDirChk,'Value'))
%     folderlist = dir(FolderName);
%     if (~isempty(folderlist.folder))
%         
%     end
% end

set(handles.DataFilesList,'String',DataFiles);
% UpdateDataInfo(handles, bh_info(1));

% Completely replaced on 2019-09-13.
% --- Executes on button press in LoadDataBtn.
function LoadDataBtn_Callback(hObject, eventdata, handles)
% hObject    handle to LoadDataBtn (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global bh_data;
global bh_info;

t0 = tic;
%folder = get(handles.DataFolderStr,'String');
filelist = get(handles.DataFilesList,'String');
fileidx = get(handles.DataFilesList,'Value');


data_file = bh_info(fileidx).filename;%fullfile(folder,char(filelist(fileidx)));


if (exist(data_file,'file') == 2)

    msgstr = sprintf('Read data from %s ...',char(filelist(fileidx))); 
    set(handles.StatusMsgStr,'String',msgstr); 
    drawnow();

    tic;
    bh_data = bhcorefunc('bhreaddata',data_file,'uint32');
    t1 = toc;
    
    tic;
    bh_info(fileidx) = bhcorefunc_mod('bhgetinfo',bh_data, bh_info(fileidx));
       
    bh_info(fileidx).ltData = bhcorefunc('bhgetlthist',bh_info(fileidx));
    
%     bh_info(fileidx).ltData = zeros(bh_info(fileidx).timebins,2);
%     bh_info(fileidx).ltData(:,1) = (0:bh_info(fileidx).timebins-1)*bh_info(fileidx).timebase/bh_info(fileidx).timebins;
% 
%     if (~isempty(bh_info(fileidx).tADC))
%         bh_info(fileidx).ltData(:,2) = histc(bh_info(fileidx).tADC(bh_info(fileidx).phIdx),0:bh_info(fileidx).timebins-1);
%     end
%     
    [i_max, t_max] = max(bh_info(fileidx).ltData(:,2));
    t_left = find(bh_info(fileidx).ltData(1:t_max,2) < 0.25*mean(bh_info(fileidx).ltData(:,2)), 1, 'last');
    t_right = t_max + find(bh_info(fileidx).ltData(t_max:end,2) < 0.25*mean(bh_info(fileidx).ltData(:,2)), 1, 'first');
    
    bh_info(fileidx).ltPeriod(1) = t_left*bh_info(fileidx).timebase/bh_info(fileidx).timebins;
    bh_info(fileidx).ltPeriod(2) = t_right*bh_info(fileidx).timebase/bh_info(fileidx).timebins;

    UpdateDataPlot(handles, bh_info(fileidx));
    t2 = toc;

    msgstr = sprintf('Finished reading in %5.1f s; get data info in %5.1f s',t1, t2); 
    set(handles.StatusMsgStr,'String',msgstr); 
  
end

t0 = toc(t0);
msgstr = get(handles.StatusMsgStr,'String'); 
msgstr = sprintf('%s, total time: %5.2f s',msgstr, t0);
set(handles.StatusMsgStr,'String',msgstr); 

%  ===   It is a replacement of UpdateDataInfo(). 2019-09-13. WZ modified
%  it.
function UpdateDataPlot(handles, info)

set(handles.DataNPh,'String',num2str(length(info.phIdx)));
set(handles.DataNPx,'String',num2str(length(info.pxIdx)));
set(handles.DataNLn,'String',num2str(length(info.lnIdx)));
set(handles.DataNFr,'String',num2str(length(info.frIdx)));
% Recorded number of frames.

set(handles.DataTBase,'String',num2str(info.timebase,'%8.4f'));
% set(handles.ImgThigh,'String',num2str(info.timebase,'%8.4f'));
% 
% set(handles.ImgTlow,'String',num2str(info.ltPeriod(1),'%6.2f'));
% set(handles.ImgThigh,'String',num2str(info.ltPeriod(2),'%6.2f'));

if (~isempty(info.ltData))
    subplot(handles.tHistAxis);
    log_value = get(handles.RadioLog,'Value'); % If or not showing the photon histogram in semilogy.
    if(log_value)
        semilogy(info.ltData(:,1),info.ltData(:,2));
    else
        plot(info.ltData(:,1),info.ltData(:,2));
    end
    xlim([info.ltData(1,1) info.ltData(end,1)]);

%     line([info.ltPeriod(1) info.ltPeriod(1)],[0 max(info.ltData(:,2))], 'Color','red','LineStyle','--')
%     line([info.ltPeriod(2) info.ltPeriod(2)],[0 max(info.ltData(:,2))], 'Color','red','LineStyle','--')
else
    subplot(handles.tHistAxis);
    cla; % If no data, clear the histogram.
end

% if (~isempty(info.img))
%     imagesc(info.img);
% end
subplot(handles.imgAxis1);
cla;
if (~isempty(info.img_1))
    image_1 = UpdateImage(handles, info.img_1);  % Image format is changed.
    showImage(image_1, 'ax1', handles);
end
subplot(handles.imgAxis2);
cla;
if (~isempty(info.img_2))
    image_2 = UpdateImage(handles, info.img_2);  % Image format is changed.
    showImage(image_2, 'ax2', handles);
end
subplot(handles.imgAxis3);
cla
if (~isempty(info.img_3))
    image_3 = UpdateImage(handles, info.img_3);  % Image format is changed.
    showImage(image_3, 'ax3', handles);
end

%----------------------
% It is no longer used. 2019-09-13.
% replaced by UpdateDataInfo()
function UpdateDataInfo(handles, info)
% info is a local variable. 
set(handles.DataNPh,'String',num2str(length(info.phIdx)));
set(handles.DataNPx,'String',num2str(length(info.pxIdx)));
set(handles.DataNLn,'String',num2str(length(info.lnIdx)));
set(handles.DataNFr,'String',num2str(length(info.frIdx))); % Recorded number of frames.
% If frame triggers are not saved during measurement, we can also input the
% number that we know.

set(handles.DataTBase,'String',num2str(info.timebase,'%8.4f'));
% set(handles.ImgThigh_1,'String',num2str(info.timebase,'%8.4f')); % Upper limit of T for buidling the image.

hData = zeros(info.timebins,2);
hData(:,1) = (0:info.timebins-1)*info.timebase/info.timebins;

if (~isempty(info.tADC))
    hData(:,2) = histc(info.tADC(info.phIdx),0:info.timebins-1);
end

subplot(handles.tHistAxis);
log_value = get(handles.RadioLog,'Value'); % If or not showing the photon histogram in semilogy.

if(log_value)
    semilogy(hData(:,1),hData(:,2));
%    ylim([1000 1.1*max(hData(:,2))]);
else
    plot(hData(:,1),hData(:,2)); % Linear plot
end

xlim([hData(1,1) hData(end,1)]);
% xlabel('Time (ns)');
% ylabel('# of photons'); 

subplot(handles.imgAxis1);
cla;
subplot(handles.imgAxis2);
cla;
subplot(handles.imgAxis3);
cla
% if (~isempty(info.img_1))
%     imagesc(info.img_1);
% end
% if (~isempty(info.img_2))
%     imagesc(info.img_2);
% end
% if (~isempty(info.img_3))
%     imagesc(info.img_3);
% end

% --- Executes on selection change in DataFilesList.
function DataFilesList_Callback(hObject, eventdata, handles)
% hObject    handle to DataFilesList (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global bh_info;

idx = get(handles.DataFilesList,'Value');
if (~isempty(idx))
    UpdateDataPlot(handles, bh_info(idx));
end


% Hints: contents = cellstr(get(hObject,'String')) returns DataFilesList contents as cell array
%        contents{get(hObject,'Value')} returns selected item from DataFilesList


% --- Executes during object creation, after setting all properties.
function DataFilesList_CreateFcn(hObject, eventdata, handles)
% hObject    handle to DataFilesList (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes on button press in DataFilesSubDirChk.
function DataFilesSubDirChk_Callback(hObject, eventdata, handles)
% hObject    handle to DataFilesSubDirChk (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of DataFilesSubDirChk

% --- Executes on selection change in DataFilesExtList.
function DataFilesExtList_Callback(hObject, eventdata, handles)
% hObject    handle to DataFilesExtList (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns DataFilesExtList contents as cell array
%        contents{get(hObject,'Value')} returns selected item from DataFilesExtList

% --- Executes during object creation, after setting all properties.
function DataFilesExtList_CreateFcn(hObject, eventdata, handles)
% hObject    handle to DataFilesExtList (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function DataTBase_Callback(hObject, eventdata, handles)
% hObject    handle to DataTBase (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of DataTBase as text
%        str2double(get(hObject,'String')) returns contents of DataTBase as a double

global bh_info;

idx = get(handles.DataFilesList,'Value');
bh_info(idx).timebase = str2double(get(handles.DataTBase,'String')); % Get the value from the edit box.

% The following code is added by me. It refreshes the histogram.
UpdateDataPlot(handles, bh_info(idx));

% --- Executes during object creation, after setting all properties.
function DataTBase_CreateFcn(hObject, eventdata, handles)
% hObject    handle to DataTBase (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function image = getImage(handles, img, ch)
% No filtering is not done here.
% img: image 1 or image 2?
% ch: all channels, only odd channels or only even channels?
% This function generates the image matrix, which will be used by other
% callback functions.
global bh_data;
global bh_info;

idx = get(handles.DataFilesList,'Value');

if (~isempty(idx))
    
    AverLn = str2double(get(handles.AverEdit,'String')); % The number of lines to average.
    AverLn_value = get(handles.LnAveRadio,'Value'); % whether or not do line averaging.
    AllFr_value = get(handles.AllFrRadio,'Value'); % show all the frames or only show the middle frame.
    LeftCut = str2double(get(handles.LeftCutEdit,'String')); % Pixels to be cut because they are artifacts.
    RightCut = str2double(get(handles.RightCutEdit,'String'));
    TopCut = str2double(get(handles.TopCutEdit,'String'));
    BottomCut = str2double(get(handles.BottomCutEdit,'String'));
%     Rec_software = get(handles.Rec_Soft, 'Value'); % Data recorded with different software require different corrections of artefact.
    % 1 is Matlab scanning_FCS.m using a digital channel as line and frame
    % triggers. 2 is the Matlab program using a counter output channel as line
    % and frame triggers, 3 is for data recorded with SPC-150 FLIM mode.

    switch img
        case 'img1'
            % micro time interval
            %t1sel = [0 bh_info(idx).timebins-1];
            % t1sel(1) = str2double(get(handles.ImgTlow_1,'String'))/bh_info(idx).timebase;
            % The above one is the original one, which is wrong.
    %         t1sel(1) = str2double(get(handles.ImgTlow_1,'String'))*bh_info(idx).timebins/bh_info(idx).timebase;
    %         t1sel(2) = str2double(get(handles.ImgThigh_1,'String'))*bh_info(idx).timebins/bh_info(idx).timebase;
    %       Changed to the folloing on 2019-09-13.
            t1sel(1) = str2double(get(handles.ImgTlow_1,'String'))/bh_info(idx).timebase;
            t1sel(2) = str2double(get(handles.ImgThigh_1,'String'))/bh_info(idx).timebase;
            % bh_info(idx).timebins is the number of channels, e.g. 4096.
            % bh_info(idx).timebase is length of the window, e.g. 25 ns.
            % t1sel(2) = str2double(get(handles.ImgThigh_1,'String'))/bh_info(idx).timebase;
            % handles.ImgThigh_1 is also updated when time base is changed. But it can be
            % still changed to select photons. 
            % ImgTlow_Callback and ImgThigh_Callback are not needed because the image
            % is updated when the 'Get image' btn is pressed.
        case 'img2'
    %       Changed to the folloing on 2019-09-13.
            t1sel(1) = str2double(get(handles.ImgTlow_2,'String'))/bh_info(idx).timebase;
            t1sel(2) = str2double(get(handles.ImgThigh_2,'String'))/bh_info(idx).timebase;
    end

    t1sel(1) = max(t1sel(1),0);
    t1sel(2) = min(t1sel(2),bh_info(idx).timebins-1);

    bh_info(idx).ltPeriod = t1sel*bh_info(idx).timebase; % new
    UpdateDataPlot(handles, bh_info(idx)); % new

    gridx_sub = str2double(get(handles.ImgGridX,'String')); % # of lines per line in each sub image;
    % number of pixels per line
    % bh_info(idx).gridx = AverLn.*gridx_sub + 2;
    % If we use bnCell = cellfun(@(tcell)
    % 2*tcell:tcell:(params.gridx)*tcell,grid_dt,'UniformOutput',0); in core
    % function, we have to add 2 extra pixels.

    bh_info(idx).gridx = AverLn.*gridx_sub;

    if (get(handles.ImgRowTrig,'Value') == 1) %line
        bh_info(idx).rowIdx = bh_info(idx).lnIdx;
    elseif (get(handles.ImgRowTrig,'Value') == 2) %frame
        bh_info(idx).rowIdx = bh_info(idx).frIdx;
    else
        bh_info(idx).rowIdx = [];
    end

    offset = str2double(get(handles.Image_shift,'String')); % number of pixels to shift.
    % negative means left and possitive means right.

    % if (~isempty(bh_info(idx).lnIdx)) % assign data events to lines
    if (~isempty(bh_info(idx).rowIdx)) %assign data events to lines 2019-09-13
        switch ch
            case 'all'
                image = bhcorefunc('bhgetimage',bh_data, bh_info(idx), round(t1sel*bh_info(idx).timebins), 1);
                % The last parameter binlines is set to 1 for normal
                % imaging.
            case 'odd'
                image = bhcorefunc_mod('bhgetimage_odd',bh_data, bh_info(idx), round(t1sel*bh_info(idx).timebins), 1);
            case 'even'
                image = bhcorefunc_mod('bhgetimage_even',bh_data, bh_info(idx), round(t1sel*bh_info(idx).timebins), 1);
        end
        image = double(image); % converting the data type to double is very important.

%         switch Rec_software
%             case 1 % If the digital channel is used for the line trigger, line triggers are synced with scanning.
%                  switch ch
%                     case 'all'
%                         image = bhcorefunc('bhgetimage',bh_data, bh_info(idx), round(t1sel*bh_info(idx).timebins));
%                     case 'odd'
%                         image = bhcorefunc('bhgetimage_odd',bh_data, bh_info(idx), round(t1sel*bh_info(idx).timebins));
%                     case 'even'
%                         image = bhcorefunc('bhgetimage_even',bh_data, bh_info(idx), round(t1sel*bh_info(idx).timebins));
%                  end
%             case 2 % If the counter is used to for the line trigger
%                 if(~AverLn_value) % if not doing line averaging, I discard two vertical lines.
%                     switch ch
%                         case 'all'
%                             image = bhcorefunc('bhgetimage_2',bh_data, bh_info(idx), round(t1sel*bh_info(idx).timebins));
%                         case 'odd'
%                             image = bhcorefunc('bhgetimage_odd_2',bh_data, bh_info(idx), round(t1sel*bh_info(idx).timebins));
%                         case 'even'
%                             image = bhcorefunc('bhgetimage_even_2',bh_data, bh_info(idx), round(t1sel*bh_info(idx).timebins));
%                     end
%                 else
%                     switch ch
%                         case 'all'
%                             image = bhcorefunc('bhgetimage',bh_data, bh_info(idx), round(t1sel*bh_info(idx).timebins));
%                         case 'odd'
%                             image = bhcorefunc('bhgetimage_odd',bh_data, bh_info(idx), round(t1sel*bh_info(idx).timebins));
%                         case 'even'
%                             image = bhcorefunc('bhgetimage_even',bh_data, bh_info(idx), round(t1sel*bh_info(idx).timebins));
%                     end
%                 end
%             case 3
%                 % May add later
%         end
        
        image = circshift(image,[0 offset]); % Shift the image to correct the artifact during scanning.
        
        if(AverLn_value)
            [r, c] = size(image);
            temp_image = reshape(image, [r, gridx_sub, AverLn]);
            image = mean(temp_image,3); % if temp_image is uint16, it becomes double
        end
        
        if(~AllFr_value)
            % Assign the data to frames.
%             NumFr = str2double(get(handles.NumFr,'String')); % Number of frames, either from recorded frame triggers or our own input.
%             LinesPerFrame = size(bh_info(idx).img_1, 1)/NumFr;
%             if (~isequal(mod(size(bh_info(idx).img_1,1), LinesPerFrame), 0)) % If the last frame is incomplete
            if (isempty(bh_info(idx).frIdx)) % If the data do not contain frame triggers.
                LinesPerFrame = str2double(get(handles.ImgGridY,'String')); % Number of lines in each frame. Invalid lines are also included in these lines.
                [r, c] = size(image);
                nlay = ceil(size(image,1)./LinesPerFrame); % Number of frames; 
                a = zeros(LinesPerFrame*nlay, c);
                a(1:r,:) = image; % If the last frame is not complete, the remaining part is zeros.
                image = permute(reshape(a',[c,LinesPerFrame,nlay]),[2,1,3]);
                % Reshape into a three-dimensional matrix
            else % If the data contain frame triggers.
                [r, c] = size(image);
                temp = [bh_info(idx).lnIdx; bh_info(idx).frIdx];
                % If the DO port is used for line and frame triggers, frIdx
                % in included in lnIdx.
                temp = sort(temp);
                [tf, ind] = ismember(bh_info(idx).frIdx, temp);
                temp2 = 1:length(bh_info(idx).frIdx);
                temp2 = temp2';
                ind = ind - temp2; % The index of lines that are the beginning of each frame;
                ind = [ind; r+1]; % The method is for the case where the first line trigger is earlier than the first frame trigger for each frame.
                
                lnpfr = [diff(ind); r-ind(end-1)+1]; % the array containing the number of lines in each frame
                max_lnpfr = max(lnpfr); % maximal number of lines per frame.c
                nlay = length(bh_info(idx).frIdx); % % number of layers;
                a = zeros(max_lnpfr, c, nlay);
                for mm = 1:nlay
                    a(1:lnpfr(mm),:,mm) = image(ind(mm):ind(mm+1)-1,:);
                end
                % The number of lines in each frame may be different. 
                image = a;
%                     a = bh_info(idx).img_1(1:LinesPerFrame*(length(bh_info(idx).frIdx)),:);
%                     [r, c] = size(a);
%                     nlay = length(bh_info(idx).frIdx);
%                     bh_info(idx).img_1 = permute(reshape(a',[c,r/nlay,nlay]),[2,1,3]);
                % Convert the image into a stack. The last incomplete frame is deleted.
                % https://www.mathworks.com/matlabcentral/answers/36563-reshaping-2d-matrix-into-3d-specific-ordering
            end
%             else % If the last frame is complete.
%                 a = bh_info(idx).img_1;
%                 [r, c] = size(a);
%                 if (isempty(bh_info(idx).frIdx)) % If the data do not contain frame triggers.
%                     nlay = size(bh_info(idx).img_1,1)./LinesPerFrame; % Number of frames;
%                 else % If the data do contain frame triggers.
%                     nlay = length(bh_info(idx).frIdx); % Number of frames;
%                 end
%                 bh_info(idx).img_1 = permute(reshape(a',[c,r/nlay,nlay]),[2,1,3]);
%             % Convert the image into a stack. bh_info(idx).img_1 is now a
%             % 3D matrix
            % end
            
            % Cut the pixels with artifacts
            image(:,1:LeftCut,:) = [];
            image(:,(end-RightCut+1):end,:) = [];
            image(1:TopCut,:,:) = [];
            image((end-BottomCut+1):end,:,:) = [];

        else % Otherwise, show all the images.
            % Artifact correction
            image(:,1:LeftCut) = [];
            image(:,(end-RightCut+1):end) = [];
            image(1:TopCut,:) = [];
            image((end-BottomCut+1):end,:) = [];
        end
     
    end
end

% Function to show image. img is the image to show and ax is the axes
function showImage(img, ax, handles)
% global bh_info;
% idx = get(handles.DataFilesList,'Value');
AllFr_value = get(handles.AllFrRadio,'Value'); % show all the frames or only show one of the frames.
AR = str2double(get(handles.AR, 'String'));

if(~AllFr_value)
    FrID = str2double(get(handles.FrIDEdit,'String')); % The ID of frame that is going to show.
    if(FrID <= size(img,3))
        switch ax
            case 'ax1'
                subplot(handles.imgAxis1);
            case 'ax2'
                subplot(handles.imgAxis2);
            case 'ax3'
                subplot(handles.imgAxis3);
        end
        imagesc(img(:,:,FrID));
        colormap hot
        colorbar
        axis image
        daspect([AR 1 1]);
    else
        cla;
        disp('Error! Frame ID larger than  the number of valid stacks!');
    end

else % Otherwise, show all the images.
    switch ax
        case 'ax1'
            subplot(handles.imgAxis1);
        case 'ax2'
            subplot(handles.imgAxis2);
        case 'ax3'
            subplot(handles.imgAxis3);
    end
    imagesc(img);
    colormap hot
    colorbar
    axis image
    daspect([AR 1 1]);
end

% --- Executes on button press in imgMTBtn_1.
function imgMTBtn_1_Callback(hObject, eventdata, handles)
% hObject    handle to imgMTBtn_1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global bh_info;
idx = get(handles.DataFilesList,'Value');
filter_list = get(handles.FilterType_1, 'String'); % read the list of filters
filter_value = get(handles.FilterType_1, 'Value'); % which one is selected
filter_type = char(filter_list(filter_value));
% wiener1_value = get(handles.Wiener1,'Value'); % If or not doing Wiener filtering for STED1.
filter_px = str2double(get(handles.Filter1_px, 'String'));
% filter_px = uint8(filter_px); % size of filter in the unit of px.

if (~isempty(idx))
    set(handles.StatusMsgStr,'String','Calculating MT values and show image...'); 
    drawnow();
    
    tic
    
    if (~isempty(bh_info(idx).lnIdx)) % assign data events to lines
        bh_info(idx).img_1 = getImage(handles, 'img1', 'all'); % calculate the image matrix
        if(~strcmp(filter_type, 'No filter'))
            nlay = size(bh_info(idx).img_1, 3);
            for n = 1:1:nlay
                switch filter_type
                    case 'Wiener filter'
                        bh_info(idx).img_1(:,:,n) = wiener2(bh_info(idx).img_1(:,:,n), [filter_px, filter_px]);
                    case 'Gaussian filter'
                        bh_info(idx).img_1(:,:,n) = imgaussfilt(bh_info(idx).img_1(:,:,n), filter_px);
                    case 'Average filter'
                        bh_info(idx).img_1(:,:,n) = filter2(fspecial('average', filter_px), bh_info(idx).img_1(:,:,n));
                    case 'Median filter'
                        bh_info(idx).img_1(:,:,n) = medfilt2(bh_info(idx).img_1(:,:,n), [filter_px, filter_px]);
                end
            end
        end
        image = UpdateImage(handles, bh_info(idx).img_1);  % Image format is changed.
        showImage(image, 'ax1', handles); % artefacts corrected but data type not changed.
    end
    
    t = toc;
    msgstr = sprintf('Get STED1 image in %5.1f s',t); 
    set(handles.StatusMsgStr,'String',msgstr); 
end

% This callback function is probably not needed, because we have the 'Get
% image' btn.
function ImgGridX_Callback(hObject, eventdata, handles)
% hObject    handle to ImgGridX (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of ImgGridX as text
%        str2double(get(hObject,'String')) returns contents of ImgGridX as a double


% --- Executes during object creation, after setting all properties.
function ImgGridX_CreateFcn(hObject, eventdata, handles)
% hObject    handle to ImgGridX (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function ImgTlow_1_Callback(hObject, eventdata, handles)
% hObject    handle to ImgTlow_1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of ImgTlow_1 as text
%        str2double(get(hObject,'String')) returns contents of ImgTlow_1 as a double

% --- Executes during object creation, after setting all properties.
function ImgTlow_1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to ImgTlow_1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function ImgThigh_1_Callback(hObject, eventdata, handles)
% hObject    handle to ImgThigh_1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of ImgThigh_1 as text
%        str2double(get(hObject,'String')) returns contents of ImgThigh_1 as a double

% --- Executes during object creation, after setting all properties.
function ImgThigh_1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to ImgThigh_1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes on button press in imgSaveBtn_1.
function imgSaveBtn_1_Callback(hObject, eventdata, handles)
% hObject    handle to imgSaveBtn_1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global bh_info;
Data_Type = get(handles.DataTypeList, 'Value');
idx = get(handles.DataFilesList,'Value');

% number of pixels per line
% gridx = str2double(get(handles.ImgGridX,'String'));
% number of lines per frame
% gridy = str2double(get(handles.ImgGridY,'String')) - 1;
% image = zeros(bh_info(idx).gridy, bh_info(idx).gridx, length(bh_info(idx).frIdx));
% The image stack to save.

low = get(handles.ImgTlow_1,'String');
high = get(handles.ImgThigh_1,'String'); % These are for the file name to save.
% 
[fp,fn,~] = fileparts(bh_info(idx).filename);

% img_fn = fullfile(fp,strcat(fn,'_STED1_',low,'to',high,'.tif'));
switch Data_Type
    case 1
        img_fn = fullfile(fp,strcat(fn,'_STED1_',low,'to',high,'_original','.tif'));
    case 2
        img_fn = fullfile(fp,strcat(fn,'_STED1_',low,'to',high,'_uint8','.tif'));
    case 3
        img_fn = fullfile(fp,strcat(fn,'_STED1_',low,'to',high,'_uint16','.tif'));
    case 4
        img_fn = fullfile(fp,strcat(fn,'_STED1_',low,'to',high,'_single','.tif'));
end

[img_fn, img_fp] = uiputfile(img_fn,'Save image as...'); % directory and name of the file.

if (~isequal(img_fn,0) && ~isequal(img_fp,0))
    msgstr = sprintf('Saving image ...'); 
    set(handles.StatusMsgStr,'String',msgstr);
    drawnow();
    tic
%     if (~isequal(mod(size(bh_info(fidx).img_1,1),gridy), 0))
%         if (isequal(length(bh_info(fidx).frIdx), 0)) % If the data do not contain frame triggers.
%             a = bh_info(fidx).img_1(1:gridy,:);
%             [r, c] = size(a);
%             nlay = floor(size(bh_info(fidx).img_1,1)./gridy); % Number of frames
%             image = permute(reshape(a',[c,r/nlay,nlay]),[2,1,3]);
%         else
%             a = bh_info(fidx).img_1(1:gridy*(length(bh_info(fidx).frIdx) - 1),:);
%             [r, c] = size(a);
%             nlay = length(bh_info(fidx).frIdx) - 1;
%             image = permute(reshape(a',[c,r/nlay,nlay]),[2,1,3]);
%             % Convert the image into a stack. The last incomplete frame is deleted.
%             % https://www.mathworks.com/matlabcentral/answers/36563-reshaping-2d-matrix-into-3d-specific-ordering
%         end
%     else % If the last frame is complete.
%         a = bh_info(fidx).img_1;
%         [r, c] = size(a);
%         nlay = length(bh_info(fidx).frIdx);
%         image = permute(reshape(a',[c,r/nlay,nlay]),[2,1,3]);
%     % Convert the image into a stack. 
%     end
    options.message = false; % Show the message in the message box, not in the com window.
    options.overwrite = true;
%     if(isa(bh_info(fidx).img_1,'double'))
%         bh_info(fidx).img_1 = single(bh_info(fidx).img_1);  % Convert to single type because ImageJ does not support BitsPerSample of 64.
%     end
    image_save = UpdateImage(handles, bh_info(idx).img_1);  % Image format is changed.
    saveastiff(image_save,fullfile(img_fp,img_fn),options);
    t = toc;
    msgstr = sprintf('The image was saved successfully. Elapsed time: %.3f s',t); 
    set(handles.StatusMsgStr,'String',msgstr); 
end

% --- Executes on button press in imgMTBtn_2.
function imgMTBtn_2_Callback(hObject, eventdata, handles)
% hObject    handle to imgMTBtn_2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global bh_info;
idx = get(handles.DataFilesList,'Value');
filter_list = get(handles.FilterType_2, 'String'); % read the list of filters
filter_value = get(handles.FilterType_2, 'Value'); % which one is selected
filter_type = char(filter_list(filter_value));
% wiener1_value = get(handles.Wiener1,'Value'); % If or not doing Wiener filtering for STED1.
filter_px = str2double(get(handles.Filter2_px, 'String'));
% filter_px = uint8(filter_px); % size of filter in the unit of px.

if (~isempty(idx))
    set(handles.StatusMsgStr,'String','Calculating MT values and show STED2 image...'); 
    drawnow();
    
    tic
    
    if (~isempty(bh_info(idx).lnIdx)) %assign data events to lines
        bh_info(idx).img_2 = getImage(handles, 'img2', 'all'); % calculate the image matrix
        
        if(~strcmp(filter_type, 'No filter'))
            nlay = size(bh_info(idx).img_2, 3);
            for n = 1:1:nlay
                switch filter_type
                    case 'Wiener filter'
                        bh_info(idx).img_2(:,:,n) = wiener2(bh_info(idx).img_2(:,:,n), [filter_px, filter_px]);
                    case 'Gaussian filter'
                        bh_info(idx).img_2(:,:,n) = imgaussfilt(bh_info(idx).img_2(:,:,n), filter_px);
                    case 'Average filter'
                        bh_info(idx).img_2(:,:,n) = filter2(fspecial('average', filter_px), bh_info(idx).img_2(:,:,n));
                    case 'Median filter'
                        bh_info(idx).img_2(:,:,n) = medfilt2(bh_info(idx).img_2(:,:,n), [filter_px, filter_px]);
                end
            end
        end
        
        image = UpdateImage(handles, bh_info(idx).img_2);  % Image format is changed.
        showImage(image, 'ax2', handles); % artefacts corrected but data type not changed.
    end

    t = toc;
    msgstr = sprintf('Get STED2 image in %5.1f s',t); 
    set(handles.StatusMsgStr,'String',msgstr); 

end

function ImgTlow_2_Callback(hObject, eventdata, handles)
% hObject    handle to ImgTlow_2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of ImgTlow_2 as text
%        str2double(get(hObject,'String')) returns contents of ImgTlow_2 as a double


% --- Executes during object creation, after setting all properties.
function ImgTlow_2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to ImgTlow_2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function ImgThigh_2_Callback(hObject, eventdata, handles)
% hObject    handle to ImgThigh_2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of ImgThigh_2 as text
%        str2double(get(hObject,'String')) returns contents of ImgThigh_2 as a double


% --- Executes during object creation, after setting all properties.
function ImgThigh_2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to ImgThigh_2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes on button press in imgSaveBtn_2.
function imgSaveBtn_2_Callback(hObject, eventdata, handles)
% hObject    handle to imgSaveBtn_2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global bh_info;
Data_Type = get(handles.DataTypeList, 'Value');
idx = get(handles.DataFilesList,'Value');

% number of pixels per line
% gridx = str2double(get(handles.ImgGridX,'String'));
% number of lines per frame
% gridy = str2double(get(handles.ImgGridY,'String'))-1; % it does not to be global
% image = zeros(bh_info(idx).gridy, bh_info(idx).gridx, length(bh_info(idx).frIdx));
% The image stack to save.
low = get(handles.ImgTlow_2,'String');
high = get(handles.ImgThigh_2,'String');
% 
[fp,fn,~] = fileparts(bh_info(idx).filename);

% img_fn = fullfile(fp,strcat(fn,'_STED2_',low,'to',high,'.tif'));
switch Data_Type
    case 1
        img_fn = fullfile(fp,strcat(fn,'_STED2_',low,'to',high,'_original','.tif'));
    case 2
        img_fn = fullfile(fp,strcat(fn,'_STED2_',low,'to',high,'_uint8','.tif'));
    case 3
        img_fn = fullfile(fp,strcat(fn,'_STED2_',low,'to',high,'_uint16','.tif'));
    case 4
        img_fn = fullfile(fp,strcat(fn,'_STED2_',low,'to',high,'_single','.tif'));
end

[img_fn, img_fp] = uiputfile(img_fn,'Save image as...'); % directory and name of the file.
if (~isequal(img_fn,0) && ~isequal(img_fp,0))
    msgstr = sprintf('Saving image (s)');
    set(handles.StatusMsgStr,'String',msgstr); 
    drawnow();
    tic
%     if (~isequal(mod(size(bh_info(fidx).img_2,1),gridy), 0))
%         if (isequal(length(bh_info(fidx).frIdx), 0)) % If the data do not contain frame triggers.
%             a = bh_info(fidx).img_2(1:gridy,:);
%             [r, c] = size(a);
%             nlay = floor(size(bh_info(fidx).img_2,1)./gridy); % Number of frames
%             image = permute(reshape(a',[c,r/nlay,nlay]),[2,1,3]);
%         else
%             a = bh_info(fidx).img_2(1:gridy*(length(bh_info(fidx).frIdx) - 1),:);
%             [r, c] = size(a);
%             nlay = length(bh_info(fidx).frIdx) - 1;
%             image = permute(reshape(a',[c,r/nlay,nlay]),[2,1,3]);
%             % Convert the image into a stack. The last incomplete frame is deleted.
%             % https://www.mathworks.com/matlabcentral/answers/36563-reshaping-2d-matrix-into-3d-specific-ordering
%         end
%     else % If the last frame is complete.
%         a = bh_info(fidx).img_2;
%         [r, c] = size(a);
%         nlay = length(bh_info(fidx).frIdx);
%         image = permute(reshape(a',[c,r/nlay,nlay]),[2,1,3]);
%     % Convert the image into a stack. 
%     end
    options.message = false; % Show the message in the message box, not in the com window.
    options.overwrite = true;
%     if(isa(bh_info(fidx).img_2,'double'))
%         bh_info(fidx).img_2 = single(bh_info(fidx).img_2);  % Convert to single type because ImageJ does not support BitsPerSample of 64.
%     end
    image_save = UpdateImage(handles, bh_info(idx).img_2);  % Image format is changed.
    saveastiff(image_save,fullfile(img_fp,img_fn),options);
    t = toc;
    msgstr = sprintf('The file was saved successfully. Elapsed time: %.3f s',t); 
    set(handles.StatusMsgStr,'String',msgstr); 
end

% --- Executes on button press in imgMTBtn_3.
function imgMTBtn_3_Callback(hObject, eventdata, handles)
% hObject    handle to imgMTBtn_3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global bh_info;

idx = get(handles.DataFilesList,'Value');
gamma = str2double(get(handles.gamma,'String'));
% neg = get(handles.No_Neg, 'Value'); % whether or not show negative intensity.

filter_list = get(handles.FilterType_3, 'String'); % read the list of filters
filter_value = get(handles.FilterType_3, 'Value'); % which one is selected
filter_type = char(filter_list(filter_value));
% wiener1_value = get(handles.Wiener1,'Value'); % If or not doing Wiener filtering for STED1.
filter_px = str2double(get(handles.Filter3_px, 'String'));
% filter_px = uint8(filter_px); % size of filter in the unit of px.

neg = get(handles.NegCheck, 'Value'); % whether or not to neglect negative intensities to do FRC for the STEDD image.
islift = get(handles.LiftCheck, 'Value'); % whether STED1 image is lifted up to avoid negative counts in STEDD.

% number of pixels per line
% bh_info(idx).gridx = str2double(get(handles.ImgGridX,'String'));
% not needed here


if (~isempty(idx))
    set(handles.StatusMsgStr,'String','Calculating MT values and show image...'); 
    drawnow();
    
    tic
    
    if (~isempty(bh_info(idx).lnIdx)) %assign data events to lines
        
        bh_info(idx).img_3 = bh_info(idx).img_1 - gamma*bh_info(idx).img_2; 
        % be careful that bh_info(idx).img_1 and bh_info(idx).img_2 can be uint16!
        
        % ===== some operation to STEDD image =====
        if(neg) % neglect negative intensities to do decorrelation for the STEDD image. %
            bh_info(idx).img_3 = max(bh_info(idx).img_3, 0); % convert the negative intensity values before filtering.
        else
            if(islift)
                if(min(min(bh_info(idx).img_3)) < 0)
                    bh_info(idx).img_3 = bh_info(idx).img_3 - min(min(bh_info(idx).img_3));
                end
            end
        end
        
        % ===== filter image 3 -------
        if(~strcmp(filter_type, 'No filter'))
            nlay = size(bh_info(idx).img_3, 3);
            for n = 1:1:nlay
                switch filter_type
                    case 'Wiener filter'
                        bh_info(idx).img_3(:,:,n) = wiener2(bh_info(idx).img_3(:,:,n), [filter_px, filter_px]);
                    case 'Gaussian filter'
                        bh_info(idx).img_3(:,:,n) = imgaussfilt(bh_info(idx).img_3(:,:,n), filter_px);
                    case 'Average filter'
                        bh_info(idx).img_3(:,:,n) = filter2(fspecial('average', filter_px), bh_info(idx).img_3(:,:,n));
                    case 'Median filter'
                        bh_info(idx).img_3(:,:,n) = medfilt2(bh_info(idx).img_3(:,:,n), [filter_px, filter_px]);
                end
            end
        end
        
        image = UpdateImage(handles, bh_info(idx).img_3);  % Image format is changed. Negative intensities can be converted to zero.
        % but bh_info(idx).img_3 is unchanged!! 
        showImage(image, 'ax3', handles); % artefacts corrected but data type not changed. It is still double, but negative intensity values may have been converted to zero.
    end
    
    t = toc;
    msgstr = sprintf('Get image in %5.1f s',t);
    set(handles.StatusMsgStr,'String',msgstr); 
    
end


function gamma_Callback(hObject, eventdata, handles)
% hObject    handle to gamma (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of gamma as text
%        str2double(get(hObject,'String')) returns contents of gamma as a double

% --- Executes during object creation, after setting all properties.
function gamma_CreateFcn(hObject, eventdata, handles)
% hObject    handle to gamma (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes on button press in imgSaveBtn_3.
function imgSaveBtn_3_Callback(hObject, eventdata, handles)
% hObject    handle to imgSaveBtn_3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global bh_info;
Data_Type = get(handles.DataTypeList, 'Value'); % 1 is uinit16, 2 is single.
idx = get(handles.DataFilesList,'Value');

% number of pixels per line
% gridx = str2double(get(handles.ImgGridX,'String'));
% number of lines per frame
% gridy = str2double(get(handles.ImgGridY,'String'))-1; % It does not have to be global
% image = zeros(bh_info(idx).gridy, bh_info(idx).gridx, length(bh_info(idx).frIdx));
% The image stack to save.
gamma = get(handles.gamma,'String');
t1 = get(handles.ImgTlow_1,'String');
t2 = get(handles.ImgThigh_1,'String');
t3 = get(handles.ImgTlow_2,'String');
t4 = get(handles.ImgThigh_2,'String');

[fp,fn,~] = fileparts(bh_info(idx).filename);
switch Data_Type
    case 1
        img_fn = fullfile(fp,strcat(fn,'_STEDD_gamma_',gamma,'_',t1,'to',t2,'_',t3,'to',t4,'_original','.tif'));
    case 2
        img_fn = fullfile(fp,strcat(fn,'_STEDD_gamma_',gamma,'_',t1,'to',t2,'_',t3,'to',t4,'_uint8','.tif'));
    case 3
        img_fn = fullfile(fp,strcat(fn,'_STEDD_gamma_',gamma,'_',t1,'to',t2,'_',t3,'to',t4,'_uint16','.tif'));
    case 4
        img_fn = fullfile(fp,strcat(fn,'_STEDD_gamma_',gamma,'_',t1,'to',t2,'_',t3,'to',t4,'_single','.tif'));
end

[img_fn, img_fp] = uiputfile(img_fn,'Save image as...'); % directory and name of the file.
if (~isequal(img_fn,0) && ~isequal(img_fp,0))
    msgstr = sprintf('Saving image (s)');
    set(handles.StatusMsgStr,'String',msgstr);
    drawnow();
    tic
%     if (~isequal(mod(size(bh_info(fidx).img_3,1),gridy), 0))
%         if (isequal(length(bh_info(fidx).frIdx), 0)) % If the data do not contain frame triggers.
%             a = bh_info(fidx).img_3(1:gridy,:);
%             [r, c] = size(a);
%             nlay = floor(size(bh_info(fidx).img_3,1)./gridy); % Number of frames
%             image = permute(reshape(a',[c,r/nlay,nlay]),[2,1,3]);
%         else
%         a = bh_info(fidx).img_3(1:gridy*(length(bh_info(fidx).frIdx) - 1),:);
%         [r, c] = size(a);
%         nlay = length(bh_info(fidx).frIdx) - 1;
%         image = permute(reshape(a',[c,r/nlay,nlay]),[2,1,3]);
%         % Convert the image into a stack. The last incomplete frame is deleted.
%         % https://www.mathworks.com/matlabcentral/answers/36563-reshaping-2d-matrix-into-3d-specific-ordering
%         end
%     else % If the last frame is complete.
%         a = bh_info(fidx).img_3;
%         [r, c] = size(a);
%         nlay = length(bh_info(fidx).frIdx);
%         image = permute(reshape(a',[c,r/nlay,nlay]),[2,1,3]);
%     % Convert the image into a stack. 
%     end
    options.message = false; % Show the message in the message box, not in the com window.
    options.overwrite = true;
%     if(isa(bh_info(fidx).img_3,'double'))
%         % bh_info(fidx).img_3 = single(bh_info(fidx).img_3);  % Convert to single type because ImageJ does not support BitsPerSample of 64.
%         bh_info(fidx).img_3 = uint16(bh_info(fidx).img_3);  % Convert to uint16 type because ImageJ does not support BitsPerSample of 64.
%     end
    image_save = UpdateImage(handles, bh_info(idx).img_3);  % Image format is changed, but bh_info(idx).img_3 itself is not.
    saveastiff(image_save,fullfile(img_fp,img_fn),options);
    t = toc;
    msgstr = sprintf('The file was saved successfully. Elapsed time: %.3f s',t); 
    set(handles.StatusMsgStr,'String',msgstr); 
end

% --- Executes on button press in Get_gamma.
function Get_gamma_Callback(hObject, eventdata, handles)
% hObject    handle to Get_gamma (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Calculates gamma factor in the frequency domain using the method published in optics letters.

global bh_info; % Use the image data in figure 1 and figure 2.

idx = get(handles.DataFilesList,'Value');
FrID = str2double(get(handles.FrIDEdit,'String')); % The ID of frame that is going to show.
AllFr_value = get(handles.AllFrRadio,'Value'); % show all the frames or only show the middle frame.
range_gamma = (0.1:0.1:10)'; % range of gamma for calculation.

if (~isempty(idx))
    set(handles.StatusMsgStr,'String','Calculating gamma factor...'); 
    drawnow();
    
    tic
    
    if (~isempty(bh_info(idx).lnIdx)) %assign data events to lines
        if(AllFr_value)
            [gamma, C_gamma] = getGamma(handles, bh_info(idx).img_1, bh_info(idx).img_2, range_gamma);
        else
            if(FrID <= size(bh_info(idx).img_1, 3))
                [gamma, C_gamma] = getGamma(handles, bh_info(idx).img_1(:,:,FrID), bh_info(idx).img_2(:,:,FrID), range_gamma);
            else
                disp('Error! Frame ID larger than  the number of valid stacks!');
                msgstr = sprintf('Error! Frame ID larger than  the number of valid stacks!'); 
                set(handles.StatusMsgStr,'String',msgstr);
                drawnow();
            end
        end
        
        figure;
        plot(range_gamma, C_gamma, 's');
        xlabel('\gamma');
        ylabel('C(\gamma)');
        set(handles.gamma, 'String', num2str(gamma));
    end
    
%     if (~isempty(bh_info(idx).frIdx)) %assign photons to frames
%         
%     end
    
    t = toc;
    msgstr = sprintf('Get gamma factor in %5.1f s',t);
    set(handles.StatusMsgStr,'String',msgstr); 
    
end

function [gamma, C_gamma] = getGamma(handles, img1, img2, range)
% this function calculates the gamma factor for img1 and img2. It uses the
% opt lett method.  stedd image is not filtered even if a filter is
% selected for stedd. img1 and img2 are both 2D images.
% Please filter img1
% and img2 before calling this function if you want.
% range is a column vector of range of gamma.

Cutoff = str2double(get(handles.CutOff,'String')); % Cut off for integration.
neg = get(handles.NegCheck, 'Value'); % whether or not to cut negative intensity.
Judgement_matrix = zeros(length(range), 1);

for num = 1:length(range)
    img_STEDD = img1 - img2*range(num);
    if(neg)
        img_STEDD = max(img_STEDD, 0);
    end
    
    [Radius, Theta, Fp] = Fourier_Transform_polar_coordinate(img_STEDD);
    Fp(isnan(Fp)) = 0; % Remove NaN
    
    ind = find(Radius < Cutoff);
    ind = max(ind);
    Judgement_matrix(num) = sum(sum(abs(Fp(:,2:ind))).*Radius(2:ind))./(sum(sum(abs(Fp(:,2:ind))))*max(Radius)); % C(gamma) in the Opt Lett paper
    % max(Radius) = 1
end
C_gamma = Judgement_matrix; % C(gamma) in the Opt Lett paper as a function of gamma
index = find(Judgement_matrix == max(Judgement_matrix));
gamma = range(index(1)); 

% --- Executes on button press in FT_1.
% Fourier transform of the first image.
function FT_1_Callback(hObject, eventdata, handles)
% hObject    handle to FT_1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global bh_info; % Use the image data in figure 1 and figure 2.

idx = get(handles.DataFilesList,'Value');
FrID = str2double(get(handles.FrIDEdit,'String')); % The ID of frame that is going to show.
AllFr_value = get(handles.AllFrRadio,'Value'); % show all the frames or only show one frame

if (~isempty(idx))
    set(handles.StatusMsgStr,'String','Calculating Fourier transform...'); 
    drawnow();

    tic

    if (~isempty(bh_info(idx).lnIdx)) %assign data events to lines     
        if(AllFr_value)
            [Radius,Theta,Fp] = Fourier_Transform_polar_coordinate(bh_info(idx).img_1);
        else
            [Radius,Theta,Fp] = Fourier_Transform_polar_coordinate(bh_info(idx).img_1(:,:,FrID));
        end
        Fp(isnan(Fp)) = 0; % Remove NaN
        figure(1);
        subplot(2,1,1);
        imagesc(Radius,Theta,Fp);
        title('Spatial spectrum');
%         % caxis([0,1]); 
        xlabel('Spatial frequency (r/rmax)');
        ylabel('Spatial frequency Theta');
        axis([0 0.2 0 2*pi]);
        % colorbar;
        colormap('hot');

        F1D = sum(Fp); % Integrate over theta
        subplot(2,1,2);
        plot(Radius,F1D/max(F1D));
        title('Integrate over Theta');
        xlabel('Spatial frequency (r/rmax)');
        ylabel('Amplitude');
        xlim([Radius(2) 0.2]);
    end

    if (~isempty(bh_info(idx).frIdx)) %assign photons to frames

    end
end
    
t = toc;
msgstr = sprintf('Get Fourier transform in %5.1f s',t);
set(handles.StatusMsgStr,'String',msgstr); 

% --- Executes on button press in FT_2.
% Fourier transform of the second image.
function FT_2_Callback(hObject, eventdata, handles)
% hObject    handle to FT_2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global bh_info; % Use the image data in figure 1 and figure 2.

idx = get(handles.DataFilesList,'Value');
FrID = str2double(get(handles.FrIDEdit,'String')); % The ID of frame that is going to show.
AllFr_value = get(handles.AllFrRadio,'Value'); % show all the frames or only show the middle frame.

if (~isempty(idx))
    set(handles.StatusMsgStr,'String','Calculating Fourier transform...'); 
    drawnow();

    tic

    if (~isempty(bh_info(idx).lnIdx)) %assign data events to lines
        if(AllFr_value)
            [Radius,Theta,Fp] = Fourier_Transform_polar_coordinate(bh_info(idx).img_2);
        else
            [Radius,Theta,Fp] = Fourier_Transform_polar_coordinate(bh_info(idx).img_2(:,:,FrID));
        end
        Fp(isnan(Fp)) = 0; % Remove NaN
        figure(2);
        subplot(2,1,1);
        imagesc(Radius,Theta,Fp);
        title('Spatial spectrum');
%         % caxis([0,1]); 
        xlabel('Spatial frequency (r/rmax)');
        ylabel('Spatial frequency Theta');
        axis([0 0.2 0 2*pi]);
        % colorbar;
        colormap('hot');

        F1D = sum(Fp); % Integrate over theta
        subplot(2,1,2);
        plot(Radius,F1D/max(F1D));
        title('Integrate over Theta');
        xlabel('Spatial frequency (r/rmax)');
        ylabel('Amplitude');
        xlim([Radius(2) 0.2]);
    end

    if (~isempty(bh_info(idx).frIdx)) %assign photons to frames

    end
end
    
t = toc;
msgstr = sprintf('Get Fourier transform in %5.1f s',t);
set(handles.StatusMsgStr,'String',msgstr); 

% --- Executes on button press in FT_3.
function FT_3_Callback(hObject, eventdata, handles)
% hObject    handle to FT_3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global bh_info; % Use the image data in figure 1 and figure 2.

idx = get(handles.DataFilesList,'Value');
FrID = str2double(get(handles.FrIDEdit,'String')); % The ID of frame that is going to show.
AllFr_value = get(handles.AllFrRadio,'Value'); % show all the frames or only show the middle frame.
neg = get(handles.NegCheck, 'Value'); % whether or not to cut negative intensity.

if (~isempty(idx))
    set(handles.StatusMsgStr,'String','Calculating Fourier transform...'); 
    drawnow();

    tic

    if (~isempty(bh_info(idx).lnIdx)) %assign data events to lines
        if(neg)
            image_STEDD = max(bh_info(idx).img_3, 0);
        else
            image_STEDD = bh_info(idx).img_3;
        end
        
        if(AllFr_value)
            [Radius,Theta,Fp] = Fourier_Transform_polar_coordinate(image_STEDD);
        else
            [Radius,Theta,Fp] = Fourier_Transform_polar_coordinate(image_STEDD(:,:,FrID));
        end
        Fp(isnan(Fp)) = 0; % Remove NaN
        figure(3);
        subplot(2,1,1);
        imagesc(Radius,Theta,Fp);
        title('Spatial spectrum');
%         % caxis([0,1]); 
        xlabel('Spatial frequency (r/rmax)');
        ylabel('Spatial frequency Theta');
        axis([0 0.2 0 2*pi]);
        % colorbar;
        colormap('hot');

        F1D = sum(Fp); % Integrate over theta
        subplot(2,1,2);
        plot(Radius,F1D/max(F1D));
        title('Integrate over Theta');
        xlabel('Spatial frequency (r/rmax)');
        ylabel('Amplitude');
        xlim([Radius(2) 0.2]); % Spatial frequency of zero is not shown.
    end

    if (~isempty(bh_info(idx).frIdx)) %assign photons to frames

    end
end
    
t = toc;
msgstr = sprintf('Get Fourier transform in %5.1f s',t);
set(handles.StatusMsgStr,'String',msgstr); 

% --- Executes on button press in Wiener2.
function Wiener2_Callback(hObject, eventdata, handles)
% hObject    handle to Wiener2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of Wiener2

function ImgGridY_Callback(hObject, eventdata, handles)
% hObject    handle to ImgGridY (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of ImgGridY as text
%        str2double(get(hObject,'String')) returns contents of ImgGridY as a double

% --- Executes during object creation, after setting all properties.
function ImgGridY_CreateFcn(hObject, eventdata, handles)
% hObject    handle to ImgGridY (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function Image_shift_Callback(hObject, eventdata, handles)
% hObject    handle to Image_shift (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of Image_shift as text
%        str2double(get(hObject,'String')) returns contents of Image_shift as a double

% --- Executes during object creation, after setting all properties.
function Image_shift_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Image_shift (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes on button press in RadioLog.
function RadioLog_Callback(hObject, eventdata, handles)
% hObject    handle to RadioLog (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of RadioLog
global bh_info;

idx = get(handles.DataFilesList,'Value');
if (~isempty(idx))
    hData = zeros(bh_info(idx).timebins,2);
    hData(:,1) = (0:bh_info(idx).timebins-1)*bh_info(idx).timebase/bh_info(idx).timebins;

    if (~isempty(bh_info(idx).tADC))
        hData(:,2) = histc(bh_info(idx).tADC(bh_info(idx).phIdx),0:bh_info(idx).timebins-1);
    end

    subplot(handles.tHistAxis);
    log_value = get(handles.RadioLog,'Value'); % If or not showing the photon histogram in semilogy.

    if(log_value)
        semilogy(hData(:,1),hData(:,2));
        % ylim([1000 1.1*max(hData(:,2))]);
    else
        plot(hData(:,1),hData(:,2)); % Linear plot
    end

    xlim([hData(1,1) hData(end,1)]);
end

function AverEdit_Callback(hObject, eventdata, handles)
% hObject    handle to AverEdit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of AverEdit as text
%        str2double(get(hObject,'String')) returns contents of AverEdit as a double

% --- Executes during object creation, after setting all properties.
function AverEdit_CreateFcn(hObject, eventdata, handles)
% hObject    handle to AverEdit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes on button press in LnAveRadio.
function LnAveRadio_Callback(hObject, eventdata, handles)
% hObject    handle to LnAveRadio (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of LnAveRadio

% -- Executes on button press in LnPf_1.
function LnPf_1_Callback(hObject, eventdata, handles)

global bh_info;

idx = get(handles.DataFilesList,'Value');

if (~isempty(idx))

    px_size = str2double(get(handles.PxSizeEdit,'String')); % Pixel size in nm.
    [xi, yi] = getline(handles.imgAxis1); % get the position of the line that is drawn by the user.
    delta_x = max(xi) - min(xi);
    delta_y = max(yi) - min(yi);
    line_dist = round(sqrt(delta_y*delta_y+delta_x*delta_x)); % length of the line in px.
    line(handles.imgAxis1, xi, yi, 'color','white'); % draw the line on STED1 image.
    line(handles.imgAxis2, xi, yi, 'color','white'); % draw the line on STED2 image.

    dim = length(size(bh_info(idx).img_1)); % 2D or 3D image?
    if(dim == 2)
        STED1_profile = improfile(bh_info(idx).img_1, xi, yi, line_dist);
        STED2_profile = improfile(bh_info(idx).img_2, xi, yi, line_dist); % Intensities along the line;
        % The size of STED1_profile and STED2_profile is exactly line_dist;
    elseif(dim == 3)
        FrID = str2double(get(handles.FrIDEdit,'String')); % The ID of frame that is going to show.
        STED1_profile = improfile(bh_info(idx).img_1(:,:,FrID), xi, yi, line_dist);
        STED2_profile = improfile(bh_info(idx).img_2(:,:,FrID), xi, yi, line_dist); % Intensities along the line;
        % The size of STED1_profile and STED2_profile is exactly line_dist;
    end

    [max_count_1, loc_1] = max(STED1_profile); % maxmum value and its position of profile 1;
    [max_count_2, loc_2] = max(STED2_profile);

    XData_1 = 1:1:line_dist; % XData in px
    XData_1 = px_size*(XData_1 - loc_1); % move the zero positon to the max of the line profile and convert x data to real distance.
    XData_1 = XData_1';

    XData_2 = 1:1:line_dist; % XData in px
    XData_2 = px_size*(XData_2 - loc_2); % move the zero positon to the max of the line profile and convert x data to real distance.
    XData_2 = XData_2';

    % Gaussian fit STED2
    [fit_STED2, gof_STED2] = ProfileFit(STED2_profile, XData_2, 3, 0); % Fit model #3, single gaussian fit. gof: goodness of fitting
    STED2_values = coeffvalues(fit_STED2);
    sigma_STED2 = STED2_values(3);
    amp_STED2 = STED2_values(1);

    h = figure;
    
    % plot(profile_1);
    set(h,'Position',[300 100 1000 800]);
    subplot(2,2,1);
    plot(XData_2, STED2_profile, 'or');
    hold all
    plot(fit_STED2,'r-');
    legend('Data','Gaussian fit');
    xlabel('Position (nm)');
    ylabel('Intensity');
    title('STED2 Gaussian fit');
    text(XData_2(1), 0.8*max_count_2, strcat('{Amp: }',num2str(amp_STED2)))
    text(XData_2(1), 0.7*max_count_2, strcat('{Sigma: }',num2str(sigma_STED2), '{ nm}'))
    text(XData_2(1), 0.6*max_count_2, strcat('{FWHM: }',num2str(sigma_STED2*2.355), '{ nm}'))
    hold off
    
     % Double Gaussian fit STED1
    [fit_STED1, gof_STED1] = ProfileFit(STED1_profile, XData_1, 1, 0); % Fit model #1, double gaussian fit. gof: goodness of fitting
    STED1_values = coeffvalues(fit_STED1);
    sigma1_STED1 = STED1_values(3);
    amp1_STED1 = STED1_values(1);
    sigma2_STED1 = STED1_values(5);
    amp2_STED1 = STED1_values(4);
    
    subplot(2,2,2);
    plot(XData_1, STED1_profile, 'ob');
    hold all
    plot(fit_STED1,'b-');
    legend('Data','Double Gaussian fit');
    xlabel('Position (nm)');
    ylabel('Intensity');
    title('STED1 double Gaussian fit');
    text(XData_1(1), 0.9*max_count_1, strcat('{Amp1: }',num2str(amp1_STED1)));
    text(XData_1(1), 0.8*max_count_1, strcat('{Sigma1: }',num2str(sigma1_STED1), '{ nm}'));
    text(XData_1(1), 0.7*max_count_1, strcat('{FWHM1: }',num2str(sigma1_STED1*2.355), '{ nm}'));
    text(XData_1(1), 0.6*max_count_1, strcat('{Amp2: }',num2str(amp2_STED1)));
    text(XData_1(1), 0.5*max_count_1, strcat('{Sigma2: }',num2str(sigma2_STED1), '{ nm}'));
    text(XData_1(1), 0.4*max_count_1, strcat('{FWHM2: }',num2str(sigma2_STED1*2.355), '{ nm}'));
    hold off
    
    % Double Gaussian fit STED1 with fixed sigma2 value from fitting STED2
    % profile.
    [fit_STED1_fixed, gof_STED1_fixed] = ProfileFit(STED1_profile, XData_1, 2, sigma_STED2);
    STED1_fixed_values = coeffvalues(fit_STED1_fixed);
    sigma1_STED1_fixed = STED1_fixed_values(3);
    amp1_STED1_fixed = STED1_fixed_values(1);
    sigma2_STED1_fixed = STED1_fixed_values(5);
    amp2_STED1_fixed = STED1_fixed_values(4); % Amplitude of the broader Gaussian
    
    gamma = amp2_STED1_fixed/amp_STED2; % calculated gamma factor.
    set(handles.gamma,'String',num2str(gamma));
    
    subplot(2,2,3);
    plot(XData_1, STED1_profile, 'og');
    hold all
    plot(fit_STED1,'g-');
    legend('Data','Double Gaussian fit');
    xlabel('Position (nm)');
    ylabel('Intensity');
    title('STED1 double Gaussian fit, fixed sigma2');
    text(XData_1(1), 0.9*max_count_1, strcat('{Amp1: }',num2str(amp1_STED1_fixed)));
    text(XData_1(1), 0.8*max_count_1, strcat('{Sigma1: }',num2str(sigma1_STED1_fixed), '{ nm}'));
    text(XData_1(1), 0.7*max_count_1, strcat('{FWHM1: }',num2str(sigma1_STED1_fixed*2.355), '{ nm}'));
    text(XData_1(1), 0.6*max_count_1, strcat('{Amp2: }',num2str(amp2_STED1_fixed)));
    text(XData_1(1), 0.5*max_count_1, strcat('{Sigma2: }',num2str(sigma2_STED1_fixed), '{ nm}'));
    text(XData_1(1), 0.4*max_count_1, strcat('{FWHM2: }',num2str(sigma2_STED1_fixed*2.355), '{ nm}'));
    hold off
    
    % Fit STEDD profile
    STEDD_profile = STED1_profile - gamma*STED2_profile;
    [fit_STEDD, gof_STEDD] = ProfileFit(STEDD_profile, XData_1, 3, 0); % Fit model #3, single gaussian fit. gof: goodness of fitting
    STEDD_values = coeffvalues(fit_STEDD);
    sigma_STEDD = STEDD_values(3);
    amp_STEDD = STEDD_values(1);

    subplot(2,2,4);
    % plot(profile_1);
    plot(XData_1, STEDD_profile, 'om');
    hold all
    plot(fit_STEDD,'m-');
    legend('Data','Gaussian fit');
    xlabel('Position (nm)');
    ylabel('Intensity');
    title('STEDD Gaussian fit');
    text(XData_1(1), 0.8*max(STEDD_profile), strcat('{Amp: }',num2str(amp_STEDD)))
    text(XData_1(1), 0.7*max(STEDD_profile), strcat('{Sigma: }',num2str(sigma_STEDD), '{ nm}'))
    text(XData_1(1), 0.6*max(STEDD_profile), strcat('{FWHM: }',num2str(sigma_STEDD*2.355), '{ nm}'))
    hold off
end

% --- Executes on button press in LnPf_3.
function LnPf_3_Callback(hObject, eventdata, handles)
% hObject    handle to LnPf_3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global bh_info;

idx = get(handles.DataFilesList,'Value');
neg = get(handles.NegCheck, 'Value'); % whether or not to cut negative intensity.

if (~isempty(idx))
    
    px_size = str2double(get(handles.PxSizeEdit,'String')); % Pixel size in nm.
    [xi, yi] = getline(handles.imgAxis3); % get the position of the line that is drawn by the user.
    delta_x = max(xi) - min(xi);
    delta_y = max(yi) - min(yi);
    line_dist = round(sqrt(delta_y*delta_y+delta_x*delta_x)); % length of the line in px.
    line(handles.imgAxis3, xi, yi, 'color','white'); % draw the line on the image.
    if(neg)
        image_3 = max(bh_info(idx).img_3, 0);
    else
        image_3 = bh_info(idx).img_3;
    end
    
    dim = length(size(image_3)); % 2D or 3D image?
    if(dim == 2)
        STEDD_profile = improfile(image_3, xi, yi, line_dist);
    % Intensities along the line;
    % The size of STED1_profile and STED2_profile is exactly line_dist;
    elseif(dim == 3)
        FrID = str2double(get(handles.FrIDEdit,'String')); % The ID of frame that is going to show.
        STEDD_profile = improfile(image_3(:,:,FrID), xi, yi, line_dist);
    % Intensities along the line;
    % The size of STED1_profile and STED2_profile is exactly line_dist;
    end
    

    [max_count, loc] = max(STEDD_profile); % maxmum value and its position of profile;

    XData = 1:1:line_dist; % XData in px
    XData = px_size*(XData - loc); % move the zero positon to the max of the line profile and convert x data to real distance.
    XData = XData';

    % Gaussian fit STEDD
    [fit_STEDD, gof_STEDD] = ProfileFit(STEDD_profile, XData, 3, 0); % Fit model #3, single gaussian fit. gof: goodness of fitting
    STEDD_values = coeffvalues(fit_STEDD);
    sigma_STEDD = STEDD_values(3);
    amp_STEDD = STEDD_values(1);

    h = figure;
    
    set(h,'Position',[300 100 500 400]);
    plot(XData, STEDD_profile, 'or');
    hold all
    plot(fit_STEDD,'r-');
    legend('Data','Gaussian fit');
    xlabel('Position (nm)');
    ylabel('Intensity');
    title('STEDD Gaussian fit');
    text(XData(1), 0.8*max_count, strcat('{Amp: }',num2str(amp_STEDD)))
    text(XData(1), 0.7*max_count, strcat('{Sigma: }',num2str(sigma_STEDD), '{ nm}'))
    text(XData(1), 0.6*max_count, strcat('{FWHM: }',num2str(sigma_STEDD*2.355), '{ nm}'))
    hold off
    
end

function PxSizeEdit_Callback(hObject, eventdata, handles)
% hObject    handle to PxSizeEdit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of PxSizeEdit as text
%        str2double(get(hObject,'String')) returns contents of PxSizeEdit as a double


% --- Executes during object creation, after setting all properties.
function PxSizeEdit_CreateFcn(hObject, eventdata, handles)
% hObject    handle to PxSizeEdit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes on button press in AllFrRadio.
function AllFrRadio_Callback(hObject, eventdata, handles)
% hObject    handle to AllFrRadio (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of AllFrRadio

function FrIDEdit_Callback(hObject, eventdata, handles)
% hObject    handle to FrIDEdit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of FrIDEdit as text
%        str2double(get(hObject,'String')) returns contents of FrIDEdit as a double

% --- Executes during object creation, after setting all properties.
function FrIDEdit_CreateFcn(hObject, eventdata, handles)
% hObject    handle to FrIDEdit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function LeftCutEdit_Callback(hObject, eventdata, handles)
% hObject    handle to LeftCutEdit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of LeftCutEdit as text
%        str2double(get(hObject,'String')) returns contents of LeftCutEdit as a double

% --- Executes during object creation, after setting all properties.
function LeftCutEdit_CreateFcn(hObject, eventdata, handles)
% hObject    handle to LeftCutEdit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function RightCutEdit_Callback(hObject, eventdata, handles)
% hObject    handle to RightCutEdit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of RightCutEdit as text
%        str2double(get(hObject,'String')) returns contents of RightCutEdit as a double


% --- Executes during object creation, after setting all properties.
function RightCutEdit_CreateFcn(hObject, eventdata, handles)
% hObject    handle to RightCutEdit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function TopCutEdit_Callback(hObject, eventdata, handles)
% hObject    handle to TopCutEdit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of TopCutEdit as text
%        str2double(get(hObject,'String')) returns contents of TopCutEdit as a double

% --- Executes during object creation, after setting all properties.
function TopCutEdit_CreateFcn(hObject, eventdata, handles)
% hObject    handle to TopCutEdit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function BottomCutEdit_Callback(hObject, eventdata, handles)
% hObject    handle to BottomCutEdit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of BottomCutEdit as text
%        str2double(get(hObject,'String')) returns contents of BottomCutEdit as a double

% --- Executes during object creation, after setting all properties.
function BottomCutEdit_CreateFcn(hObject, eventdata, handles)
% hObject    handle to BottomCutEdit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function image = UpdateImage(handles, img)
% This function does not change the global image data: bh_info.img1,
% bh_info.img2 and bh_info.img3.
% global bh_info;
% idx = get(handles.DataFilesList,'Value');
neg = get(handles.NegCheck, 'Value'); % whether or not to cut negative intensity.
isnorm = get(handles.NormCheck, 'Value'); % whether or not to normalize the images.
Data_Type = get(handles.DataTypeList, 'Value');
% 1 is original, 2 is unit8, 3 is unit16 and 4 is single

if(~isempty(img))
    switch Data_Type
        case 1
            if(neg)
                img = max(img, 0); % convert all negative to zero.
            end
            image = img;
        case 2
            if(neg)
                if(isnorm)
                    img = 255*img/max(max(max(img)));
                end
            else
                if(isnorm)
                    img = 255*(img - min(min(min(img))))/(max(max(max(img))) - min(min(min(img))));
                end
            end
            image = uint8(img);
        case 3
            if(neg)
                if(isnorm)
                    img = 65535*img/max(max(max(img)));
                end
            else
                if(isnorm)
                    img = 65535*(img - min(min(min(img))))/(max(max(max(img))) - min(min(min(img))));
                end
            end
            image = uint16(img);
        case 4
            if(neg)
                img = max(img, 0); % convert all negative to zero.
            end
            image = single(img);
    end
end

% --- Executes on button press in UpdateImages.
% Update the frame of image to be shown.
function UpdateImages_Callback(hObject, eventdata, handles)
% hObject    handle to UpdateImages (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global bh_info;
idx = get(handles.DataFilesList,'Value');

if(~isempty(bh_info(idx).img_1))
    image1 = UpdateImage(handles, bh_info(idx).img_1);
    showImage(image1, 'ax1', handles);
end
if(~isempty(bh_info(idx).img_2))
    image2 = UpdateImage(handles, bh_info(idx).img_2);
    showImage(image2, 'ax2', handles);
end
if(~isempty(bh_info(idx).img_3))
    image3 = UpdateImage(handles, bh_info(idx).img_3);
    showImage(image3, 'ax3', handles);
end
% bh_info(idx).img_1 to 3 are not changed.

msgstr = sprintf('The images are updated'); 
set(handles.StatusMsgStr,'String',msgstr);
drawnow();


function CutOff_Callback(hObject, eventdata, handles)
% hObject    handle to CutOff (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of CutOff as text
%        str2double(get(hObject,'String')) returns contents of CutOff as a double


% --- Executes during object creation, after setting all properties.
function CutOff_CreateFcn(hObject, eventdata, handles)
% hObject    handle to CutOff (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% % --- Executes on button press in SaveData_1.
% function SaveData_1_Callback(hObject, eventdata, handles)
% % hObject    handle to SaveData_1 (see GCBO)
% % eventdata  reserved - to be defined in a future version of MATLAB
% % handles    structure with handles and user data (see GUIDATA)
% global bh_info;
% 
% idx = get(handles.DataFilesList,'Value');
% 
% % number of pixels per line
% % gridx = str2double(get(handles.ImgGridX,'String'));
% % number of lines per frame
% % gridy = str2double(get(handles.ImgGridY,'String')) - 1;
% % image = zeros(bh_info(idx).gridy, bh_info(idx).gridx, length(bh_info(idx).frIdx));
% % The image stack to save.
% 
% low = get(handles.ImgTlow_1,'String');
% high = get(handles.ImgThigh_1,'String'); % These are for the file name to save.
% % 
% [fp,fn,~] = fileparts(bh_info(idx).filename);
% 
% img_fn = fullfile(fp,strcat(fn,'_STED1_',low,'to',high,'.mat'));
% 
% [img_fn, img_fp] = uiputfile(img_fn,'Save data as...'); % directory and name of the file.
% 
% if (~isequal(img_fn,0) && ~isequal(img_fp,0))
%     msgstr = sprintf('Saving data ...'); 
%     set(handles.StatusMsgStr,'String',msgstr); 
%     tic
% %     if (~isequal(mod(size(bh_info(fidx).img_1,1),gridy), 0))
% %         if (isequal(length(bh_info(fidx).frIdx), 0)) % If the data do not contain frame triggers.
% %             a = bh_info(fidx).img_1(1:gridy,:);
% %             [r, c] = size(a);
% %             nlay = floor(size(bh_info(fidx).img_1,1)./gridy); % Number of frames
% %             image = permute(reshape(a',[c,r/nlay,nlay]),[2,1,3]);
% %         else
% %             a = bh_info(fidx).img_1(1:gridy*(length(bh_info(fidx).frIdx) - 1),:);
% %             [r, c] = size(a);
% %             nlay = length(bh_info(fidx).frIdx) - 1;
% %             image = permute(reshape(a',[c,r/nlay,nlay]),[2,1,3]);
% %             % Convert the image into a stack. The last incomplete frame is deleted.
% %             % https://www.mathworks.com/matlabcentral/answers/36563-reshaping-2d-matrix-into-3d-specific-ordering
% %         end
% %     else % If the last frame is complete.
% %         a = bh_info(fidx).img_1;
% %         [r, c] = size(a);
% %         nlay = length(bh_info(fidx).frIdx);
% %         image = permute(reshape(a',[c,r/nlay,nlay]),[2,1,3]);
% %     % Convert the image into a stack. 
% %     end
%     Image_1 = bh_info(idx).img_1;
%     save(fullfile(img_fp,img_fn), 'Image_1'); %  bh_info(fidx).img_1 cannot be directly saved.
%     t = toc;
%     msgstr = sprintf('The data were saved successfully. Elapsed time: %.3f s',t); 
%     set(handles.StatusMsgStr,'String',msgstr); 
% end

% % --- Executes on button press in SaveData_2.
% function SaveData_2_Callback(hObject, eventdata, handles)
% % hObject    handle to SaveData_2 (see GCBO)
% % eventdata  reserved - to be defined in a future version of MATLAB
% % handles    structure with handles and user data (see GUIDATA)
% global bh_info;
% 
% idx = get(handles.DataFilesList,'Value');
% 
% % number of pixels per line
% % gridx = str2double(get(handles.ImgGridX,'String'));
% % number of lines per frame
% % gridy = str2double(get(handles.ImgGridY,'String')) - 1;
% % image = zeros(bh_info(idx).gridy, bh_info(idx).gridx, length(bh_info(idx).frIdx));
% % The image stack to save.
% 
% low = get(handles.ImgTlow_2,'String');
% high = get(handles.ImgThigh_2,'String'); % These are for the file name to save.
% % 
% [fp,fn,~] = fileparts(bh_info(idx).filename);
% 
% img_fn = fullfile(fp,strcat(fn,'_STED2_',low,'to',high,'.mat'));
% 
% [img_fn, img_fp] = uiputfile(img_fn,'Save data as...'); % directory and name of the file.
% 
% if (~isequal(img_fn,0) && ~isequal(img_fp,0))
%     msgstr = sprintf('Saving data ...'); 
%     set(handles.StatusMsgStr,'String',msgstr); 
%     tic
% %     if (~isequal(mod(size(bh_info(fidx).img_1,1),gridy), 0))
% %         if (isequal(length(bh_info(fidx).frIdx), 0)) % If the data do not contain frame triggers.
% %             a = bh_info(fidx).img_1(1:gridy,:);
% %             [r, c] = size(a);
% %             nlay = floor(size(bh_info(fidx).img_1,1)./gridy); % Number of frames
% %             image = permute(reshape(a',[c,r/nlay,nlay]),[2,1,3]);
% %         else
% %             a = bh_info(fidx).img_1(1:gridy*(length(bh_info(fidx).frIdx) - 1),:);
% %             [r, c] = size(a);
% %             nlay = length(bh_info(fidx).frIdx) - 1;
% %             image = permute(reshape(a',[c,r/nlay,nlay]),[2,1,3]);
% %             % Convert the image into a stack. The last incomplete frame is deleted.
% %             % https://www.mathworks.com/matlabcentral/answers/36563-reshaping-2d-matrix-into-3d-specific-ordering
% %         end
% %     else % If the last frame is complete.
% %         a = bh_info(fidx).img_1;
% %         [r, c] = size(a);
% %         nlay = length(bh_info(fidx).frIdx);
% %         image = permute(reshape(a',[c,r/nlay,nlay]),[2,1,3]);
% %     % Convert the image into a stack. 
% %     end
%     Image_2 = bh_info(idx).img_2;
%     save(fullfile(img_fp,img_fn), 'Image_2'); %  bh_info(fidx).img_2 cannot be directly saved.
%     t = toc;
%     msgstr = sprintf('The data were saved successfully. Elapsed time: %.3f s',t); 
%     set(handles.StatusMsgStr,'String',msgstr); 
% end
% 
% % --- Executes on button press in SaveData_3.
% function SaveData_3_Callback(hObject, eventdata, handles)
% % hObject    handle to SaveData_3 (see GCBO)
% % eventdata  reserved - to be defined in a future version of MATLAB
% % handles    structure with handles and user data (see GUIDATA)
% global bh_info;
% 
% idx = get(handles.DataFilesList,'Value');
% 
% % number of pixels per line
% % gridx = str2double(get(handles.ImgGridX,'String'));
% % number of lines per frame
% % gridy = str2double(get(handles.ImgGridY,'String')) - 1;
% % image = zeros(bh_info(idx).gridy, bh_info(idx).gridx, length(bh_info(idx).frIdx));
% % The image stack to save.
% 
% gamma = get(handles.gamma,'String');
% % 
% [fp,fn,~] = fileparts(bh_info(idx).filename);
% 
% img_fn = fullfile(fp,strcat(fn,'_STEDD_gamma_',gamma,'.mat'));
% 
% [img_fn, img_fp] = uiputfile(img_fn,'Save data as...'); % directory and name of the file.
% 
% if (~isequal(img_fn,0) && ~isequal(img_fp,0))
%     msgstr = sprintf('Saving data ...'); 
%     set(handles.StatusMsgStr,'String',msgstr); 
%     tic
% %     if (~isequal(mod(size(bh_info(fidx).img_1,1),gridy), 0)0.)
% %         if (isequal(length(bh_info(fidx).frIdx), 0)) % If the data do not contain frame triggers.
% %             a = bh_info(fidx).img_1(1:gridy,:);
% %             [r, c] = size(a);
% %             nlay = floor(size(bh_info(fidx).img_1,1)./gridy); % Number of frames
% %             image = permute(reshape(a',[c,r/nlay,nlay]),[2,1,3]);
% %         else
% %             a = bh_info(fidx).img_1(1:gridy*(length(bh_info(fidx).frIdx) - 1),:);
% %             [r, c] = size(a);
% %             nlay = length(bh_info(fidx).frIdx) - 1;
% %             image = permute(reshape(a',[c,r/nlay,nlay]),[2,1,3]);
% %             % Convert the image into a stack. The last incomplete frame is deleted.
% %             % https://www.mathworks.com/matlabcentral/answers/36563-reshaping-2d-matrix-into-3d-specific-ordering
% %         end
% %     else % If the last frame is complete.
% %         a = bh_info(fidx).img_1;
% %         [r, c] = size(a);
% %         nlay = length(bh_info(fidx).frIdx);
% %         image = permute(reshape(a',[c,r/nlay,nlay]),[2,1,3]);
% %     % Convert the image into a stack. 
% %     end
%     Image_3 = bh_info(idx).img_3;
%     save(fullfile(img_fp,img_fn), 'Image_3'); %  bh_info(fidx).img_3 cannot be directly saved.
%     t = toc;
%     msgstr = sprintf('The data were saved successfully. Elapsed time: %.3f s',t); 
%     set(handles.StatusMsgStr,'String',msgstr); 
% end


% --- Executes on button press in SaveHist.
function SaveHist_Callback(hObject, eventdata, handles)
% hObject    handle to SaveHist (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global bh_info;
idx = get(handles.DataFilesList,'Value');
[fp,fn,~] = fileparts(bh_info(idx).filename);
img_fn = fullfile(fp,strcat(fn,'_photon_histogram','.mat'));

[img_fn, img_fp] = uiputfile(img_fn,'Save data as...'); % directory and name of the file.

if (~isequal(img_fn,0) && ~isequal(img_fp,0))
    msgstr = sprintf('Saving histogram ...'); 
    set(handles.StatusMsgStr,'String',msgstr); 
    drawnow();
    
    tic
    % Calculate the photon histogram.
    hData = zeros(bh_info(idx).timebins,2);
    hData(:,1) = (0:bh_info(idx).timebins-1)*bh_info(idx).timebase/bh_info(idx).timebins;

    if (~isempty(bh_info(idx).tADC))
        hData(:,2) = histc(bh_info(idx).tADC(bh_info(idx).phIdx),0:bh_info(idx).timebins-1);
    end
    
    save(fullfile(img_fp,img_fn), 'hData'); %  bh_info(fidx).img_3 cannot be directly saved.
    t = toc;
    msgstr = sprintf('The photon histogram was saved successfully. Elapsed time: %.3f s',t); 
    set(handles.StatusMsgStr,'String',msgstr); 
end

% --- Executes on button press in Odd_1.
function Odd_1_Callback(hObject, eventdata, handles)
% hObject    handle to Odd_1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global bh_info;

idx = get(handles.DataFilesList,'Value');
filter_list = get(handles.FilterType_1, 'String'); % read the list of filters
filter_value = get(handles.FilterType_1, 'Value'); % which one is selected
filter_type = char(filter_list(filter_value));
% wiener1_value = get(handles.Wiener1,'Value'); % If or not doing Wiener filtering for STED1.
filter_px = str2double(get(handles.Filter1_px, 'String'));
% filter_px = uint8(filter_px); % size of filter in the unit of px.

if (~isempty(idx))
    set(handles.StatusMsgStr,'String','Calculating MT values and show STED1 image in odd channels ...'); 
    drawnow();
    
    tic
    
    if (~isempty(bh_info(idx).lnIdx)) % assign data events to lines
        bh_info(idx).img_1 = getImage(handles, 'img1', 'odd'); % calculate the image matrix
        if(~strcmp(filter_type, 'No filter'))
            nlay = size(bh_info(idx).img_1, 3);
            for n = 1:1:nlay
                switch filter_type
                    case 'Wiener filter'
                        bh_info(idx).img_1(:,:,n) = wiener2(bh_info(idx).img_1(:,:,n), [filter_px, filter_px]);
                    case 'Gaussian filter'
                        bh_info(idx).img_1(:,:,n) = imgaussfilt(bh_info(idx).img_1(:,:,n), filter_px);
                    case 'Average filter'
                        bh_info(idx).img_1(:,:,n) = filter2(fspecial('average', filter_px), bh_info(idx).img_1(:,:,n));
                    case 'Median filter'
                        bh_info(idx).img_1(:,:,n) = medfilt2(bh_info(idx).img_1(:,:,n), [filter_px, filter_px]);
                end
            end
        end
        image = UpdateImage(handles, bh_info(idx).img_1);  % Image format is changed.
        showImage(image, 'ax1', handles); % artefacts corrected but data type not changed.
    end
    
    t = toc;
    msgstr = sprintf('Get image in odd channels in %5.1f s',t); 
    set(handles.StatusMsgStr,'String',msgstr); 
    
end


% --- Executes on button press in Even_1.
function Even_1_Callback(hObject, eventdata, handles)
% hObject    handle to Odd_1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global bh_info;

idx = get(handles.DataFilesList,'Value');
filter_list = get(handles.FilterType_1, 'String'); % read the list of filters
filter_value = get(handles.FilterType_1, 'Value'); % which one is selected
filter_type = char(filter_list(filter_value));
% wiener1_value = get(handles.Wiener1,'Value'); % If or not doing Wiener filtering for STED1.
filter_px = str2double(get(handles.Filter1_px, 'String'));
% filter_px = uint8(filter_px); % size of filter in the unit of px.

if (~isempty(idx))
    set(handles.StatusMsgStr,'String','Calculating MT values and show STED1 image in even channels ...'); 
    drawnow();
    
    tic
    
    if (~isempty(bh_info(idx).lnIdx)) % assign data events to lines
        bh_info(idx).img_1 = getImage(handles, 'img1', 'even'); % calculate the image matrix
        
        % filter the image
        if(~strcmp(filter_type, 'No filter'))
            nlay = size(bh_info(idx).img_1, 3);
            for n = 1:1:nlay
                switch filter_type
                    case 'Wiener filter'
                        bh_info(idx).img_1(:,:,n) = wiener2(bh_info(idx).img_1(:,:,n), [filter_px, filter_px]);
                    case 'Gaussian filter'
                        bh_info(idx).img_1(:,:,n) = imgaussfilt(bh_info(idx).img_1(:,:,n), filter_px);
                    case 'Average filter'
                        bh_info(idx).img_1(:,:,n) = filter2(fspecial('average', filter_px), bh_info(idx).img_1(:,:,n));
                    case 'Median filter'
                        bh_info(idx).img_1(:,:,n) = medfilt2(bh_info(idx).img_1(:,:,n), [filter_px, filter_px]);
                end
            end
        end
        
        image = UpdateImage(handles, bh_info(idx).img_1);  % Image format is changed.
        showImage(image, 'ax1', handles); % artefacts corrected but data type not changed.
    end
    
    t = toc;
    msgstr = sprintf('Get image in even channels in %5.1f s',t); 
    set(handles.StatusMsgStr,'String',msgstr); 
    
end

% --- Executes on button press in Odd_2.
function Odd_2_Callback(hObject, eventdata, handles)
% hObject    handle to Odd_1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global bh_info;
idx = get(handles.DataFilesList,'Value');
filter_list = get(handles.FilterType_2, 'String'); % read the list of filters
filter_value = get(handles.FilterType_2, 'Value'); % which one is selected
filter_type = char(filter_list(filter_value));
% wiener1_value = get(handles.Wiener1,'Value'); % If or not doing Wiener filtering for STED1.
filter_px = str2double(get(handles.Filter2_px, 'String'));
% filter_px = uint8(filter_px); % size of filter in the unit of px.

if (~isempty(idx))
    set(handles.StatusMsgStr,'String','Calculating MT values and show STED2 image in odd channels ...'); 
    drawnow();
    
    tic
    
    if (~isempty(bh_info(idx).lnIdx)) % assign data events to lines
        bh_info(idx).img_2 = getImage(handles, 'img2', 'odd'); % calculate the image matrix
        
        % ===== Filter the image =======
        if(~strcmp(filter_type, 'No filter'))
            nlay = size(bh_info(idx).img_2, 3);
            for n = 1:1:nlay
                switch filter_type
                    case 'Wiener filter'
                        bh_info(idx).img_2(:,:,n) = wiener2(bh_info(idx).img_2(:,:,n), [filter_px, filter_px]);
                    case 'Gaussian filter'
                        bh_info(idx).img_2(:,:,n) = imgaussfilt(bh_info(idx).img_2(:,:,n), filter_px);
                    case 'Average filter'
                        bh_info(idx).img_2(:,:,n) = filter2(fspecial('average', filter_px), bh_info(idx).img_2(:,:,n));
                    case 'Median filter'
                        bh_info(idx).img_2(:,:,n) = medfilt2(bh_info(idx).img_2(:,:,n), [filter_px, filter_px]);
                end
            end
        end
        
        image = UpdateImage(handles, bh_info(idx).img_2);  % Image format is changed.
        showImage(image, 'ax2', handles); % artefacts corrected but data type not changed.
    end
    
    t = toc;
    msgstr = sprintf('Get image in odd channels in %5.1f s',t); 
    set(handles.StatusMsgStr,'String',msgstr); 
    
end


% --- Executes on button press in Even_2.
function Even_2_Callback(hObject, eventdata, handles)
% hObject    handle to Even_2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global bh_info;
idx = get(handles.DataFilesList,'Value');
filter_list = get(handles.FilterType_2, 'String'); % read the list of filters
filter_value = get(handles.FilterType_2, 'Value'); % which one is selected
filter_type = char(filter_list(filter_value));
% wiener1_value = get(handles.Wiener1,'Value'); % If or not doing Wiener filtering for STED1.
filter_px = str2double(get(handles.Filter2_px, 'String'));
% filter_px = uint8(filter_px); % size of filter in the unit of px.

if (~isempty(idx))
    set(handles.StatusMsgStr,'String','Calculating MT values and show image in even channels ...'); 
    drawnow();
    
    tic
    
    if (~isempty(bh_info(idx).lnIdx)) % assign data events to lines
        bh_info(idx).img_2 = getImage(handles, 'img2', 'even'); % calculate the image matrix
        
        % ===== Filter the image =======
        if(~strcmp(filter_type, 'No filter'))
            nlay = size(bh_info(idx).img_2, 3);
            for n = 1:1:nlay
                switch filter_type
                    case 'Wiener filter'
                        bh_info(idx).img_2(:,:,n) = wiener2(bh_info(idx).img_2(:,:,n), [filter_px, filter_px]);
                    case 'Gaussian filter'
                        bh_info(idx).img_2(:,:,n) = imgaussfilt(bh_info(idx).img_2(:,:,n), filter_px);
                    case 'Average filter'
                        bh_info(idx).img_2(:,:,n) = filter2(fspecial('average', filter_px), bh_info(idx).img_2(:,:,n));
                    case 'Median filter'
                        bh_info(idx).img_2(:,:,n) = medfilt2(bh_info(idx).img_2(:,:,n), [filter_px, filter_px]);
                end
            end
        end
        
        image = UpdateImage(handles, bh_info(idx).img_2);  % Image format is changed.
        showImage(image, 'ax2', handles); % artefacts corrected but data type not changed.
    end
    
    t = toc;
    msgstr = sprintf('Get image in even channels in %5.1f s',t); 
    set(handles.StatusMsgStr,'String',msgstr); 
end

% --- Executes on button press in FRC_img1.
function FRC_img1_Callback(hObject, eventdata, handles)
% hObject    handle to FRC_img1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
set(handles.StatusMsgStr,'String','Calculating FRC values ... '); 
drawnow();
tic
save_frc_value_1 = get(handles.Save_Data_1, 'Value'); % Save the FRC results?
filter_list = get(handles.FilterType_1, 'String'); % read the list of filters
filter_value = get(handles.FilterType_1, 'Value'); % which one is selected
filter_type = char(filter_list(filter_value));
% wiener1_value = get(handles.Wiener1,'Value'); % If or not doing Wiener filtering for STED1.
filter_px = str2double(get(handles.Filter1_px, 'String'));
% filter_px = uint8(filter_px); % size of filter in the unit of px.

image_odd = getImage(handles, 'img1', 'odd');
image_even = getImage(handles, 'img1', 'even');

% filter the images
if(~strcmp(filter_type, 'No filter'))
    nlay = size(image_odd, 3); % number of layers
    for ii = 1:nlay
        switch filter_type
            case 'Wiener filter'
                image_odd(:,:,ii) = wiener2(image_odd(:,:,ii), [filter_px, filter_px]);
                image_even(:,:,ii) = wiener2(image_even(:,:,ii), [filter_px, filter_px]);
            case 'Gaussian filter'
                image_odd(:,:,ii) = imgaussfilt(image_odd(:,:,ii), filter_px);
                image_even(:,:,ii) = imgaussfilt(image_even(:,:,ii), filter_px);
            case 'Average filter'
                image_odd(:,:,ii) = filter2(fspecial('average', filter_px), image_odd(:,:,ii));
                image_even(:,:,ii) = filter2(fspecial('average', filter_px), image_even(:,:,ii));
            case 'Median filter'
                image_odd(:,:,ii) = medfilt2(image_odd(:,:,ii), [filter_px, filter_px]);
                image_even(:,:,ii) = medfilt2(image_even(:,:,ii), [filter_px, filter_px]);
        end
    end
end

px_size = str2double(get(handles.PxSizeEdit,'String')); % Pixel size in nm.
show_frc = 1; % whether or not show FRC curve

AllFr_value = get(handles.AllFrRadio,'Value'); % show all the frames or only show one of the frames.
% FRC only works for 2D images.

if(~AllFr_value)
    FrID = str2double(get(handles.FrIDEdit,'String')); % The ID of frame that is going to show.
    if(FrID <= size(image_odd,3))
        [res_value, frc_curve, res_high, res_low, frc_smooth] = imres_ims(image_odd(:,:,FrID),image_even(:,:,FrID),px_size,show_frc);
    else
        disp('Error! Frame ID larger than  the number of valid stacks!');
    end

else % Otherwise, show all the images.
    [res_value, frc_curve, res_high, res_low, frc_smooth] = imres_ims(image_odd,image_even,px_size,show_frc);
end

% % Plot FRC curve
% Now this is done in the core function
% qmax = 0.5/(px_size);
% figure
% hold on
% plot([0 qmax],[0 0],'k') % zero line
% plot(linspace(0,qmax*sqrt(2), length(frc_curve)), frc_curve,'-')
% plot([0 qmax],[1/7 1/7],'m')
% plot(1/(res_value),1/7,'rx'); % res_value has already taken pixel size into account
% plot(1/(res_value)*[1 1],[-0.2 1/7],'r')
% hold off
% xlim([0,qmax]);
% ylim([-0.2 1.2])
% xlabel('Spatial frequency (nm^{-1})');
% ylabel('FRC')
% txt = sprintf('FRC resolution: %2.1f +/- %2.2f nm.\n', res_value, (res_low - res_high)/2);
% text(qmax/3, 0.5, txt);

% Now save the FRC curve
if (save_frc_value_1)
    qmax = 0.5/(px_size);
    a = linspace(0,qmax*sqrt(2), length(frc_curve));
    FRC_data = [a' frc_curve' frc_smooth]; % FRC data to export
    global bh_info;
    idx = get(handles.DataFilesList,'Value');

    low = get(handles.ImgTlow_1,'String');
    high = get(handles.ImgThigh_1,'String'); % These are for the file name to save.
    % 
    [fp,fn,~] = fileparts(bh_info(idx).filename);

    img_fn = fullfile(fp,strcat(fn,'_STED1_',low,'to',high,'_FRC','.txt'));

    [img_fn, img_fp] = uiputfile(img_fn,'Save data as...'); % directory and name of the file.

    if (~isequal(img_fn,0) && ~isequal(img_fp,0))
        msgstr = sprintf('Saving data ...'); 
        set(handles.StatusMsgStr,'String',msgstr);
        drawnow();
        save(fullfile(img_fp,img_fn), 'FRC_data','-ascii'); %  bh_info(fidx).img_1 cannot be directly saved.
    end

    t = toc;
    msgstr = sprintf('Fourier Ring Correlation and saved in %5.1f s',t); 
    set(handles.StatusMsgStr,'String',msgstr);
    drawnow();
else
    t = toc;
    msgstr = sprintf('Fourier Ring Correlation in %5.1f s',t); 
    set(handles.StatusMsgStr,'String',msgstr);
    drawnow();
end

% --- Executes on button press in FRC_img2.
function FRC_img2_Callback(hObject, eventdata, handles)
% hObject    handle to FRC_img2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global bh_info;
set(handles.StatusMsgStr,'String','Calculating FRC values ... '); 
drawnow();
tic

save_frc_value_2 = get(handles.Save_Data_2, 'Value'); % Save the FRC results?
filter_list = get(handles.FilterType_2, 'String'); % read the list of filters
filter_value = get(handles.FilterType_2, 'Value'); % which one is selected
filter_type = char(filter_list(filter_value));
% wiener1_value = get(handles.Wiener1,'Value'); % If or not doing Wiener filtering for STED1.
filter_px = str2double(get(handles.Filter2_px, 'String'));
% filter_px = uint8(filter_px); % size of filter in the unit of px.

image_odd = getImage(handles, 'img2', 'odd');
image_even = getImage(handles, 'img2', 'even');

% filter the images
if(~strcmp(filter_type, 'No filter'))
    nlay = size(image_odd, 3); % number of layers
    for ii = 1:nlay
        switch filter_type
            case 'Wiener filter'
                image_odd(:,:,ii) = wiener2(image_odd(:,:,ii), [filter_px, filter_px]);
                image_even(:,:,ii) = wiener2(image_even(:,:,ii), [filter_px, filter_px]);
            case 'Gaussian filter'
                image_odd(:,:,ii) = imgaussfilt(image_odd(:,:,ii), filter_px);
                image_even(:,:,ii) = imgaussfilt(image_even(:,:,ii), filter_px);
            case 'Average filter'
                image_odd(:,:,ii) = filter2(fspecial('average', filter_px), image_odd(:,:,ii));
                image_even(:,:,ii) = filter2(fspecial('average', filter_px), image_even(:,:,ii));
            case 'Median filter'
                image_odd(:,:,ii) = medfilt2(image_odd(:,:,ii), [filter_px, filter_px]);
                image_even(:,:,ii) = medfilt2(image_even(:,:,ii), [filter_px, filter_px]);
        end
    end
end

px_size = str2double(get(handles.PxSizeEdit,'String')); % Pixel size in nm.
show_frc = 1; % whether or not show FRC curve
AllFr_value = get(handles.AllFrRadio,'Value'); % show all the frames or only show one of the frames.
% FRC only works for 2D images.

if(~AllFr_value)
    FrID = str2double(get(handles.FrIDEdit,'String')); % The ID of frame that is going to show.
    if(FrID <= size(image_odd,3))
        [~, frc_curve, ~, ~, frc_smooth] = imres_ims(image_odd(:,:,FrID),image_even(:,:,FrID),px_size,show_frc);
    else
        disp('Error! Frame ID larger than  the number of valid stacks!');
    end

else % Otherwise, show all the images.
    [~, frc_curve, ~, ~, frc_smooth] = imres_ims(image_odd,image_even,px_size,show_frc);
end

% Now save the FRC curve
if (save_frc_value_2)
    qmax = 0.5/px_size;
    a = linspace(0,qmax*sqrt(2), length(frc_curve)); % in the unit of 1/um
    FRC_data = [a' frc_curve' frc_smooth]; % FRC data to export
    idx = get(handles.DataFilesList,'Value');

    low = get(handles.ImgTlow_2,'String');
    high = get(handles.ImgThigh_2,'String'); % These are for the file name to save.
    % 
    [fp,fn,~] = fileparts(bh_info(idx).filename);

    img_fn = fullfile(fp,strcat(fn,'_STED2_',low,'to',high,'_FRC','.txt'));

    [img_fn, img_fp] = uiputfile(img_fn,'Save data as...'); % directory and name of the file.

    if (~isequal(img_fn,0) && ~isequal(img_fp,0))
        msgstr = sprintf('Saving data ...'); 
        set(handles.StatusMsgStr,'String',msgstr);
        drawnow();
        save(fullfile(img_fp,img_fn), 'FRC_data','-ascii'); %  bh_info(fidx).img_1 cannot be directly saved.
    end

    t = toc;
    msgstr = sprintf('Fourier Ring Correlation calculated and saved in %5.1f s',t); 
    set(handles.StatusMsgStr,'String',msgstr);
    drawnow();
else
    t = toc;
    msgstr = sprintf('Fourier Ring Correlation calculated in %5.1f s',t); 
    set(handles.StatusMsgStr,'String',msgstr);
    drawnow();
end
    

function gamma_range_1_Callback(hObject, eventdata, handles)
% hObject    handle to gamma_range_1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of gamma_range_1 as text
%        str2double(get(hObject,'String')) returns contents of gamma_range_1 as a double


% --- Executes during object creation, after setting all properties.
function gamma_range_1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to gamma_range_1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function gamma_range_2_Callback(hObject, eventdata, handles)
% hObject    handle to gamma_range_2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of gamma_range_2 as text
%        str2double(get(hObject,'String')) returns contents of gamma_range_2 as a double


% --- Executes during object creation, after setting all properties.
function gamma_range_2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to gamma_range_2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes on button press in FRC_gamma.
function FRC_gamma_Callback(hObject, eventdata, handles)
% hObject    handle to FRC_gamma (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% Calculate FRC for different gamma values and plot the resolution as a
% function of gamma.

global bh_info;

% Ask for the range of gamma for calculation
prompt = {'From: (>=2)','Step:', 'To'};
dlgtitle = 'Range of gamma for calculation';
dims = [1 35];
definput = {'0.1','0.1','4'}; % default values
gamma_range = inputdlg(prompt,dlgtitle,dims,definput); % cell array containing the inputs
gamma_range_low = str2num(gamma_range{1});
gamma_range_step = str2num(gamma_range{2});
gamma_range_high = str2num(gamma_range{3});

set(handles.StatusMsgStr,'String','Calculating FRC values and and the best gamma factor'); 
drawnow();
tic
save_frc_value_3 = get(handles.Save_Data_3, 'Value'); % Save FRC results?
px_size = str2double(get(handles.PxSizeEdit,'String')); % Pixel size in nm.

show_frc = 0; % whether or not show FRC curve
neg = get(handles.NegCheck, 'Value'); % whether or not to neglect negative intensities to do FRC for the STEDD image.
islift = get(handles.LiftCheck, 'Value'); % A constant offset is added to STEDD image to avoid negative counts.
% The amount of baseline added is just enough so that the minimum intensity in image_odd and
% image_even in STEDD is zero.

AllFr_value = get(handles.AllFrRadio,'Value'); % show all the frames or only show one of the frames.
% FRC only works for 2D images.

filter_list_1 = get(handles.FilterType_1, 'String'); % read the list of filters
filter_value_1 = get(handles.FilterType_1, 'Value'); % which one is selected
filter_type_1 = char(filter_list_1(filter_value_1));
% wiener1_value = get(handles.Wiener1,'Value'); % If or not doing Wiener filtering for STED1.
filter_px_1 = str2double(get(handles.Filter1_px, 'String'));
% filter_px = uint8(filter_px); % size of filter in the unit of px.

image_odd_1 = getImage(handles, 'img1', 'odd');
image_even_1 = getImage(handles, 'img1', 'even');

% filter the images of STED1
if(~strcmp(filter_type_1, 'No filter'))
    nlay = size(image_odd_1, 3); % number of layers
    for ii = 1:nlay
        switch filter_type_1
            case 'Wiener filter'
                image_odd_1(:,:,ii) = wiener2(image_odd_1(:,:,ii), [filter_px_1, filter_px_1]);
                image_even_1(:,:,ii) = wiener2(image_even_1(:,:,ii), [filter_px_1, filter_px_1]);
            case 'Gaussian filter'
                image_odd_1(:,:,ii) = imgaussfilt(image_odd_1(:,:,ii), filter_px_1);
                image_even_1(:,:,ii) = imgaussfilt(image_even_1(:,:,ii), filter_px_1);
            case 'Average filter'
                image_odd_1(:,:,ii) = filter2(fspecial('average', filter_px_1), image_odd_1(:,:,ii));
                image_even_1(:,:,ii) = filter2(fspecial('average', filter_px_1), image_even_1(:,:,ii));
            case 'Median filter'
                image_odd_1(:,:,ii) = medfilt2(image_odd_1(:,:,ii), [filter_px_1, filter_px_1]);
                image_even_1(:,:,ii) = medfilt2(image_even_1(:,:,ii), [filter_px_1, filter_px_1]);
        end
    end
end

filter_list_2 = get(handles.FilterType_2, 'String'); % read the list of filters
filter_value_2 = get(handles.FilterType_2, 'Value'); % which one is selected
filter_type_2 = char(filter_list_2(filter_value_2));
% wiener1_value = get(handles.Wiener1,'Value'); % If or not doing Wiener filtering for STED1.
filter_px_2 = str2double(get(handles.Filter2_px, 'String'));
% filter_px = uint8(filter_px); % size of filter in the unit of px.

image_odd_2 = getImage(handles, 'img2', 'odd');
image_even_2 = getImage(handles, 'img2', 'even');

% filter the images of STED2
if(~strcmp(filter_type_2, 'No filter'))
    nlay = size(image_odd_2, 3); % number of layers
    for ii = 1:nlay
        switch filter_type_2
            case 'Wiener filter'
                image_odd_2(:,:,ii) = wiener2(image_odd_2(:,:,ii), [filter_px_2, filter_px_2]);
                image_even_2(:,:,ii) = wiener2(image_even_2(:,:,ii), [filter_px_2, filter_px_2]);
            case 'Gaussian filter'
                image_odd_2(:,:,ii) = imgaussfilt(image_odd_2(:,:,ii), filter_px_2);
                image_even_2(:,:,ii) = imgaussfilt(image_even_2(:,:,ii), filter_px_2);
            case 'Average filter'
                image_odd_2(:,:,ii) = filter2(fspecial('average', filter_px_2), image_odd_2(:,:,ii));
                image_even_2(:,:,ii) = filter2(fspecial('average', filter_px_2), image_even_2(:,:,ii));
            case 'Median filter'
                image_odd_2(:,:,ii) = medfilt2(image_odd_2(:,:,ii), [filter_px_2, filter_px_2]);
                image_even_2(:,:,ii) = medfilt2(image_even_2(:,:,ii), [filter_px_2, filter_px_2]);
        end
    end
end

ratio = gamma_range_low:gamma_range_step:gamma_range_high; % ratio is just gamma
N_gamma = length(ratio);
C = linspecer(N_gamma); % for colors of the lines.

res_value = zeros(length(ratio), 1);
% frc_curve = zeros(length(ratio), 1);
res_high = zeros(length(ratio), 1);
res_low = zeros(length(ratio), 1);
frc_smooth = [];
frc_curve = [];

% ----- figure for showing smoothed FRC curves
f1 = figure('Name','FRC curves, STEDD','NumberTitle','off');
qmax = 0.5/px_size*1000; % space frequency in the unit of um^-1
hold on

plot([0 qmax],[1/7 1/7],'m') % in the unit of 1/um
% plot(1/(res_value),1/7,'rx'); % res_value has already taken pixel size into account
% plot(1/(res_value)*[1 1],[-0.2 1/7],'r')
plot([0 qmax],[0 0],'k') % zero line
xlim([0,qmax]);
ylim([-0.2 1.1])
xlabel('Spatial frequency (\mum^{-1})');
ylabel('FRC');
title('FRC curves, STEDD');

for ii = 1:length(ratio)
    image_odd = image_odd_1 - ratio(ii)*image_odd_2;
    image_even = image_even_1 - ratio(ii)*image_even_2;
    if(neg) % neglect negative intensities to do FRC for the STEDD image.
        image_odd = max(image_odd, 0);
        image_even = max(image_even, 0);
    else
        if(islift)
            if(min(min(image_odd)) < 0)
                image_odd = image_odd - min(min(image_odd));
            end
            if(min(min(image_even)) < 0)
                image_even = image_even - min(min(image_even));
            end
        end
    end
    
    % ==== do filtering to STEDD image before calculating FRC ========
    filter_list_3 = get(handles.FilterType_3, 'String'); % read the list of filters
    filter_value_3 = get(handles.FilterType_3, 'Value'); % which one is selected
    filter_type_3 = char(filter_list_3(filter_value_3));
    % wiener1_value = get(handles.Wiener1,'Value'); % If or not doing Wiener filtering for STED1.
    filter_px_3 = str2double(get(handles.Filter3_px, 'String'));
    % filter_px = uint8(filter_px); % size of filter in the unit of px.
    
    % filter the images
    if(~strcmp(filter_type_3, 'No filter'))
        nlay = size(image_odd, 3); % number of layers
        for ii = 1:nlay
            switch filter_type_3
                case 'Wiener filter'
                    image_odd(:,:,ii) = wiener2(image_odd(:,:,ii), [filter_px_3, filter_px_3]);
                    image_even(:,:,ii) = wiener2(image_even(:,:,ii), [filter_px_3, filter_px_3]);
                case 'Gaussian filter'
                    image_odd(:,:,ii) = imgaussfilt(image_odd(:,:,ii), filter_px_3);
                    image_even(:,:,ii) = imgaussfilt(image_even(:,:,ii), filter_px_3);
                case 'Average filter'
                    image_odd(:,:,ii) = filter2(fspecial('average', filter_px_3), image_odd(:,:,ii));
                    image_even(:,:,ii) = filter2(fspecial('average', filter_px_3), image_even(:,:,ii));
                case 'Median filter'
                    image_odd(:,:,ii) = medfilt2(image_odd(:,:,ii), [filter_px_3, filter_px_3]);
                    image_even(:,:,ii) = medfilt2(image_even(:,:,ii), [filter_px_3, filter_px_3]);
            end
        end
    end
    
    if(~AllFr_value)
        FrID = str2double(get(handles.FrIDEdit,'String')); % The ID of frame that is going to show.
        if(FrID <= size(image_odd, 3))
            [res_value(ii), frc_curve(:, ii), res_high(ii), res_low(ii), frc_smooth(:, ii)] = imres_ims(image_odd(:,:,FrID), image_even(:,:,FrID), px_size, show_frc);
        else
            disp('Error! Frame ID larger than  the number of valid stacks!');
        end

    else % Otherwise, show all the images.
        [res_value(ii), frc_curve(:, ii), res_high(ii), res_low(ii), frc_smooth(:, ii)] = imres_ims(image_odd, image_even, px_size, show_frc);
    end
    
    figure(f1);
    plot(linspace(0, qmax*sqrt(2), length(frc_smooth)), frc_smooth(:, ii),'color', C(ii,:),'LineWidth',1.5);
    
end

figure(f1);
colormap(C);
c = colorbar('Ticks', [0, 1],...
    'TickLabels', {num2str(ratio(1)), num2str(ratio(end))});
c.Label.String = 'Gamma';
set(findall(f1,'-property','FontSize'),'FontSize',14)

% Now plot FRC resolution against gamma factor.
f2 = figure('Name','FRC resolution','NumberTitle','off');
h = errorbar(ratio, res_value, res_value - res_low, res_high - res_value);
h.Marker = 'o';
h.MarkerSize = 8;
h.Color = 'r';
h.LineWidth = 1.5;
xlabel('Gamma factor');
ylabel('FRC resolution (nm)');
title('FRC resolution, STEDD');
set(findall(f2,'-property','FontSize'),'FontSize',14);
% index = find(res_value == min(res_value));
% set(handles.gamma,'String',num2str(ratio(index(1))));

% Now save the FRC curve
if (save_frc_value_3)
    a = linspace(0,qmax*sqrt(2), length(frc_curve)); % spatial frequency in the unit of 1/um.
    FRC_data = [a' frc_curve frc_smooth]; % FRC data to export
    FRC_res = [ratio' res_value res_high res_low]; % Resolution values to export.

    idx = get(handles.DataFilesList,'Value');

    t1 = get(handles.ImgTlow_1,'String');
    t2 = get(handles.ImgThigh_1,'String');
    t3 = get(handles.ImgTlow_2,'String');
    t4 = get(handles.ImgThigh_2,'String');
    % 
    [fp,fn,~] = fileparts(bh_info(idx).filename);

    img_fn = fullfile(fp,strcat(fn,'_STEDD_gamma_',num2str(gamma_range_low),'to', num2str(gamma_range_high),'_',t1,'to',t2,'_',t3,'to',t4,'_FRC','.txt'));

    [img_fn, img_fp] = uiputfile(img_fn,'Save data as...'); % directory and name of the file.

    if (~isequal(img_fn,0) && ~isequal(img_fp,0))
        msgstr = sprintf('Saving FRC curves ...'); 
        set(handles.StatusMsgStr,'String',msgstr);
        drawnow();
        save(fullfile(img_fp,img_fn), 'FRC_data','-ascii'); %  bh_info(fidx).img_1 cannot be directly saved.
    end

    [fp,fn,~] = fileparts(bh_info(idx).filename);

    img_fn = fullfile(fp,strcat(fn,'_STEDD_gamma_', num2str(gamma_range_low),'to', num2str(gamma_range_high),'_',t1,'to',t2,'_',t3,'to',t4,'_FRC_res','.txt'));

    [img_fn, img_fp] = uiputfile(img_fn,'Save data as...'); % directory and name of the file.

    if (~isequal(img_fn,0) && ~isequal(img_fp,0))
        msgstr = sprintf('Saving FRC resolutions ...'); 
        set(handles.StatusMsgStr,'String',msgstr);
        drawnow();
        save(fullfile(img_fp,img_fn), 'FRC_res','-ascii'); %  bh_info(fidx).img_1 cannot be directly saved.
    end

    t = toc;
    msgstr = sprintf('Calculated and saved FRC in %5.1f s',t); 
    set(handles.StatusMsgStr,'String',msgstr);
    drawnow();
else
    t = toc;
    msgstr = sprintf('Calculated FRC in %5.1f s',t); 
    set(handles.StatusMsgStr,'String',msgstr);
    drawnow();
end

% --- Executes on selection change in DataTypeList.
function DataTypeList_Callback(hObject, eventdata, handles)
% hObject    handle to DataTypeList (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns DataTypeList contents as cell array
%        contents{get(hObject,'Value')} returns selected item from DataTypeList


% --- Executes during object creation, after setting all properties.
function DataTypeList_CreateFcn(hObject, eventdata, handles)
% hObject    handle to DataTypeList (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in NormRadio.
function NormRadio_Callback(hObject, eventdata, handles)
% hObject    handle to NormRadio (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of NormRadio


% --- Executes on button press in NegCheck.
function NegCheck_Callback(hObject, eventdata, handles)
% hObject    handle to NegCheck (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of NegCheck


% --- Executes on button press in NormCheck.
function NormCheck_Callback(hObject, eventdata, handles)
% hObject    handle to NormCheck (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of NormCheck


% --- Executes on selection change in Rec_Soft.
function Rec_Soft_Callback(hObject, eventdata, handles)
% hObject    handle to Rec_Soft (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns Rec_Soft contents as cell array
%        contents{get(hObject,'Value')} returns selected item from Rec_Soft


% --- Executes during object creation, after setting all properties.
function Rec_Soft_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Rec_Soft (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function LiftEdit_Callback(hObject, eventdata, handles)
% hObject    handle to LiftEdit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of LiftEdit as text
%        str2double(get(hObject,'String')) returns contents of LiftEdit as a double


% --- Executes during object creation, after setting all properties.
function LiftEdit_CreateFcn(hObject, eventdata, handles)
% hObject    handle to LiftEdit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in LiftBtn.
function LiftBtn_Callback(hObject, eventdata, handles)
% hObject    handle to LiftBtn (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% This button adds a constant baseline to STED1 image, so that the STEDD
% image does not have negative counts.
% The global image matrix is changed.
global bh_info;
idx = get(handles.DataFilesList,'Value');
neg = get(handles.NegCheck, 'Value'); % whether or not to neglect negative intensities to do FRC for the STEDD image.
Data_Type = get(handles.DataTypeList, 'Value');
% 1 is original, 2 is unit8, 3 is unit16 and 4 is single
lift = str2double(get(handles.LiftEdit, 'String'));

if(Data_Type == 1 && neg == 0) % works only for original data type and negative counts are not omitted.
    if (~isempty(bh_info(idx).img_1))
        bh_info(idx).img_1 = bh_info(idx).img_1 + lift;
            % image = UpdateImage(handles, bh_info(idx).img_1);  % Image format is changed.
        showImage(bh_info(idx).img_1, 'ax1', handles); % artefacts corrected but data type not changed.
        msgstr = sprintf('A constant baseline was added to STED1 image.'); 
        set(handles.StatusMsgStr,'String',msgstr); 
    end
end
    
% --- Executes on button press in LiftCheck.
function LiftCheck_Callback(hObject, eventdata, handles)
% hObject    handle to LiftCheck (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of LiftCheck



function Filter2_px_Callback(hObject, eventdata, handles)
% hObject    handle to Filter2_px (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of Filter2_px as text
%        str2double(get(hObject,'String')) returns contents of Filter2_px as a double


% --- Executes during object creation, after setting all properties.
function Filter2_px_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Filter2_px (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in Wiener1.
function Wiener1_Callback(hObject, eventdata, handles)
% hObject    handle to Wiener1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of Wiener1



function Filter1_px_Callback(hObject, eventdata, handles)
% hObject    handle to Filter1_px (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of Filter1_px as text
%        str2double(get(hObject,'String')) returns contents of Filter1_px as a double


% --- Executes during object creation, after setting all properties.
function Filter1_px_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Filter1_px (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function AR_Callback(hObject, eventdata, handles)
% hObject    handle to AR (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of AR as text
%        str2double(get(hObject,'String')) returns contents of AR as a double


% --- Executes during object creation, after setting all properties.
function AR_CreateFcn(hObject, eventdata, handles)
% hObject    handle to AR (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in FRC_Filter_sted2.
function FRC_Filter_sted2_Callback(hObject, eventdata, handles)
% --- Calculates the FRC of STED2 image as a function of the size of Wiener
% filter applied.
% hObject    handle to FRC_Filter_sted2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% --- This bottom calculates the FRC of STED2 image as a function of the size of
% filter applied.

% === filter applied to STED2 image
filter_list_2 = get(handles.FilterType_2, 'String'); % read the list of filters
filter_value_2 = get(handles.FilterType_2, 'Value'); % which one is selected
filter_type_2 = char(filter_list_2(filter_value_2));

if(strcmp(filter_type_2, 'No filter'))
    % if no filter type is selected
    disp('Error! Please choose the type of filter!');
    msgstr = sprintf('Error! Please choose the type of filter!'); 
    set(handles.StatusMsgStr,'String',msgstr);
    drawnow();

else
    % Ask for the range for calculation
    prompt = {'From: (>=2)','Step:', 'To'};
    dlgtitle = 'Size of Wiener filter (px)';
    dims = [1 35];
    definput = {'3','3','48'}; % default values
    filter_range = inputdlg(prompt,dlgtitle,dims,definput); % cell array containing the inputs
    from = str2num(filter_range{1});
    step = str2num(filter_range{2});
    to = str2num(filter_range{3});

    save_frc_value_2 = get(handles.Save_Data_2, 'Value'); % Save the FRC results?

    set(handles.StatusMsgStr,'String','Calculating FRC values vs. the size of filter applied to STED2 ... '); 
    drawnow();
    tic

    px_size = str2double(get(handles.PxSizeEdit,'String')); % Pixel size in nm.
    show_frc = 0; % whether or not show FRC curve
    AllFr_value = get(handles.AllFrRadio,'Value'); % show all the frames or only show one of the frames.
    % FRC only works for 2D images.

    image_odd = getImage(handles, 'img2', 'odd');
    image_even = getImage(handles, 'img2', 'even');
    filter2_px = (from:step:to)'; % The minimum size is 2, otherwise the resultant image is NaN.
    N_filter = length(filter2_px);
    C = linspecer(N_filter); % for colors of the lines.

    res_value = zeros(length(filter2_px), 1);
    res_high = zeros(length(filter2_px), 1);
    res_low = zeros(length(filter2_px), 1);

    % Plot the smoothed FRC curves. Prepare the figure
    f1 = figure('Name','FRC curves, STED2','NumberTitle','off');
    qmax = 0.5/px_size*1000;
    plot([0 qmax],[0 0],'k'); % zero line
    hold on
    plot([0 qmax],[1/7 1/7],'m--');
    xlim([0,qmax]);
    ylim([-0.2 1.1]);
    xlabel('Spatial frequency (\mum^{-1})');
    ylabel('FRC');
    title('FRC curves, STED2');

    for ii = 1:length(filter2_px)

        if(~AllFr_value)
            FrID = str2double(get(handles.FrIDEdit,'String')); % The ID of frame that is going to show.
            if(FrID <= size(image_odd,3))
                switch filter_type_2
                    case 'Wiener filter'
            %             image_odd(:,:,FrID) = wiener2(image_odd(:,:,FrID), [filter2_px(ii) filter2_px(ii)]);
            %             image_even(:,:,FrID) = wiener2(image_even(:,:,FrID), [filter2_px(ii) filter2_px(ii)]);
            %             [res_value(ii), ~, res_high(ii), res_low(ii), ~] = imres_ims(image_odd(:,:,FrID), image_even(:,:,FrID), px_size, show_frc);
                        [res_value(ii), ~, res_high(ii), res_low(ii), frc_smooth(:, ii)] = imres_ims(wiener2(image_odd(:,:,FrID), [filter2_px(ii) filter2_px(ii)]),...
                            wiener2(image_even(:,:,FrID), [filter2_px(ii) filter2_px(ii)]), px_size, show_frc);
                    case 'Gaussian filter'
                        [res_value(ii), ~, res_high(ii), res_low(ii), frc_smooth(:, ii)] = imres_ims(imgaussfilt(image_odd(:,:,FrID), filter1_px(ii)),...
                        imgaussfilt(image_even(:,:,FrID), filter2_px(ii)), px_size, show_frc);
                    case 'Average filter'
                        [res_value(ii), ~, res_high(ii), res_low(ii), frc_smooth(:, ii)] = imres_ims(filter2(fspecial('average', filter2_px(ii)), image_odd(:,:,FrID)),...
                        filter2(fspecial('average', filter2_px(ii)), image_even(:,:,FrID)), px_size, show_frc);
                    case 'Median filter'
                        [res_value(ii), ~, res_high(ii), res_low(ii), frc_smooth(:, ii)] = imres_ims(medfilt2(image_odd(:,:,FrID), [filter2_px(ii) filter2_px(ii)]),...
                            medfilt2(image_even(:,:,FrID), [filter2_px(ii) filter2_px(ii)]), px_size, show_frc);
                end
            else
                disp('Error! Frame ID larger than  the number of valid stacks!');
                msgstr = sprintf('Error! Frame ID larger than  the number of valid stacks!'); 
                set(handles.StatusMsgStr,'String',msgstr);
                drawnow();
            end

        else % Otherwise, show all the images.
    %         image_odd = wiener2(image_odd, [filter2_px(ii) filter2_px(ii)]);
    %         image_even = wiener2(image_even, [filter2_px(ii) filter2_px(ii)]);
    %         % The two images should not be filtered again and again.
            % [res_value(ii), ~, res_high(ii), res_low(ii), ~] = imres_ims(image_odd,image_even,px_size,show_frc);
            switch filter_type_2
                case 'Wiener filter'
                    [res_value(ii), ~, res_high(ii), res_low(ii), frc_smooth(:, ii)] = imres_ims(wiener2(image_odd, [filter2_px(ii) filter2_px(ii)]),...
                        wiener2(image_even, [filter2_px(ii) filter2_px(ii)]),px_size,show_frc);
                case 'Gaussian filter'
                    [res_value(ii), ~, res_high(ii), res_low(ii), frc_smooth(:, ii)] = imres_ims(imgaussfilt(image_odd, filter2_px(ii)),...
                        imgaussfilt(image_even, filter2_px(ii)), px_size, show_frc);
                case 'Average filter'
                    [res_value(ii), ~, res_high(ii), res_low(ii), frc_smooth(:, ii)] = imres_ims(filter2(fspecial('average', filter2_px(ii)), image_odd),...
                        filter2(fspecial('average', filter2_px(ii)), image_even), px_size, show_frc);
                case 'Median filter'
                    [res_value(ii), ~, res_high(ii), res_low(ii), frc_smooth(:, ii)] = imres_ims(medfilt2(image_odd, [filter2_px(ii) filter2_px(ii)]),...
                        medfilt2(image_even, [filter2_px(ii) filter2_px(ii)]), px_size, show_frc);
            end
        end

        figure(f1); % Plot on the figure for FRC curves.
        plot(linspace(0,qmax*sqrt(2), length(frc_smooth(:, ii))), frc_smooth(:, ii),'color',C(ii,:),'LineWidth',1.5);

    end
    figure(f1);
    colormap(C);
    c = colorbar('Ticks', [0, 1],...
        'TickLabels', {num2str(filter2_px(1)), num2str(filter2_px(end))});
    c.Label.String = strcat({'Size of '}, filter_type_2);
    set(findall(f1,'-property','FontSize'),'FontSize',14)

    % Now plot FRC resolution against Wiener filter size.
    f2 = figure('Name','FRC resolution, STED2','NumberTitle','off');
    h = errorbar(filter2_px, res_value, res_value - res_low, res_high - res_value);
    h.Marker = 'o';
    h.MarkerSize = 8;
    h.Color = 'r';
    h.LineWidth = 1.5;
    xlabel(strcat({'Size of '}, filter_type_2));
    ylabel('FRC resolution (nm)');
    title('FRC resolution, STED2');
    set(findall(f2,'-property','FontSize'),'FontSize',14);

    t = toc;
    msgstr = sprintf('Fourier Ring Correlation calculated and saved in %5.1f s',t); 
    set(handles.StatusMsgStr,'String',msgstr);
    drawnow();

    % Now save the FRC curves
    if (save_frc_value_2)
        global bh_info;
        a = linspace(0,qmax*sqrt(2), length(frc_smooth)); % spatial frequency in the unit of 1/um.
        FRC_data = [a' frc_smooth]; % FRC data to export
        FRC_res = [filter2_px res_value res_high res_low]; % Resolution values to export.

        idx = get(handles.DataFilesList,'Value');

        t3 = get(handles.ImgTlow_2,'String');
        t4 = get(handles.ImgThigh_2,'String');
        % 
        [fp,fn,~] = fileparts(bh_info(idx).filename);
        
        switch filter_type_2
            case 'Wiener filter'
                img_fn = fullfile(fp,strcat(fn,'_STED2_',t3,'to',t4,'_Wiener_', filter_range{1}, 'to', filter_range{3}, '_FRC','.txt'));
            case 'Gaussian filter'
                img_fn = fullfile(fp,strcat(fn,'_STED2_',t3,'to',t4,'_Gaussian_', filter_range{1}, 'to', filter_range{3}, '_FRC','.txt'));
            case 'Average filter'
                img_fn = fullfile(fp,strcat(fn,'_STED2_',t3,'to',t4,'_average_', filter_range{1}, 'to', filter_range{3}, '_FRC','.txt'));
            case 'Median filter'
                img_fn = fullfile(fp,strcat(fn,'_STED2_',t3,'to',t4,'_median_', filter_range{1}, 'to', filter_range{3}, '_FRC','.txt'));
        end

        [img_fn, img_fp] = uiputfile(img_fn,'Save data as...'); % directory and name of the file.

        if (~isequal(img_fn,0) && ~isequal(img_fp,0))
            msgstr = sprintf('Saving FRC curves ...'); 
            set(handles.StatusMsgStr,'String',msgstr);
            drawnow();
            save(fullfile(img_fp,img_fn), 'FRC_data','-ascii'); %  bh_info(fidx).img_1 cannot be directly saved.
        end

        [fp,fn,~] = fileparts(bh_info(idx).filename);

        img_fn = fullfile(fp,strcat(fn,'_STED2_',t3,'to',t4,'_Wiener_', filter_range{1}, 'to', filter_range{3}, '_FRC_res','.txt'));

        [img_fn, img_fp] = uiputfile(img_fn,'Save data as...'); % directory and name of the file.

        if (~isequal(img_fn,0) && ~isequal(img_fp,0))
            msgstr = sprintf('Saving FRC resolutions ...'); 
            set(handles.StatusMsgStr,'String',msgstr);
            drawnow();
            save(fullfile(img_fp,img_fn), 'FRC_res','-ascii'); %  bh_info(fidx).img_1 cannot be directly saved.
        end

        t = toc;
        msgstr = sprintf('Calculated and saved FRC in %5.1f s',t); 
        set(handles.StatusMsgStr,'String',msgstr);
        drawnow();
    else
        t = toc;
        msgstr = sprintf('Calculated FRC in %5.1f s',t); 
        set(handles.StatusMsgStr,'String',msgstr);
        drawnow();
    end
end

% --- Executes on selection change in ImgRowTrig.
function ImgRowTrig_Callback(hObject, eventdata, handles)
% hObject    handle to ImgRowTrig (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns ImgRowTrig contents as cell array
%        contents{get(hObject,'Value')} returns selected item from ImgRowTrig


% --- Executes during object creation, after setting all properties.
function ImgRowTrig_CreateFcn(hObject, eventdata, handles)
% hObject    handle to ImgRowTrig (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in FRC_Filter_stedd_2.
function FRC_Filter_stedd_2_Callback(hObject, eventdata, handles)
% hObject    handle to FRC_Filter_stedd_2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% This button calculates FRC of the STEDD image as a
% function of the size of filter applied to STED2 image.

% === filter applied to STED2 image
filter_list_2 = get(handles.FilterType_2, 'String'); % read the list of filters
filter_value_2 = get(handles.FilterType_2, 'Value'); % which one is selected
filter_type_2 = char(filter_list_2(filter_value_2));

if(strcmp(filter_type_2, 'No filter'))
    % if no filter type is selected
    disp('Error! Please choose the type of filter for STED2!');
    msgstr = sprintf('Error! Please choose the type of filter for STED2!'); 
    set(handles.StatusMsgStr,'String',msgstr);
    drawnow();
    
else % Only if the type of filter for STED1 is select do we start the following calculations.
    % Ask for the range for calculation
    prompt = {'From: (>=2)','Step:', 'To'};
    dlgtitle = 'Size of filter applied to STED2 (px)';
    dims = [1 35];
    definput = {'3','3','48'}; % default values
    filter_range = inputdlg(prompt,dlgtitle,dims,definput); % cell array containing the inputs
    from = str2num(filter_range{1});
    step = str2num(filter_range{2});
    to = str2num(filter_range{3});
    
    % the size of filter applied to STED2 image
    filter2_px = (from:step:to)'; 
    N_filter = length(filter2_px);
    C = linspecer(N_filter); % for colors of the lines.

    set(handles.StatusMsgStr,'String','Calculating FRC of STEDD vs. the size of filter applied to STED2 ... '); 
    drawnow();
    tic

    %------ parameters setup --------
    save_frc_value_3 = get(handles.Save_Data_3, 'Value'); % Save FRC results?
    px_size = str2double(get(handles.PxSizeEdit,'String')); % Pixel size in nm.
    show_frc = 0; % whether or not show FRC curve
    neg = get(handles.NegCheck, 'Value'); % whether or not to neglect negative intensities to do FRC for the STEDD image.
    islift = get(handles.LiftCheck, 'Value'); % whether STED1 image is lifted up to avoid negative counts in STEDD.
    % The amount of baseline added is just enough so that the minimum intensity in image_odd and
    % image_even in STEDD is zero.
    AllFr_value = get(handles.AllFrRadio,'Value'); % show all the frames or only show one of the frames.
    % FRC only works for 2D images.
    
    % the type of filter applied to STED1 image
    filter_list_1 = get(handles.FilterType_1, 'String'); % read the list of filters
    filter_value_1 = get(handles.FilterType_1, 'Value'); % which one is selected
    filter_type_1 = char(filter_list_1(filter_value_1));
    % wiener1_value = get(handles.Wiener1,'Value'); % If or not doing Wiener filtering for STED1.
    filter_px_1 = str2double(get(handles.Filter1_px, 'String'));
    % filter_px = uint8(filter_px); % size of filter in the unit of px.

    gamma = str2double(get(handles.gamma,'String'));
    
    % the type of filter applied to STEDD image
    filter_list_3 = get(handles.FilterType_3, 'String'); % read the list of filters
    filter_value_3 = get(handles.FilterType_3, 'Value'); % which one is selected
    filter_type_3 = char(filter_list_3(filter_value_3));
    % wiener1_value = get(handles.Wiener1,'Value'); % If or not doing Wiener filtering for STED1.
    filter_px_3 = str2double(get(handles.Filter3_px, 'String'));
    % filter_px = uint8(filter_px); % size of filter in the unit of px.
    
    % ==== end of parameter setup. ======
    
    res_value = zeros(length(filter2_px), 1);
    % frc_curve = zeros(length(ratio), 1);
    res_high = zeros(length(filter2_px), 1);
    res_low = zeros(length(filter2_px), 1);

    % ---- reading images -----
    image_odd_2 = getImage(handles, 'img2', 'odd');
    image_even_2 = getImage(handles, 'img2', 'even');
    image_odd_2_filtered = zeros(size(image_odd_2, 1), size(image_odd_2, 2));
    image_even_2_filtered = zeros(size(image_even_2, 1), size(image_even_2, 2)); % filtered image, only one selected slice.

    image_odd_1 = getImage(handles, 'img1', 'odd');
    image_even_1 = getImage(handles, 'img1', 'even'); % Image data are already reshaped if not "show all frames".
    image_odd_1_filtered = zeros(size(image_odd_1, 1), size(image_odd_1, 2));
    image_even_1_filtered = zeros(size(image_even_1, 1), size(image_even_1, 2)); % filtered image, only one selected slice.
    
        % ==== % prepare figure for showing smoothed FRC curves
    f1 = figure('Name','FRC curves, STEDD','NumberTitle','off'); % figure for showing smoothed FRC curves
    qmax = 0.5/px_size*1000; % space frequency in the unit of um^-1
    hold on
    plot([0 qmax],[1/7 1/7],'m') % in the unit of 1/um
    % plot(1/(res_value),1/7,'rx'); % res_value has already taken pixel size into account
    % plot(1/(res_value)*[1 1],[-0.2 1/7],'r')
    plot([0 qmax],[0 0],'k') % zero line
    xlim([0, qmax]);
    ylim([-0.2 1.1])
    xlabel('Spatial frequency (\mum^{-1})');
    ylabel('FRC');
    title('FRC curves, STEDD');

    % ------ calculation ----------
    if(~AllFr_value) % if the checkbox is not checked, then work on the specified frame.
        FrID = str2double(get(handles.FrIDEdit,'String')); % The ID of frame that is going to show.
        if(FrID <= size(image_odd_1, 3)) % if the frame ID is not out of range.
        % filter the images of STED2, only the selected frame.
            switch filter_type_1
                case 'Wiener filter'
                    image_odd_1_filtered = wiener2(image_odd_1(:,:,FrID), [filter_px_1, filter_px_1]);
                    image_even_1_filtered = wiener2(image_even_1(:,:,FrID), [filter_px_1, filter_px_1]);
                case 'Gaussian filter'
                    image_odd_1_filtered = imgaussfilt(image_odd_1(:,:,FrID), filter_px_1);
                    image_even_1_filtered = imgaussfilt(image_even_1(:,:,FrID), filter_px_1);
                case 'Average filter'
                    image_odd_1_filtered = filter2(fspecial('average', filter_px_1), image_odd_1(:,:,FrID));
                    image_even_1_filtered = filter2(fspecial('average', filter_px_1), image_even_1(:,:,FrID));
                case 'Median filter'
                    image_odd_1_filtered = medfilt2(image_odd_1(:,:,FrID), [filter_px_1, filter_px_1]);
                    image_even_1_filtered = medfilt2(image_even_1(:,:,FrID), [filter_px_1, filter_px_1]);
                case 'No filter'
                    image_odd_1_filtered = image_odd_1(:,:,FrID);
                    image_even_1_filtered = image_even_1(:,:,FrID);
            end % end of filtering STED2.

            % ===== Start start the loop for STED2
            for ii = 1:length(filter2_px)
            %  =====     filter STED1 image
                switch filter_type_2
                    case 'Wiener filter'
                        image_odd_2_filtered = wiener2(image_odd_2(:,:,FrID), [filter2_px(ii), filter2_px(ii)]);
                        image_even_2_filtered = wiener2(image_even_2(:,:,FrID), [filter2_px(ii), filter2_px(ii)]);
                    case 'Gaussian filter'
                        image_odd_2_filtered = imgaussfilt(image_odd_2(:,:,FrID), filter2_px(ii));
                        image_even_2_filtered = imgaussfilt(image_even_2(:,:,FrID), filter2_px(ii));
                    case 'Average filter'
                        image_odd_2_filtered = filter2(fspecial('average', filter2_px(ii)), image_odd_2(:,:,FrID));
                        image_even_2_filtered = filter2(fspecial('average', filter2_px(ii)), image_even_2(:,:,FrID));
                    case 'Median filter'
                        image_odd_2_filtered = medfilt2(image_odd_2(:,:,FrID), [filter2_px(ii), filter2_px(ii)]);
                        image_even_2_filtered = medfilt2(image_even_2(:,:,FrID), [filter2_px(ii), filter2_px(ii)]);
                end

                image_odd = image_odd_1_filtered - gamma*image_odd_2_filtered;
                image_even = image_even_1_filtered - gamma*image_even_2_filtered; % STEDD image. 2D image. Refreshed in every loop.

                % ===== some operation to STEDD image =====
                if(neg) % neglect negative intensities to do FRC for the STEDD image.
                    image_odd = max(image_odd, 0);
                    image_even = max(image_even, 0);
                else
                    if(islift)
                        if(min(min(image_odd)) < 0)
                            image_odd = image_odd - min(min(image_odd));
                        end
                        if(min(min(image_even)) < 0)
                            image_even = image_even - min(min(image_even));
                        end
                    end
                end

                % ====== filter STEDD image ===============
                if(~strcmp(filter_type_3, 'No filter')) % if an option other than "No filter" is selected.
                    switch filter_type_3
                        case 'Wiener filter'
                            image_odd = wiener2(image_odd, [filter_px_3, filter_px_3]);
                            image_even = wiener2(image_even, [filter_px_3, filter_px_3]);
                        case 'Gaussian filter'
                            image_odd = imgaussfilt(image_odd, filter_px_3);
                            image_even = imgaussfilt(image_even, filter_px_3);
                        case 'Average filter'
                            image_odd = filter2(fspecial('average', filter_px_3), image_odd);
                            image_even = filter2(fspecial('average', filter_px_3), image_even);
                        case 'Median filter'
                            image_odd = medfilt2(image_odd, [filter_px_3, filter_px_3]);
                            image_even = medfilt2(image_even, [filter_px_3, filter_px_3]);
                    end
                end

                % ======= FRC calculation for STEDD ===================
                [res_value(ii), ~, res_high(ii), res_low(ii), frc_smooth(:, ii)] = imres_ims(image_odd, image_even, px_size, show_frc);

                % === update the smoothened FRC curve.
                figure(f1);
                plot(linspace(0, qmax*sqrt(2), length(frc_smooth(:, ii))), frc_smooth(:, ii),'color', C(ii,:),'LineWidth',1.5);
            
            end % End of the loop for filter size 1.
            
        else % if the FrID is out of range.
            disp('Error! Frame ID larger than  the number of valid stacks!');
            msgstr = sprintf('Error! Frame ID larger than  the number of valid stacks!'); 
            set(handles.StatusMsgStr,'String',msgstr);
            drawnow();
        end
    else % Otherwise, show all the images. If the checkbox is checked, then the frame number is meaningless.
        % filter the images of STED1
        switch filter_type_1
            case 'Wiener filter'
                image_odd_1_filtered = wiener2(image_odd_1, [filter_px_1, filter_px_1]);
                image_even_1_filtered = wiener2(image_even_1, [filter_px_1, filter_px_1]);
            case 'Gaussian filter'
                image_odd_1_filtered = imgaussfilt(image_odd_1, filter_px_1);
                image_even_1_filtered = imgaussfilt(image_even_1, filter_px_1);
            case 'Average filter'
                image_odd_1_filtered = filter2(fspecial('average', filter_px_1), image_odd_1);
                image_even_1_filtered = filter2(fspecial('average', filter_px_1), image_even_1);
            case 'Median filter'
                image_odd_1_filtered = medfilt2(image_odd_1, [filter_px_1, filter_px_1]);
                image_even_1_filtered = medfilt2(image_even_1, [filter_px_1, filter_px_1]);
            case 'No filter'
                image_odd_1_filtered = image_odd_1;
                image_even_1_filtered = image_even_1;
        end % end of filtering STED2.

        % ===== Start start the loop for STED2
        for ii = 1:length(filter2_px)
        %  =====     filter STED1 image
            switch filter_type_2
                case 'Wiener filter'
                    image_odd_2_filtered = wiener2(image_odd_2, [filter2_px(ii), filter2_px(ii)]);
                    image_even_2_filtered = wiener2(image_even_2, [filter2_px(ii), filter2_px(ii)]);
                case 'Gaussian filter'
                    image_odd_2_filtered = imgaussfilt(image_odd_2, filter2_px(ii));
                    image_even_2_filtered = imgaussfilt(image_even_2, filter2_px(ii));
                case 'Average filter'
                    image_odd_2_filtered = filter2(fspecial('average', filter2_px(ii)), image_odd_2);
                    image_even_2_filtered = filter2(fspecial('average', filter2_px(ii)), image_even_2);
                case 'Median filter'
                    image_odd_2_filtered = medfilt2(image_odd_2, [filter2_px(ii), filter2_px(ii)]);
                    image_even_2_filtered = medfilt2(image_even_2, [filter2_px(ii), filter2_px(ii)]);
            end

            image_odd = image_odd_1_filtered - gamma*image_odd_2_filtered;
            image_even = image_even_1_filtered - gamma*image_even_2_filtered; % STEDD image. 2D image. Refreshed in every loop.

            % ===== some operation to STEDD image =====
            if(neg) % neglect negative intensities to do FRC for the STEDD image.
                image_odd = max(image_odd, 0);
                image_even = max(image_even, 0);
            else
                if(islift)
                    if(min(min(image_odd)) < 0)
                        image_odd = image_odd - min(min(image_odd));
                    end
                    if(min(min(image_even)) < 0)
                        image_even = image_even - min(min(image_even));
                    end
                end
            end

            % ====== filter STEDD image ===============
            if(~strcmp(filter_type_3, 'No filter')) % if an option other than "No filter" is selected.
                switch filter_type_3
                    case 'Wiener filter'
                        image_odd = wiener2(image_odd, [filter_px_3, filter_px_3]);
                        image_even = wiener2(image_even, [filter_px_3, filter_px_3]);
                    case 'Gaussian filter'
                        image_odd = imgaussfilt(image_odd, filter_px_3);
                        image_even = imgaussfilt(image_even, filter_px_3);
                    case 'Average filter'
                        image_odd = filter2(fspecial('average', filter_px_3), image_odd);
                        image_even = filter2(fspecial('average', filter_px_3), image_even);
                    case 'Median filter'
                        image_odd = medfilt2(image_odd, [filter_px_3, filter_px_3]);
                        image_even = medfilt2(image_even, [filter_px_3, filter_px_3]);
                end
            end

            % ======= FRC calculation for STEDD ===================
            [res_value(ii), ~, res_high(ii), res_low(ii), frc_smooth(:, ii)] = imres_ims(image_odd, image_even, px_size, show_frc);

            % === update the smoothened FRC curve.
            figure(f1);
            plot(linspace(0, qmax*sqrt(2), length(frc_smooth(:, ii))), frc_smooth(:, ii),'color', C(ii,:),'LineWidth',1.5);
            
        end % End of the loop for filter size 1.
    end % end of "all frames"
    
    figure(f1);
    colormap(C);
    c = colorbar('Ticks', [0, 1],...
        'TickLabels', {num2str(filter2_px(1)), num2str(filter2_px(end))});
    c.Label.String = strcat({'Size of '}, filter_type_2, {' (px)'});
    set(findall(f1,'-property','FontSize'),'FontSize',14)

    % Now plot FRC resolution against filter size.
    f2 = figure('Name','FRC resolution, STEDD','NumberTitle','off');
    h = errorbar(filter2_px, res_value, res_value - res_low, res_high - res_value);
    h.Marker = 'o';
    h.MarkerSize = 8;
    h.Color = 'r';
    h.LineWidth = 1.5;
    xlabel(strcat(filter_type_2, ' size applied to STED2 image (px)'));
    ylabel('FRC resolution (nm)');
    title('FRC resolution, STEDD');
    set(findall(f2,'-property','FontSize'),'FontSize',14);

    % Now save the FRC curves
    if (save_frc_value_3)
        a = linspace(0,qmax*sqrt(2), length(frc_smooth)); % spatial frequency in the unit of 1/um.
        FRC_data = [a' frc_smooth]; % FRC data to export
        FRC_res = [filter2_px res_value res_high res_low]; % Resolution values to export.

        global bh_info;
        idx = get(handles.DataFilesList,'Value');

        t1 = get(handles.ImgTlow_1,'String');
        t2 = get(handles.ImgThigh_1,'String');
        t3 = get(handles.ImgTlow_2,'String');
        t4 = get(handles.ImgThigh_2,'String');
        % 
        [fp,fn,~] = fileparts(bh_info(idx).filename);

        % save smoothened FRC curves
        img_fn = fullfile(fp,strcat(fn,'_STEDD_gamma_',num2str(gamma),'_',t1,'to',t2,'_',t3,'to',t4,...
            '_STED2_',filter_type_2,'_',filter_range{1},'to',filter_range{3},'_FRC','.txt'));
        [img_fn, img_fp] = uiputfile(img_fn,'Save FRC curves as...'); % directory and name of the file.
        if (~isequal(img_fn,0) && ~isequal(img_fp,0))
            msgstr = sprintf('Saving FRC curves ...'); 
            set(handles.StatusMsgStr,'String',msgstr);
            drawnow();
            save(fullfile(img_fp,img_fn), 'FRC_data','-ascii');
        end
        
        % save FRC values.
        img_fn = fullfile(fp,strcat(fn,'_STEDD_gamma_',num2str(gamma),'_',t1,'to',t2,'_',t3,'to',t4,...
            '_STED2_', filter_type_2, '_', filter_range{1},'to',filter_range{3},'_FRC_res','.txt'));
        [img_fn, img_fp] = uiputfile(img_fn,'Save FRC ressolutions as...'); % directory and name of the file.
        if (~isequal(img_fn,0) && ~isequal(img_fp,0))
            msgstr = sprintf('Saving FRC resolutions ...'); 
            set(handles.StatusMsgStr,'String',msgstr);
            drawnow();
            save(fullfile(img_fp, img_fn), 'FRC_res','-ascii'); 
        end

        t = toc;
        msgstr = sprintf('Calculated and saved FRC in %5.1f s',t); 
        set(handles.StatusMsgStr,'String',msgstr);
        drawnow();
    else
        t = toc;
        msgstr = sprintf('Calculated FRC in %5.1f s',t); 
        set(handles.StatusMsgStr,'String',msgstr);
        drawnow();
    end
end


% --- Executes on button press in FRC_Filter_sted1.
function FRC_Filter_sted1_Callback(hObject, eventdata, handles)
% hObject    handle to FRC_Filter_sted1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% --- Calculates the FRC of STED1 image as a function of the size of
% filter applied.
% hObject    handle to FRC_Filter_sted1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% === filter applied to STED1 image
filter_list_1 = get(handles.FilterType_1, 'String'); % read the list of filters
filter_value_1 = get(handles.FilterType_1, 'Value'); % which one is selected
filter_type_1 = char(filter_list_1(filter_value_1));

if(strcmp(filter_type_1, 'No filter'))
    % if no filter type is selected
    disp('Error! Please choose the type of filter!');
    msgstr = sprintf('Error! Please choose the type of filter!'); 
    set(handles.StatusMsgStr,'String',msgstr);
    drawnow();
    
else
    % Ask for the range for calculation
    prompt = {'From: (>=2)','Step:', 'To'};
    dlgtitle = 'Size of Wiener filter (px)';
    dims = [1 35];
    definput = {'3','3','48'}; % default values
    filter_range = inputdlg(prompt, dlgtitle, dims, definput); % cell array containing the inputs
    from = str2num(filter_range{1});
    step = str2num(filter_range{2});
    to = str2num(filter_range{3});

    save_frc_value_1 = get(handles.Save_Data_1, 'Value'); % Save the FRC results?

    set(handles.StatusMsgStr,'String','Calculating FRC values vs. the size of filter for STED1 ... '); 
    drawnow();
    tic

    px_size = str2double(get(handles.PxSizeEdit,'String')); % Pixel size in nm.
    show_frc = 0; % whether or not show FRC curve
    AllFr_value = get(handles.AllFrRadio,'Value'); % show all the frames or only show one of the frames.
    % FRC only works for 2D images.

    image_odd = getImage(handles, 'img1', 'odd');
    image_even = getImage(handles, 'img1', 'even');
    filter1_px = (from:step:to)'; % The minimum size is 2, otherwise the resultant image is NaN.
    N_filter = length(filter1_px);
    C = linspecer(N_filter); % for colors of the lines.

    res_value = zeros(length(filter1_px), 1);
    res_high = zeros(length(filter1_px), 1);
    res_low = zeros(length(filter1_px), 1);

    % Plot the smoothed FRC curves. Prepare the figure
    f1 = figure('Name','FRC curves, STED1','NumberTitle','off');
    qmax = 0.5/px_size*1000;
    plot([0 qmax],[0 0],'k'); % zero line
    hold on
    plot([0 qmax],[1/7 1/7],'m--');
    xlim([0,qmax]);
    ylim([-0.2 1.1]);
    xlabel('Spatial frequency (\mum^{-1})');
    ylabel('FRC');
    title('FRC curves, STED1');

    for ii = 1:length(filter1_px)

        if(~AllFr_value)
            FrID = str2double(get(handles.FrIDEdit,'String')); % The ID of frame that is going to study. Note that only the selected frame is filtered.
            if(FrID <= size(image_odd, 3))
                switch filter_type_1
                    case 'Wiener filter'
        %             image_odd(:,:,FrID) = wiener2(image_odd(:,:,FrID), [filter2_px(ii) filter2_px(ii)]);
        %             image_even(:,:,FrID) = wiener2(image_even(:,:,FrID), [filter2_px(ii) filter2_px(ii)]);
        %             [res_value(ii), ~, res_high(ii), res_low(ii), ~] = imres_ims(image_odd(:,:,FrID), image_even(:,:,FrID), px_size, show_frc);
                        [res_value(ii), ~, res_high(ii), res_low(ii), frc_smooth(:, ii)] = imres_ims(wiener2(image_odd(:,:,FrID), [filter1_px(ii) filter1_px(ii)]),...
                            wiener2(image_even(:,:,FrID), [filter1_px(ii) filter1_px(ii)]), px_size, show_frc);
                    case 'Gaussian filter'
                        [res_value(ii), ~, res_high(ii), res_low(ii), frc_smooth(:, ii)] = imres_ims(imgaussfilt(image_odd(:,:,FrID), filter1_px(ii)),...
                        imgaussfilt(image_even(:,:,FrID), filter1_px(ii)), px_size, show_frc);
                    case 'Average filter'
                        [res_value(ii), ~, res_high(ii), res_low(ii), frc_smooth(:, ii)] = imres_ims(filter2(fspecial('average', filter1_px(ii)), image_odd(:,:,FrID)),...
                        filter2(fspecial('average', filter1_px(ii)), image_even(:,:,FrID)), px_size, show_frc);
                    case 'Median filter'
                        [res_value(ii), ~, res_high(ii), res_low(ii), frc_smooth(:, ii)] = imres_ims(medfilt2(image_odd(:,:,FrID), [filter1_px(ii) filter1_px(ii)]),...
                            medfilt2(image_even(:,:,FrID), [filter1_px(ii) filter1_px(ii)]), px_size, show_frc);
                end
            else
                disp('Error! Frame ID larger than  the number of valid stacks!');
                msgstr = sprintf('Error! Frame ID larger than  the number of valid stacks!'); 
                set(handles.StatusMsgStr,'String',msgstr);
                drawnow();
            end

        else % Otherwise, study all the images.
    %         image_odd = wiener2(image_odd, [filter2_px(ii) filter2_px(ii)]);
    %         image_even = wiener2(image_even, [filter2_px(ii) filter2_px(ii)]);
    %         % The two images should not be filtered again and again.
            % [res_value(ii), ~, res_high(ii), res_low(ii), ~] = imres_ims(image_odd,image_even,px_size,show_frc);
            switch filter_type_1
                case 'Wiener filter'
                    [res_value(ii), ~, res_high(ii), res_low(ii), frc_smooth(:, ii)] = imres_ims(wiener2(image_odd, [filter1_px(ii) filter1_px(ii)]),...
                        wiener2(image_even, [filter1_px(ii) filter1_px(ii)]),px_size,show_frc);
                case 'Gaussian filter'
                    [res_value(ii), ~, res_high(ii), res_low(ii), frc_smooth(:, ii)] = imres_ims(imgaussfilt(image_odd, filter1_px(ii)),...
                        imgaussfilt(image_even, filter1_px(ii)), px_size, show_frc);
                case 'Average filter'
                    [res_value(ii), ~, res_high(ii), res_low(ii), frc_smooth(:, ii)] = imres_ims(filter2(fspecial('average', filter1_px(ii)), image_odd),...
                        filter2(fspecial('average', filter1_px(ii)), image_even), px_size, show_frc);
                case 'Median filter'
                    [res_value(ii), ~, res_high(ii), res_low(ii), frc_smooth(:, ii)] = imres_ims(medfilt2(image_odd, [filter1_px(ii) filter1_px(ii)]),...
                        medfilt2(image_even, [filter1_px(ii) filter1_px(ii)]), px_size, show_frc);
            end
        end

        figure(f1); % Plot on the figure for FRC curves.
        plot(linspace(0,qmax*sqrt(2), length(frc_smooth(:, ii))), frc_smooth(:, ii),'color',C(ii,:),'LineWidth',1.5);

    end
    figure(f1);
    colormap(C);
    c = colorbar('Ticks', [0, 1],...
        'TickLabels', {num2str(filter1_px(1)), num2str(filter1_px(end))});
    c.Label.String = strcat({'Size of '}, filter_type_1);
    set(findall(f1,'-property','FontSize'),'FontSize',14)

    % Now plot FRC resolution against Wiener filter size.
    f2 = figure('Name','FRC resolution, STED1','NumberTitle','off');
    h = errorbar(filter1_px, res_value, res_value - res_low, res_high - res_value);
    h.Marker = 'o';
    h.MarkerSize = 8;
    h.Color = 'r';
    h.LineWidth = 1.5;
    xlabel(strcat({'Size of '}, filter_type_1));
    ylabel('FRC resolution (nm)');
    title('FRC resolution, STED1');
    set(findall(f2,'-property','FontSize'),'FontSize',14);

    t = toc;
    msgstr = sprintf('Fourier Ring Correlation calculated and saved in %5.1f s',t); 
    set(handles.StatusMsgStr,'String',msgstr);
    drawnow();

    % Now save the FRC curves
    if (save_frc_value_1)
        global bh_info;
        a = linspace(0,qmax*sqrt(2), length(frc_smooth)); % spatial frequency in the unit of 1/um.
        FRC_data = [a' frc_smooth]; % FRC data to export
        FRC_res = [filter1_px res_value res_high res_low]; % Resolution values to export.

        idx = get(handles.DataFilesList,'Value');

        t1 = get(handles.ImgTlow_1,'String');
        t2 = get(handles.ImgThigh_1,'String');
        % 
        [fp,fn,~] = fileparts(bh_info(idx).filename);

         switch filter_type_1
            case 'Wiener filter'
                img_fn = fullfile(fp,strcat(fn,'_STED2_',t3,'to',t4,'_Wiener_', filter_range{1}, 'to', filter_range{3}, '_FRC','.txt'));
            case 'Gaussian filter'
                img_fn = fullfile(fp,strcat(fn,'_STED2_',t3,'to',t4,'_Gaussian_', filter_range{1}, 'to', filter_range{3}, '_FRC','.txt'));
            case 'Average filter'
                img_fn = fullfile(fp,strcat(fn,'_STED2_',t3,'to',t4,'_average_', filter_range{1}, 'to', filter_range{3}, '_FRC','.txt'));
            case 'Median filter'
                img_fn = fullfile(fp,strcat(fn,'_STED2_',t3,'to',t4,'_median_', filter_range{1}, 'to', filter_range{3}, '_FRC','.txt'));
         end

        [img_fn, img_fp] = uiputfile(img_fn,'Save data as...'); % directory and name of the file.

        if (~isequal(img_fn,0) && ~isequal(img_fp,0))
            msgstr = sprintf('Saving FRC curves ...'); 
            set(handles.StatusMsgStr,'String',msgstr);
            drawnow();
            save(fullfile(img_fp,img_fn), 'FRC_data','-ascii'); %  bh_info(fidx).img_1 cannot be directly saved.
        end

        [fp,fn,~] = fileparts(bh_info(idx).filename);

        img_fn = fullfile(fp,strcat(fn,'_STED1_',t1,'to',t2,'_Wiener_', filter_range{1}, 'to', filter_range{3}, '_FRC_res','.txt'));

        [img_fn, img_fp] = uiputfile(img_fn,'Save data as...'); % directory and name of the file.

        if (~isequal(img_fn,0) && ~isequal(img_fp,0))
            msgstr = sprintf('Saving FRC resolutions ...'); 
            set(handles.StatusMsgStr,'String',msgstr);
            drawnow();
            save(fullfile(img_fp,img_fn), 'FRC_res','-ascii'); %  bh_info(fidx).img_1 cannot be directly saved.
        end

        t = toc;
        msgstr = sprintf('Calculated and saved FRC in %5.1f s',t); 
        set(handles.StatusMsgStr,'String',msgstr);
        drawnow();
    else
        t = toc;
        msgstr = sprintf('Calculated FRC in %5.1f s',t); 
        set(handles.StatusMsgStr,'String',msgstr);
        drawnow();
    end
end


% --- Executes on button press in FRC_Filter_stedd_1.
function FRC_Filter_stedd_1_Callback(hObject, eventdata, handles)
% hObject    handle to FRC_Filter_stedd_1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% This button calculates FRC of the STEDD image as a
% function of the size of Wiener filter applied to STED1 image.

% === filter applied to STED1 image
filter_list_1 = get(handles.FilterType_1, 'String'); % read the list of filters
filter_value_1 = get(handles.FilterType_1, 'Value'); % which one is selected
filter_type_1 = char(filter_list_1(filter_value_1));

if(strcmp(filter_type_1, 'No filter'))
    % if no filter type is selected
    disp('Error! Please choose the type of filter for STED1!');
    msgstr = sprintf('Error! Please choose the type of filter for STED1!'); 
    set(handles.StatusMsgStr,'String',msgstr);
    drawnow();
    
else % Only if the type of filter for STED1 is select do we start the following calculations.
    % Ask for the range for calculation
    prompt = {'From: (>=2)','Step:', 'To'};
    dlgtitle = 'Size of filter applied to STED1 (px)';
    dims = [1 35];
    definput = {'3','3','48'}; % default values
    filter_range = inputdlg(prompt,dlgtitle,dims,definput); % cell array containing the inputs
    from = str2num(filter_range{1});
    step = str2num(filter_range{2});
    to = str2num(filter_range{3});
    
    % the size of filter applied to STED1 image
    filter1_px = (from:step:to)'; 
    N_filter = length(filter1_px);
    C = linspecer(N_filter); % for colors of the lines.

    set(handles.StatusMsgStr,'String','Calculating FRC of STEDD vs. the size of filter applied to STED1 ... '); 
    drawnow();
    tic

    %------ parameters setup --------
    save_frc_value_3 = get(handles.Save_Data_3, 'Value'); % Save FRC results?
    px_size = str2double(get(handles.PxSizeEdit,'String')); % Pixel size in nm.
    show_frc = 0; % whether or not show FRC curve
    neg = get(handles.NegCheck, 'Value'); % whether or not to neglect negative intensities to do FRC for the STEDD image.
    islift = get(handles.LiftCheck, 'Value'); % whether STED1 image is lifted up to avoid negative counts in STEDD.
    % The amount of baseline added is just enough so that the minimum intensity in image_odd and
    % image_even in STEDD is zero.
    AllFr_value = get(handles.AllFrRadio,'Value'); % show all the frames or only show one of the frames.
    % FRC only works for 2D images.
    
    % the type of filter applied to STED2 image
    filter_list_2 = get(handles.FilterType_2, 'String'); % read the list of filters
    filter_value_2 = get(handles.FilterType_2, 'Value'); % which one is selected
    filter_type_2 = char(filter_list_2(filter_value_2));
    % wiener1_value = get(handles.Wiener1,'Value'); % If or not doing Wiener filtering for STED1.
    filter_px_2 = str2double(get(handles.Filter2_px, 'String'));
    % filter_px = uint8(filter_px); % size of filter in the unit of px.

    gamma = str2double(get(handles.gamma,'String'));
    
    % the type of filter applied to STEDD image
    filter_list_3 = get(handles.FilterType_3, 'String'); % read the list of filters
    filter_value_3 = get(handles.FilterType_3, 'Value'); % which one is selected
    filter_type_3 = char(filter_list_3(filter_value_3));
    % wiener1_value = get(handles.Wiener1,'Value'); % If or not doing Wiener filtering for STED1.
    filter_px_3 = str2double(get(handles.Filter3_px, 'String'));
    % filter_px = uint8(filter_px); % size of filter in the unit of px.
    
    % ==== end of parameter setup. ======
    
    res_value = zeros(length(filter1_px), 1);
    % frc_curve = zeros(length(ratio), 1);
    res_high = zeros(length(filter1_px), 1);
    res_low = zeros(length(filter1_px), 1);

    % ---- reading images -----
    image_odd_1 = getImage(handles, 'img1', 'odd');
    image_even_1 = getImage(handles, 'img1', 'even');
    image_odd_1_filtered = zeros(size(image_odd_1, 1), size(image_odd_1, 2));
    image_even_1_filtered = zeros(size(image_even_1, 1), size(image_even_1, 2)); % filtered image, only one selected slice.

    image_odd_2 = getImage(handles, 'img2', 'odd');
    image_even_2 = getImage(handles, 'img2', 'even'); % Image data are already reshaped if not "show all frames".
    image_odd_2_filtered = zeros(size(image_odd_2, 1), size(image_odd_2, 2));
    image_even_2_filtered = zeros(size(image_even_2, 1), size(image_even_2, 2)); % filtered image, only one selected slice.
    
        % ==== % prepare figure for showing smoothed FRC curves
    f1 = figure('Name','FRC curves, STEDD','NumberTitle','off'); % figure for showing smoothed FRC curves
    qmax = 0.5/px_size*1000; % space frequency in the unit of um^-1
    hold on
    plot([0 qmax],[1/7 1/7],'m') % in the unit of 1/um
    % plot(1/(res_value),1/7,'rx'); % res_value has already taken pixel size into account
    % plot(1/(res_value)*[1 1],[-0.2 1/7],'r')
    plot([0 qmax],[0 0],'k') % zero line
    xlim([0, qmax]);
    ylim([-0.2 1.1])
    xlabel('Spatial frequency (\mum^{-1})');
    ylabel('FRC');
    title('FRC curves, STEDD');

    % ------ calculation ----------
    if(~AllFr_value) % if the checkbox is not checked, then work on the specified frame.
        FrID = str2double(get(handles.FrIDEdit,'String')); % The ID of frame that is going to show.
        if(FrID <= size(image_odd_2, 3)) % if the frame ID is not out of range.
        % filter the images of STED2, only the selected frame.
            switch filter_type_2
                case 'Wiener filter'
                    image_odd_2_filtered = wiener2(image_odd_2(:,:,FrID), [filter_px_2, filter_px_2]);
                    image_even_2_filtered = wiener2(image_even_2(:,:,FrID), [filter_px_2, filter_px_2]);
                case 'Gaussian filter'
                    image_odd_2_filtered = imgaussfilt(image_odd_2(:,:,FrID), filter_px_2);
                    image_even_2_filtered = imgaussfilt(image_even_2(:,:,FrID), filter_px_2);
                case 'Average filter'
                    image_odd_2_filtered = filter2(fspecial('average', filter_px_2), image_odd_2(:,:,FrID));
                    image_even_2_filtered = filter2(fspecial('average', filter_px_2), image_even_2(:,:,FrID));
                case 'Median filter'
                    image_odd_2_filtered = medfilt2(image_odd_2(:,:,FrID), [filter_px_2, filter_px_2]);
                    image_even_2_filtered = medfilt2(image_even_2(:,:,FrID), [filter_px_2, filter_px_2]);
                case 'No filter'
                  image_odd_2_filtered = image_odd_2(:,:,FrID);
                    image_even_2_filtered = image_even_2(:,:,FrID);  
            end % end of filtering STED2.

            % ===== Start start the loop for STED1
            for ii = 1:length(filter1_px)
            %  =====     filter STED1 image
                switch filter_type_1
                    case 'Wiener filter'
                        image_odd_1_filtered= wiener2(image_odd_1(:,:,FrID), [filter1_px(ii), filter1_px(ii)]);
                        image_even_1_filtered = wiener2(image_even_1(:,:,FrID), [filter1_px(ii), filter1_px(ii)]);
                    case 'Gaussian filter'
                        image_odd_1_filtered = imgaussfilt(image_odd_1(:,:,FrID), filter1_px(ii));
                        image_even_1_filtered = imgaussfilt(image_even_1(:,:,FrID), filter1_px(ii));
                    case 'Average filter'
                        image_odd_1_filtered = filter2(fspecial('average', filter1_px(ii)), image_odd_1(:,:,FrID));
                        image_even_1_filtered = filter2(fspecial('average', filter1_px(ii)), image_even_1(:,:,FrID));
                    case 'Median filter'
                        image_odd_1_filtered = medfilt2(image_odd_1(:,:,FrID), [filter1_px(ii), filter1_px(ii)]);
                        image_even_1_filtered = medfilt2(image_even_1(:,:,FrID), [filter1_px(ii), filter1_px(ii)]);
                end

                image_odd = image_odd_1_filtered - gamma*image_odd_2_filtered;
                image_even = image_even_1_filtered - gamma*image_even_2_filtered; % STEDD image. 2D image. Refreshed in every loop.

                % ===== some operation to STEDD image =====
                if(neg) % neglect negative intensities to do FRC for the STEDD image.
                    image_odd = max(image_odd, 0);
                    image_even = max(image_even, 0);
                else
                    if(islift)
                        if(min(min(image_odd)) < 0)
                            image_odd = image_odd - min(min(image_odd));
                        end
                        if(min(min(image_even)) < 0)
                            image_even = image_even - min(min(image_even));
                        end
                    end
                end

                % ====== filter STEDD image ===============
                if(~strcmp(filter_type_3, 'No filter')) % if an option other than "No filter" is selected.
                    switch filter_type_3
                        case 'Wiener filter'
                            image_odd = wiener2(image_odd, [filter_px_3, filter_px_3]);
                            image_even = wiener2(image_even, [filter_px_3, filter_px_3]);
                        case 'Gaussian filter'
                            image_odd = imgaussfilt(image_odd, filter_px_3);
                            image_even = imgaussfilt(image_even, filter_px_3);
                        case 'Average filter'
                            image_odd = filter2(fspecial('average', filter_px_3), image_odd);
                            image_even = filter2(fspecial('average', filter_px_3), image_even);
                        case 'Median filter'
                            image_odd = medfilt2(image_odd, [filter_px_3, filter_px_3]);
                            image_even = medfilt2(image_even, [filter_px_3, filter_px_3]);
                    end
                end

                % ======= FRC calculation for STEDD ===================
                [res_value(ii), ~, res_high(ii), res_low(ii), frc_smooth(:, ii)] = imres_ims(image_odd, image_even, px_size, show_frc);

                % === update the smoothened FRC curve.
                figure(f1);
                plot(linspace(0, qmax*sqrt(2), length(frc_smooth(:, ii))), frc_smooth(:, ii),'color', C(ii,:),'LineWidth',1.5);
            
            end % End of the loop for filter size 1.
            
        else % if the FrID is out of range.
            disp('Error! Frame ID larger than  the number of valid stacks!');
            msgstr = sprintf('Error! Frame ID larger than  the number of valid stacks!'); 
            set(handles.StatusMsgStr,'String',msgstr);
            drawnow();
        end
    else % Otherwise, show all the images. If the checkbox is checked, then the frame number is meaningless.
            % The output of getImage is a 2D image. size(image_odd_2, 3) ==
            % 3.
    % filter the images of STED2
        switch filter_type_2
            case 'Wiener filter'
                image_odd_2_filtered = wiener2(image_odd_2, [filter_px_2, filter_px_2]);
                image_even_2_filtered = wiener2(image_even_2, [filter_px_2, filter_px_2]);
            case 'Gaussian filter'
                image_odd_2_filtered = imgaussfilt(image_odd_2, filter_px_2);
                image_even_2_filtered = imgaussfilt(image_even_2, filter_px_2);
            case 'Average filter'
                image_odd_2_filtered = filter2(fspecial('average', filter_px_2), image_odd_2);
                image_even_2_filtered = filter2(fspecial('average', filter_px_2), image_even_2);
            case 'Median filter'
                image_odd_2_filtered = medfilt2(image_odd_2, [filter_px_2, filter_px_2]);
                image_even_2_filtered = medfilt2(image_even_2, [filter_px_2, filter_px_2]);
            case 'No filter'
                image_odd_2_filtered = image_odd_2;
                image_even_2_filtered = image_even_2;
        end

        % ===== Start start the loop for STED1
        for ii = 1:length(filter1_px)
        %  =====     filter STED1 image
            switch filter_type_1
                case 'Wiener filter'
                    image_odd_1_filtered = wiener2(image_odd_1, [filter1_px(ii), filter1_px(ii)]);
                    image_even_1_filtered = wiener2(image_even_1, [filter1_px(ii), filter1_px(ii)]);
                case 'Gaussian filter'
                    image_odd_1_filtered = imgaussfilt(image_odd_1, filter1_px(ii));
                    image_even_1_filtered = imgaussfilt(image_even_1, filter1_px(ii));
                case 'Average filter'
                    image_odd_1_filtered = filter2(fspecial('average', filter1_px(ii)), image_odd_1);
                    image_even_1_filtered = filter2(fspecial('average', filter1_px(ii)), image_even_1);
                case 'Median filter'
                    image_odd_1_filtered = medfilt2(image_odd_1, [filter1_px(ii), filter1_px(ii)]);
                    image_even_1_filtered = medfilt2(image_even_1, [filter1_px(ii), filter1_px(ii)]);
            end

            image_odd = image_odd_1_filtered - gamma*image_odd_2_filtered;
            image_even = image_even_1_filtered - gamma*image_even_2_filtered; % STEDD image. 2D image. Refreshed in every loop.

            % ===== some operation to STEDD image =====
            if(neg) % neglect negative intensities to do FRC for the STEDD image.
                image_odd = max(image_odd, 0);
                image_even = max(image_even, 0);
            else
                if(islift)
                    if(min(min(image_odd)) < 0)
                        image_odd = image_odd - min(min(image_odd));
                    end
                    if(min(min(image_even)) < 0)
                        image_even = image_even - min(min(image_even));
                    end
                end
            end

            % ====== filter STEDD image ===============
            if(~strcmp(filter_type_3, 'No filter')) % if an option other than "No filter" is selected.
                switch filter_type_3
                    case 'Wiener filter'
                        image_odd = wiener2(image_odd, [filter_px_3, filter_px_3]);
                        image_even = wiener2(image_even, [filter_px_3, filter_px_3]);
                    case 'Gaussian filter'
                        image_odd = imgaussfilt(image_odd, filter_px_3);
                        image_even = imgaussfilt(image_even, filter_px_3);
                    case 'Average filter'
                        image_odd = filter2(fspecial('average', filter_px_3), image_odd);
                        image_even = filter2(fspecial('average', filter_px_3), image_even);
                    case 'Median filter'
                        image_odd = medfilt2(image_odd, [filter_px_3, filter_px_3]);
                        image_even = medfilt2(image_even, [filter_px_3, filter_px_3]);
                end
            end

            % ======= FRC calculation for STEDD ===================
            [res_value(ii), ~, res_high(ii), res_low(ii), frc_smooth(:, ii)] = imres_ims(image_odd, image_even, px_size, show_frc);

            % === update the smoothened FRC curve.
            figure(f1);
            plot(linspace(0, qmax*sqrt(2), length(frc_smooth(:, ii))), frc_smooth(:, ii),'color', C(ii,:),'LineWidth',1.5);
            
        end % End of the loop for filter size 1.
    end % end of "all frames"
    
    figure(f1);
    colormap(C);
    c = colorbar('Ticks', [0, 1],...
        'TickLabels', {num2str(filter1_px(1)), num2str(filter1_px(end))});
    c.Label.String = 'Size of Wiener filter (px)';
    set(findall(f1,'-property','FontSize'),'FontSize',14)

    % Now plot FRC resolution against Wiener filter size.
    f2 = figure('Name','FRC resolution, STEDD','NumberTitle','off');
    h = errorbar(filter1_px, res_value, res_value - res_low, res_high - res_value);
    h.Marker = 'o';
    h.MarkerSize = 8;
    h.Color = 'r';
    h.LineWidth = 1.5;
    xlabel(strcat(filter_type_1, ' size applied to STED1 image (px)'));
    ylabel('FRC resolution (nm)');
    title('FRC resolution, STEDD');
    set(findall(f2,'-property','FontSize'),'FontSize',14);

    t = toc;
    msgstr = sprintf('Calculated FRC in %5.1f s',t); 
    set(handles.StatusMsgStr,'String',msgstr);
    drawnow();

    % Now save the FRC curves
    if (save_frc_value_3)
        a = linspace(0,qmax*sqrt(2), length(frc_smooth)); % spatial frequency in the unit of 1/um.
        FRC_data = [a' frc_smooth]; % FRC data to export
        FRC_res = [filter1_px res_value res_high res_low]; % Resolution values to export.

        global bh_info;
        idx = get(handles.DataFilesList,'Value');

        t1 = get(handles.ImgTlow_1,'String');
        t2 = get(handles.ImgThigh_1,'String');
        t3 = get(handles.ImgTlow_2,'String');
        t4 = get(handles.ImgThigh_2,'String');
        % 
        [fp,fn,~] = fileparts(bh_info(idx).filename);

        img_fn = fullfile(fp,strcat(fn,'_STEDD_gamma_',num2str(gamma),'_',t1,'to',t2,'_',t3,'to',t4,...
            '_STED1_',filter_type_1,'_',filter_range{1},'to',filter_range{3},'_FRC','.txt'));

        [img_fn, img_fp] = uiputfile(img_fn,'Save data as...'); % directory and name of the file.

        if (~isequal(img_fn,0) && ~isequal(img_fp,0))
            msgstr = sprintf('Saving FRC curves ...'); 
            set(handles.StatusMsgStr,'String',msgstr);
            drawnow();
            save(fullfile(img_fp,img_fn), 'FRC_data','-ascii');
        end

        img_fn = fullfile(fp,strcat(fn,'_STEDD_gamma_',num2str(gamma),'_',t1,'to',t2,'_',t3,'to',t4,...
            '_STED1_',filter_type_1,'_', filter_range{1},'to',filter_range{3},'_FRC_res','.txt'));

        [img_fn, img_fp] = uiputfile(img_fn,'Save data as...'); % directory and name of the file.

        if (~isequal(img_fn,0) && ~isequal(img_fp,0))
            msgstr = sprintf('Saving FRC resolutions ...'); 
            set(handles.StatusMsgStr,'String',msgstr);
            drawnow();
            save(fullfile(img_fp,img_fn), 'FRC_res','-ascii');
        end

        t = toc;
        msgstr = sprintf('Calculated and saved FRC in %5.1f s',t); 
        set(handles.StatusMsgStr,'String',msgstr);
        drawnow();
    else
        t = toc;
        msgstr = sprintf('Calculated FRC in %5.1f s',t); 
        set(handles.StatusMsgStr,'String',msgstr);
        drawnow();
    end
end


% --- Executes on selection change in FilterType_1.
function FilterType_1_Callback(hObject, eventdata, handles)
% hObject    handle to FilterType_1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns FilterType_1 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from FilterType_1


% --- Executes during object creation, after setting all properties.
function FilterType_1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to FilterType_1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in FilterType_2.
function FilterType_2_Callback(hObject, eventdata, handles)
% hObject    handle to FilterType_2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns FilterType_2 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from FilterType_2
% test

% --- Executes during object creation, after setting all properties.
function FilterType_2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to FilterType_2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function Filter3_px_Callback(hObject, eventdata, handles)
% hObject    handle to Filter3_px (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of Filter3_px as text
%        str2double(get(hObject,'String')) returns contents of Filter3_px as a double


% --- Executes during object creation, after setting all properties.
function Filter3_px_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Filter3_px (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in FilterType_3.
function FilterType_3_Callback(hObject, eventdata, handles)
% hObject    handle to FilterType_3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns FilterType_3 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from FilterType_3


% --- Executes during object creation, after setting all properties.
function FilterType_3_CreateFcn(hObject, eventdata, handles)
% hObject    handle to FilterType_3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in Gamma_Filter.
function Gamma_Filter_Callback(hObject, eventdata, handles)
% hObject    handle to Gamma_Filter (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% This button uses the opt lett method to determine the gamma factor for different sizes of filter applied to sted2 image.
% The filter applied to stedd image is ignored.
% One can choose to save the data shown in external figures into ascii
% files if one checks the "save data" checkbox.

% === filter applied to STED2 image
filter_list_2 = get(handles.FilterType_2, 'String'); % read the list of filters
filter_value_2 = get(handles.FilterType_2, 'Value'); % which one is selected
filter_type_2 = char(filter_list_2(filter_value_2));

if(strcmp(filter_type_2, 'No filter'))
    % if no filter type is selected, there is no need to continue
    disp('Error! Please choose the type of filter for STED2!');
    msgstr = sprintf('Error! Please choose the type of filter for STED2!'); 
    set(handles.StatusMsgStr,'String',msgstr);
    drawnow();
    
else
    % Ask for the range for calculation
    % Here the calculation may take long, so the range of gamma can be
    % determined by us.
    prompt = {'Filter size from: (>=2)','Step:', 'To', 'Gamma from:', 'Step', 'To'};
    dlgtitle = 'Size of filter applied to STED2 (px) and range of gamma';
    dims = [1 35];
    definput = {'3','3','48','0.1','0.1','10'}; % default values
    range = inputdlg(prompt,dlgtitle,dims,definput); % cell array containing the inputs
    filter_from = str2num(range{1});
    filter_step = str2num(range{2});
    filter_to = str2num(range{3});
    % the size of filter applied to STED2 image
    filter2_px = (filter_from:filter_step:filter_to)'; 
    N_filter = length(filter2_px);
    C = linspecer(N_filter); % for colors of the lines.
    
    gamma_from = str2num(range{4});
    gamma_step = str2num(range{5});
    gamma_to = str2num(range{6});
    gamma_range = (gamma_from:gamma_step:gamma_to)'; % range of gamma
    N_gamma = length(gamma_range); % number of gammas

    set(handles.StatusMsgStr,'String','Calculating gamma factors as a function of filter sizes applied to STED2 image ...'); 
    drawnow();
    tic
    
    % ====== parameter setup =====================================
    idx = get(handles.DataFilesList,'Value');
    FrID = str2double(get(handles.FrIDEdit,'String')); % The ID of frame that is going to show.
    AllFr_value = get(handles.AllFrRadio,'Value'); % show all the frames or only show the middle frame.
    % whether converting negative counts is taken care of in getGamma function.
    global bh_info; % For STED1 image, use the global image.
    save_data_value_3 = get(handles.Save_Data_3, 'Value'); % Save the gamma values and the C_gamma curves?
    gamma = zeros(N_filter, 1);
    C_gamma = zeros(N_gamma, N_filter);
    % ==== end of parameter setup. ======
    
    % === read img_2
    img_2 = getImage(handles, 'img2', 'all'); % STED2 image is not a global variable.
    % now filter img_2.
    
    % ==== % prepare figure for showing the judgement curves on a single
    % figure
    f1 = figure('Name','C_gamma curves','NumberTitle','off');
    xlabel('\gamma');
    ylabel('C(\gamma)');
    title('C(\gamma) curves');
    hold on
    
    % ------ calculation ----------
    if(~AllFr_value) % if the checkbox is not checked, then work on the specified frame.
        if(FrID <= size(image_odd_1, 3)) % if the frame ID is not out of range.
            % ===== Start start the loop for STED2, only one frame.
            for ii = 1:N_filter
                switch filter_type_2
                    case 'Wiener filter'
                        img2_tmp = wiener2(img_2(:,:,FrID), [filter2_px(ii), filter2_px(ii)]);
                    case 'Gaussian filter'
                        img2_tmp = imgaussfilt(img_2(:,:,FrID), filter2_px(ii));
                    case 'Average filter'
                        img2_tmp = filter2(fspecial('average', filter2_px(ii)), img_2(:,:,FrID));
                    case 'Median filter'
                        img2_tmp = medfilt2(img_2(:,:,FrID), [filter2_px(ii), filter2_px(ii)]);
                end
                
                [gamma(ii), C_gamma(:,ii)] = getGamma(handles, bh_info(idx).img_1(:,:,FrID), img2_tmp, gamma_range);
                figure(f1)
                plot(gamma_range, C_gamma(:,ii), 's-', 'color', C(ii,:),'LineWidth',1);
            end % end of the loop
            
        else % if the FrID is out of range.
            disp('Error! Frame ID larger than  the number of valid stacks!');
            msgstr = sprintf('Error! Frame ID larger than  the number of valid stacks!'); 
            set(handles.StatusMsgStr,'String',msgstr);
            drawnow();
        end
    else % Otherwise, show all the images. If the checkbox is checked, then the frame number is meaningless.
        % ===== Start start the loop for STED2, only one frame.
        for ii = 1:N_filter 
            switch filter_type_2
                case 'Wiener filter'
                    img2_tmp = wiener2(img_2, [filter2_px(ii), filter2_px(ii)]);
                case 'Gaussian filter'
                    img2_tmp = imgaussfilt(img_2, filter2_px(ii));
                case 'Average filter'
                    img2_tmp = filter2(fspecial('average', filter2_px(ii)), img_2);
                case 'Median filter'
                    img2_tmp = medfilt2(img_2, [filter2_px(ii), filter2_px(ii)]);
            end
            
            [gamma(ii), C_gamma(:,ii)] = getGamma(handles, bh_info(idx).img_1, img2_tmp, gamma_range);
            figure(f1)
            plot(gamma_range, C_gamma(:,ii), 's-', 'color', C(ii,:),'LineWidth',1);
        end % end of the loop
    end
    
    figure(f1);
    colormap(C);
    c = colorbar('Ticks', [0, 1],...
        'TickLabels', {num2str(filter2_px(1)), num2str(filter2_px(end))});
    c.Label.String = strcat({'Size of '}, filter_type_2, {' (px)'});
    set(findall(f1,'-property','FontSize'),'FontSize',14)
    
    % === now plot gamma as a function of filter size
    f2 = figure('Name', 'Gamma factor', 'NumberTitle', 'off');
    h = plot(filter2_px, gamma);
    h.Marker = 'o';
    h.MarkerSize = 8;
    h.Color = 'r';
    h.LineWidth = 1.5;
    xlabel(strcat(filter_type_2, ' size applied to STED2 image (px)'));
    ylabel('Gamma factor');
    title('Gamma factor');
    set(findall(f2,'-property','FontSize'),'FontSize',14);
    
    % now save the gamma data
    if(save_data_value_3)
        gamma_data = [filter2_px gamma]; % gamma vs filter size;
        C_gamma_data = [gamma_range C_gamma]; % C_gamma curves vs gamma range
        
        idx = get(handles.DataFilesList,'Value');
        t1 = get(handles.ImgTlow_1,'String');
        t2 = get(handles.ImgThigh_1,'String');
        t3 = get(handles.ImgTlow_2,'String');
        t4 = get(handles.ImgThigh_2,'String');
        % 
        [fp,fn,~] = fileparts(bh_info(idx).filename);
        img_fn = fullfile(fp,strcat(fn,'_',t1,'to',t2,'_',t3,'to',t4,...
            '_STED2_',filter_type_2,'_gamma_vs_size','.txt'));
        [img_fn, img_fp] = uiputfile(img_fn,'Save data as...'); % directory and name of the file.
        
        if (~isequal(img_fn,0) && ~isequal(img_fp,0))
            msgstr = sprintf('Saving gamma factors ...'); 
            set(handles.StatusMsgStr,'String',msgstr);
            drawnow();
            save(fullfile(img_fp,img_fn), 'gamma_data','-ascii');
        end
        
        % save C_gamma curves
        img_fn = fullfile(fp,strcat(fn,'_',t1,'to',t2,'_',t3,'to',t4,...
            '_STED2_',filter_type_2,'_C_gamma_vs_size','.txt'));
        [img_fn, img_fp] = uiputfile(img_fn,'Save data as...'); % directory and name of the file.
        
        if (~isequal(img_fn,0) && ~isequal(img_fp,0))
            msgstr = sprintf('Saving C_gamma curves ...'); 
            set(handles.StatusMsgStr,'String',msgstr);
            drawnow();
            save(fullfile(img_fp,img_fn), 'C_gamma_data','-ascii');
        end

        t = toc;
        msgstr = sprintf('Calculated and saved gamma versus filter size in %5.1f s',t); 
        set(handles.StatusMsgStr,'String',msgstr);
        drawnow();
    else
        t = toc;
        msgstr = sprintf('Calculated gamma versus filter size in %5.1f s',t); 
        set(handles.StatusMsgStr,'String',msgstr);
        drawnow();
    end

end

% --- Executes on button press in Save_Data_1.
function Save_Data_1_Callback(hObject, eventdata, handles)
% hObject    handle to Save_Data_1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of Save_Data_1


% --- Executes on button press in Save_Data_2.
function Save_Data_2_Callback(hObject, eventdata, handles)
% hObject    handle to Save_Data_2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of Save_Data_2


% --- Executes on button press in Save_Data_3.
function Save_Data_3_Callback(hObject, eventdata, handles)
% hObject    handle to Save_Data_3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of Save_Data_3

% --- Executes on button press in DeCorr_1.
function DeCorr_1_Callback(hObject, eventdata, handles)
% hObject    handle to DeCorr_1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global bh_info;
idx = get(handles.DataFilesList,'Value');

if (~isempty(idx)) % if a file is selected
    
    FrID = str2double(get(handles.FrIDEdit,'String')); % The ID of frame that is going to show.
    AllFr_value = get(handles.AllFrRadio,'Value'); % show all the frames or only show the middle frame.
    px_size = str2double(get(handles.PxSizeEdit,'String'));

    if handles.New_Decorr_1.Value == 0 % Use the original code

        % Ask for the range for calculation
        prompt = {'Sampling','Number'};
        dlgtitle = 'Decorr';
        dims = [1 35];
        definput = {'50','10'}; % default values
        parameter_range = inputdlg(prompt, dlgtitle, dims, definput); % cell array containing the inputs
        Nr= str2num(parameter_range{1});
        Ng = str2num(parameter_range{2});
        
        set(handles.StatusMsgStr,'String','Calculating decorrelation of the STED1 image ...'); 
        drawnow();
        tic;
        
        if(AllFr_value) % if show all frames
            main_imageDecorr(bh_info(idx).img_1, px_size, Nr, Ng);
        else
            if(FrID <= size(bh_info(idx).img_1, 3))
                main_imageDecorr(bh_info(idx).img_1(:,:,FrID), px_size, Nr, Ng);
            else
                disp('Error! Frame ID larger than  the number of valid stacks!');
                msgstr = sprintf('Error! Frame ID larger than  the number of valid stacks!'); 
                set(handles.StatusMsgStr,'String',msgstr);
                drawnow();
            end
        end

    elseif handles.New_Decorr_1.Value == 1 
        % Ask for the range for calculation
        prompt = {'Func(1 - Real, 0 - Fourier)','Nr','Epsilon'};
        dlgtitle = 'Decorr';
        dims = [1 35];
        definput = {'0','50','0.1'}; % default values
        parameter_range = inputdlg(prompt, dlgtitle, dims, definput); % cell array containing the inputs

        func = str2num(parameter_range{1});
        n = str2num(parameter_range{2});
        e = str2num(parameter_range{3});

        set(handles.StatusMsgStr,'String','Calculating decorrelation of the STED1 image ...'); 
        drawnow();
        tic;
        
        if(AllFr_value) % if show all frames
            main_decorr_3D(bh_info(idx).img_1,px_size,func,n,e);
        else
            if(FrID <= size(bh_info(idx).img_1, 3))
                main_decorr_3D(bh_info(idx).img_1(:,:,FrID),px_size,func,n,e);
            else
                disp('Error! Frame ID larger than  the number of valid stacks!');
                msgstr = sprintf('Error! Frame ID larger than  the number of valid stacks!'); 
                set(handles.StatusMsgStr,'String',msgstr);
                drawnow();
            end
        end
    end
    t = toc;
    msgstr = sprintf('Decorrelation finished in %5.1f s',t);
    set(handles.StatusMsgStr,'String',msgstr); 
end

% --- Executes on button press in DeCorr_1.
function DeCorr_2_Callback(hObject, eventdata, handles)
% hObject    handle to DeCorr_1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global bh_info;
idx = get(handles.DataFilesList,'Value');

if (~isempty(idx)) % if a file is selected
    
    FrID = str2double(get(handles.FrIDEdit,'String')); % The ID of frame that is going to show.
    AllFr_value = get(handles.AllFrRadio,'Value'); % show all the frames or only show the middle frame.
    px_size = str2double(get(handles.PxSizeEdit,'String'));

    if handles.New_Decorr_2.Value == 0 % Use the original code

        % Ask for the range for calculation
        prompt = {'Sampling','Number'};
        dlgtitle = 'Decorr';
        dims = [1 35];
        definput = {'50','10'}; % default values
        parameter_range = inputdlg(prompt, dlgtitle, dims, definput); % cell array containing the inputs
        Nr= str2num(parameter_range{1});
        Ng = str2num(parameter_range{2});
        
        set(handles.StatusMsgStr,'String','Calculating decorrelation of the STED2 image ...'); 
        drawnow();
        tic;
        
        if(AllFr_value) % if show all frames
            main_imageDecorr(bh_info(idx).img_2, px_size, Nr, Ng);
        else
            if(FrID <= size(bh_info(idx).img_2, 3))
                main_imageDecorr(bh_info(idx).img_2(:,:,FrID), px_size, Nr, Ng);
            else
                disp('Error! Frame ID larger than  the number of valid stacks!');
                msgstr = sprintf('Error! Frame ID larger than  the number of valid stacks!'); 
                set(handles.StatusMsgStr,'String',msgstr);
                drawnow();
            end
        end

    elseif handles.New_Decorr_2.Value == 1 
        % Ask for the range for calculation
        prompt = {'Func(1 - Real, 0 - Fourier)','Nr','Epsilon'};
        dlgtitle = 'Decorr';
        dims = [1 35];
        definput = {'0','50','0.1'}; % default values
        parameter_range = inputdlg(prompt, dlgtitle, dims, definput); % cell array containing the inputs

        func = str2num(parameter_range{1});
        n = str2num(parameter_range{2});
        e = str2num(parameter_range{3});

        set(handles.StatusMsgStr,'String','Calculating decorrelation of the STED2 image ...'); 
        drawnow();
        tic;
        
        if(AllFr_value) % if show all frames
            main_decorr_3D(bh_info(idx).img_2, px_size, func, n, e);
        else
            if(FrID <= size(bh_info(idx).img_2, 3))
                main_decorr_3D(bh_info(idx).img_2(:,:,FrID), px_size, func, n, e);
            else
                disp('Error! Frame ID larger than  the number of valid stacks!');
                msgstr = sprintf('Error! Frame ID larger than  the number of valid stacks!'); 
                set(handles.StatusMsgStr,'String',msgstr);
                drawnow();
            end
        end
    end
    t = toc;
    msgstr = sprintf('Decorrelation finished in %5.1f s',t);
    set(handles.StatusMsgStr,'String',msgstr); 
end


% --- Executes on button press in DeCorr_2.
function DeCorr_3_Callback(hObject, eventdata, handles)
% hObject    handle to DeCorr_2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global bh_info;
idx = get(handles.DataFilesList,'Value');

if (~isempty(idx)) % if a file is selected
    
    FrID = str2double(get(handles.FrIDEdit,'String')); % The ID of frame that is going to show.
    AllFr_value = get(handles.AllFrRadio,'Value'); % show all the frames or only show the middle frame.
    px_size = str2double(get(handles.PxSizeEdit,'String'));

    if handles.New_Decorr_3.Value == 0 % Use the original code

        % Ask for the range for calculation
        prompt = {'Sampling','Number'};
        dlgtitle = 'Decorr';
        dims = [1 35];
        definput = {'50','10'}; % default values
        parameter_range = inputdlg(prompt, dlgtitle, dims, definput); % cell array containing the inputs
        Nr= str2num(parameter_range{1});
        Ng = str2num(parameter_range{2});
        
        set(handles.StatusMsgStr,'String','Calculating decorrelation of the STEDD image ...'); 
        drawnow();
        tic;
        
        if(AllFr_value) % if show all frames
            main_imageDecorr(bh_info(idx).img_3, px_size, Nr, Ng);
        else
            if(FrID <= size(bh_info(idx).img_3, 3))
                main_imageDecorr(bh_info(idx).img_3(:,:,FrID), px_size, Nr, Ng);
            else
                disp('Error! Frame ID larger than  the number of valid stacks!');
                msgstr = sprintf('Error! Frame ID larger than  the number of valid stacks!'); 
                set(handles.StatusMsgStr,'String',msgstr);
                drawnow();
            end
        end

    elseif handles.New_Decorr_3.Value == 1 
        % Ask for the range for calculation
        prompt = {'Func(1 - Real, 0 - Fourier)','Nr','Epsilon'};
        dlgtitle = 'Decorr';
        dims = [1 35];
        definput = {'0','50','0.1'}; % default values
        parameter_range = inputdlg(prompt, dlgtitle, dims, definput); % cell array containing the inputs

        func = str2num(parameter_range{1});
        n = str2num(parameter_range{2});
        e = str2num(parameter_range{3});

        set(handles.StatusMsgStr,'String','Calculating decorrelation of the STEDD image ...'); 
        drawnow();
        tic;
        
        if(AllFr_value) % if show all frames
            main_decorr_3D(bh_info(idx).img_3, px_size, func, n, e);
        else
            if(FrID <= size(bh_info(idx).img_3, 3))
                main_decorr_3D(bh_info(idx).img_3(:,:,FrID), px_size, func, n, e);
            else
                disp('Error! Frame ID larger than  the number of valid stacks!');
                msgstr = sprintf('Error! Frame ID larger than  the number of valid stacks!'); 
                set(handles.StatusMsgStr,'String',msgstr);
                drawnow();
            end
        end
    end
    t = toc;
    msgstr = sprintf('Decorrelation finished in %5.1f s',t);
    set(handles.StatusMsgStr,'String',msgstr); 
end

% --- Executes on button press in FRC_img3.
function FRC_img3_Callback(hObject, eventdata, handles)
% hObject    handle to FRC_img3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% This button calculates the FRC of the STEDD image for only the current
% set of parameters.

set(handles.StatusMsgStr,'String','Calculating FRC values ... '); 
drawnow();
tic

save_frc_value_3 = get(handles.Save_Data_3, 'Value'); % Save the FRC results?
filter_list_1 = get(handles.FilterType_1, 'String'); % read the list of filters
filter_value_1 = get(handles.FilterType_1, 'Value'); % which one is selected
filter_type_1 = char(filter_list_1(filter_value_1));
% wiener1_value = get(handles.Wiener1,'Value'); % If or not doing Wiener filtering for STED1.
filter_px_1 = str2double(get(handles.Filter1_px, 'String'));
% filter_px = uint8(filter_px); % size of filter in the unit of px.

filter_list_2 = get(handles.FilterType_2, 'String'); % read the list of filters
filter_value_2 = get(handles.FilterType_2, 'Value'); % which one is selected
filter_type_2 = char(filter_list_2(filter_value_2));
% wiener1_value = get(handles.Wiener1,'Value'); % If or not doing Wiener filtering for STED1.
filter_px_2 = str2double(get(handles.Filter2_px, 'String'));
% filter_px = uint8(filter_px); % size of filter in the unit of px.

filter_list_3 = get(handles.FilterType_3, 'String'); % read the list of filters
filter_value_3 = get(handles.FilterType_3, 'Value'); % which one is selected
filter_type_3 = char(filter_list_3(filter_value_3));
% wiener1_value = get(handles.Wiener1,'Value'); % If or not doing Wiener filtering for STED1.
filter_px_3 = str2double(get(handles.Filter3_px, 'String'));
% filter_px = uint8(filter_px); % size of filter in the unit of px.

image_odd_1 = getImage(handles, 'img1', 'odd');
image_even_1 = getImage(handles, 'img1', 'even');

image_odd_2 = getImage(handles, 'img2', 'odd');
image_even_2 = getImage(handles, 'img2', 'even');

% filter the STED1 images
if(~strcmp(filter_type_1, 'No filter'))
    nlay = size(image_odd_1, 3); % number of layers
    for ii = 1:nlay
        switch filter_type_1
            case 'Wiener filter'
                image_odd_1(:,:,ii) = wiener2(image_odd_1(:,:,ii), [filter_px_1, filter_px_1]);
                image_even_1(:,:,ii) = wiener2(image_even_1(:,:,ii), [filter_px_1, filter_px_1]);
            case 'Gaussian filter'
                image_odd_1(:,:,ii) = imgaussfilt(image_odd_1(:,:,ii), filter_px_1);
                image_even_1(:,:,ii) = imgaussfilt(image_even_1(:,:,ii), filter_px_1);
            case 'Average filter'
                image_odd_1(:,:,ii) = filter2(fspecial('average', filter_px_1), image_odd_1(:,:,ii));
                image_even_1(:,:,ii) = filter2(fspecial('average', filter_px_1), image_even_1(:,:,ii));
            case 'Median filter'
                image_odd_1(:,:,ii) = medfilt2(image_odd_1(:,:,ii), [filter_px_1, filter_px_1]);
                image_even_1(:,:,ii) = medfilt2(image_even_1(:,:,ii), [filter_px_1, filter_px_1]);
        end
    end
end

% filter the STED2 images
if(~strcmp(filter_type_2, 'No filter'))
    nlay = size(image_odd_2, 3); % number of layers
    for ii = 1:nlay
        switch filter_type_2
            case 'Wiener filter'
                image_odd_2(:,:,ii) = wiener2(image_odd_2(:,:,ii), [filter_px_2, filter_px_2]);
                image_even_2(:,:,ii) = wiener2(image_even_2(:,:,ii), [filter_px_2, filter_px_2]);
            case 'Gaussian filter'
                image_odd_2(:,:,ii) = imgaussfilt(image_odd_2(:,:,ii), filter_px_2);
                image_even_2(:,:,ii) = imgaussfilt(image_even_2(:,:,ii), filter_px_2);
            case 'Average filter'
                image_odd_2(:,:,ii) = filter2(fspecial('average', filter_px_2), image_odd_2(:,:,ii));
                image_even_2(:,:,ii) = filter2(fspecial('average', filter_px_2), image_even_2(:,:,ii));
            case 'Median filter'
                image_odd_2(:,:,ii) = medfilt2(image_odd_2(:,:,ii), [filter_px_2, filter_px_2]);
                image_even_2(:,:,ii) = medfilt2(image_even_2(:,:,ii), [filter_px_2, filter_px_2]);
        end
    end
end

gamma = str2double(get(handles.gamma,'String'));
image_odd_3 = image_odd_1 - gamma*image_odd_2;
image_even_3 = image_even_1 - gamma*image_even_2;

% filter the STEDD images
if(~strcmp(filter_type_3, 'No filter'))
    nlay = size(image_odd_3, 3); % number of layers
    for ii = 1:nlay
        switch filter_type_3
            case 'Wiener filter'
                image_odd_3(:,:,ii) = wiener2(image_odd_3(:,:,ii), [filter_px_3, filter_px_3]);
                image_even_3(:,:,ii) = wiener2(image_even_3(:,:,ii), [filter_px_3, filter_px_3]);
            case 'Gaussian filter'
                image_odd_3(:,:,ii) = imgaussfilt(image_odd_3(:,:,ii), filter_px_3);
                image_even_3(:,:,ii) = imgaussfilt(image_even_3(:,:,ii), filter_px_3);
            case 'Average filter'
                image_odd_3(:,:,ii) = filter2(fspecial('average', filter_px_3), image_odd_3(:,:,ii));
                image_even_3(:,:,ii) = filter2(fspecial('average', filter_px_3), image_even_3(:,:,ii));
            case 'Median filter'
                image_odd_3(:,:,ii) = medfilt2(image_odd_3(:,:,ii), [filter_px_3, filter_px_3]);
                image_even_3(:,:,ii) = medfilt2(image_even_3(:,:,ii), [filter_px_3, filter_px_3]);
        end
    end
end

% ===== some operation to STEDD image =====
neg = get(handles.NegCheck, 'Value'); % whether or not to cut negative intensity.
islift = get(handles.LiftCheck, 'Value'); % A constant offset is added to STEDD image to avoid negative counts.
if(neg) % neglect negative intensities to do FRC for the STEDD image.
    image_odd_3 = max(image_odd_3, 0);
    image_even_3 = max(image_even_3, 0);
else
    if(islift)
        if(min(min(image_odd_3)) < 0)
            image_odd_3 = image_odd_3 - min(min(image_odd_3));
        end
        if(min(min(image_even_3)) < 0)
            image_even_3 = image_even_3 - min(min(image_even_3));
        end
    end
end

px_size = str2double(get(handles.PxSizeEdit,'String')); % Pixel size in nm.
show_frc = 1; % whether or not show FRC curve

AllFr_value = get(handles.AllFrRadio,'Value'); % show all the frames or only show one of the frames.
% FRC only works for 2D images.

if(~AllFr_value)
    FrID = str2double(get(handles.FrIDEdit,'String')); % The ID of frame that is going to show.
    if(FrID <= size(image_odd_1,3))
        [res_value, frc_curve, res_high, res_low, frc_smooth] = imres_ims(image_odd_3(:,:,FrID),image_even_3(:,:,FrID), px_size, show_frc);
    else
        disp('Error! Frame ID larger than  the number of valid stacks!');
    end

else % Otherwise, show all the images.
    [res_value, frc_curve, res_high, res_low, frc_smooth] = imres_ims(image_odd_3, image_even_3, px_size, show_frc);
end

% % Plot FRC curve
% Now this is done in the core function
% qmax = 0.5/(px_size);
% figure
% hold on
% plot([0 qmax],[0 0],'k') % zero line
% plot(linspace(0,qmax*sqrt(2), length(frc_curve)), frc_curve,'-')
% plot([0 qmax],[1/7 1/7],'m')
% plot(1/(res_value),1/7,'rx'); % res_value has already taken pixel size into account
% plot(1/(res_value)*[1 1],[-0.2 1/7],'r')
% hold off
% xlim([0,qmax]);
% ylim([-0.2 1.2])
% xlabel('Spatial frequency (nm^{-1})');
% ylabel('FRC')
% txt = sprintf('FRC resolution: %2.1f +/- %2.2f nm.\n', res_value, (res_low - res_high)/2);
% text(qmax/3, 0.5, txt);

% Now save the FRC curve
if (save_frc_value_3)
    qmax = 0.5/(px_size);
    a = linspace(0,qmax*sqrt(2), length(frc_curve));
    FRC_data = [a' frc_curve' frc_smooth]; % FRC data to export
    global bh_info;
    idx = get(handles.DataFilesList,'Value');

    t1 = get(handles.ImgTlow_1,'String');
    t2 = get(handles.ImgThigh_1,'String');
    t3 = get(handles.ImgTlow_2,'String');
    t4 = get(handles.ImgThigh_2,'String');
    % 
    [fp,fn,~] = fileparts(bh_info(idx).filename);

    img_fn = fullfile(fp,strcat(fn,'_STEDD_gamma_', num2str(gamma), '_', t1,'to', t2, '_', t3, 'to', t4,...
        '_STED1_', filter_type_1, '_', num2str(filter_px_1),...
        '_STED2_', filter_type_2, '_', num2str(filter_px_2),...
        '_STEDD_', filter_type_3, '_', num2str(filter_px_3),...
        '_FRC','.txt'));

    [img_fn, img_fp] = uiputfile(img_fn,'Save data as...'); % directory and name of the file.

    if (~isequal(img_fn,0) && ~isequal(img_fp,0))
        msgstr = sprintf('Saving data ...'); 
        set(handles.StatusMsgStr,'String',msgstr);
        drawnow();
        save(fullfile(img_fp,img_fn), 'FRC_data','-ascii'); %  bh_info(fidx).img_1 cannot be directly saved.
    end

    t = toc;
    msgstr = sprintf('Fourier Ring Correlation and saved in %5.1f s',t); 
    set(handles.StatusMsgStr,'String',msgstr);
    drawnow();
else
    t = toc;
    msgstr = sprintf('Fourier Ring Correlation in %5.1f s',t); 
    set(handles.StatusMsgStr,'String',msgstr);
    drawnow();
end


% --- Executes on button press in Fitting.
function Fitting_Callback(hObject, eventdata, handles)
% hObject    handle to Fitting (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global bh_data;
global bh_info;

% Ask for the range for calculation
prompt = {'Background number zeros','Epsilon','Delay time'};
dlgtitle = 'Fitting';
dims = [1 35];
definput = {'30','10','3'}; % default values
filter_range = inputdlg(prompt, dlgtitle, dims, definput); % cell array containing the inputs

l=str2num(filter_range{1});
epsi=str2num(filter_range{2});
tau=str2num(filter_range{3});

filelist = get(handles.DataFilesList,'String');
fileidx = get(handles.DataFilesList,'Value');

%Find position of maximum in histogramm
[pick_max(1),pick_max(2)]=max(bh_info(fileidx).ltData(:,2));

if bh_info(fileidx).ltData(pick_max(2),1)>20
    [pick_max(1),pick_max(2)]=max(bh_info(fileidx).ltData(1:pick_max(2)-100,2));
end 
    
%Find position of background
    massiv_min=bh_info(fileidx).ltData(1:pick_max(2),:);
    
    deriv_min=zeros(size(massiv_min));
    
%Derrivative
    for i=2:pick_max(2)
    
    deriv_min(i,2)=(massiv_min(i,2)-massiv_min(i-1,2))/(massiv_min(i,1)-massiv_min(i-1,1));
    deriv_min(i,1)=massiv_min(i,1);
    end      

%Derrivative smoothing: averaging on 2*n steps if they are not 0

n=40;
    deriv_min_sth=zeros(size(deriv_min));
    deriv_min_sth(1:pick_max(2)-n,2)=deriv_min(1:pick_max(2)-n,2);
    deriv_min_sth(1:pick_max(2),1)=deriv_min(1:pick_max(2),1);
    deriv_min(pick_max(2)-n:pick_max(2),2)=0;
    deriv_min(1:n,2)=0;
    for i=n:pick_max(2)-n
        
        if deriv_min_sth(i,2)~=0
        m=1;
        
        else
            m=0;
        end 
        
        for k=1:n-1
           
        if deriv_min(i-k,2)~=0 
            
            deriv_min_sth(i,2)=deriv_min_sth(i,2)+deriv_min(i-k,2);
            m=m+1;
        else 
            deriv_min_sth(i,2)=deriv_min_sth(i,2);
            m=m;
        end 
        
        if deriv_min(i+k,2)~=0    
    
            deriv_min_sth(i,2)=deriv_min_sth(i,2)+deriv_min(i+k,2);
            m=m+1;
        else 
            deriv_min_sth(i,2)=deriv_min_sth(i,2); 
            m=m;
        end   

        end 
        
        if m~=0
        
        deriv_min_sth(i,2)=deriv_min_sth(i,2)/m;
        
        else 
        deriv_min_sth(i,2)=deriv_min_sth(i,2);
        
        end
    end
    
%Find background 
background=zeros(size(massiv_min));
photon_lim_min=50;

for i=2:pick_max(2)
    if massiv_min(i,2)>photon_lim_min && abs((deriv_min_sth(i,2)-deriv_min_sth(i-1,2)))<epsi && deriv_min_sth(i,2)~=0 && deriv_min_sth(i-1,2)~=0
       background(i,2)=massiv_min(i,2);
       background(i,1)=massiv_min(i,1);
     
    end
end

background_t=background(:,1);
background_t(background_t==0)=[];
background_ph=background(:,2);
background_ph(background_ph==0)=[];

s=size(background_t);
s(1,2)=s(1,2)+1;
background=zeros(s);

background(:,1)=background_t;
background(:,2)=background_ph;
bk=min(background(:,2));

if size(bk)==[0,1]
    bk=photon_lim_min;
end

%Photon array to fit
image=bh_info(fileidx).ltData;
image(1:pick_max(2),2)=0;

%Clearing photon array from artefacts
for i=pick_max(2):size(image,1)-l
    
        m=0;
        
    for k=1:l
        
        if image(i+k,2)==0
            m=m+1;
        end
 
    end 
    
   if m>l/2
        image(i,2)=0;
    end
    
end


%Clearing photon array from background
for i=pick_max(2):size(image,1)   
    if image(i,2)<bk
    image(i,2)=0;
    end
end


%Final array to be processed
image=image(image(:,2)~=0,:);
pick_max(1,2)=image(1,1);

%for i=1:200
   
%    image(i,2)=0;
    
%end

%for i=size(image,1)-100:size(image,1)
    
 %   image(i,2)=0;
    
%end

image=image(image(:,2)~=0,:);

%Double gaussian fitting

a0=bk;

a1=pick_max(1,1);
a2=0.3*pick_max(1,1);

b1=pick_max(1,2);
b2=pick_max(1,2)+2;

k1=0;
k2=0;

%f=fit(image(:,1),image(:,2),func,'StartPoint',[a1 a2 a3 b1 b2 c1 c2],'Lower',[0 0 0 image(1,1) image(1,1)+2 0.1 0.1],'Upper',[Inf Inf Inf image(1,1) image(size(image,1),1) Inf Inf]);

func='a0+a1*exp(-k1*(x-b1))-a2*exp(-k2*(x-b2))';

%f=fit(image(:,1),image(:,2),func,'StartPoint',[a0 a1 a2 b k1 k2],'Lower',[0 -Inf -Inf image(1,1) 0 0],'Upper',[Inf Inf Inf image(size(image,1),1)+4 Inf Inf],'Robust','LAR','Algorithm','Trust-Region','DiffMaxChange',0.00001,'MaxIter',5000);

f=fit(image(:,1),image(:,2),func,'StartPoint',[a0 a1 a2 b1 b2 k1 k2],'Lower',[0 0 0 pick_max(1,2)+tau-0.5 pick_max(1,2) 0 0],'Upper',[Inf Inf Inf pick_max(1,2)+tau+0.5 25 10 10],'Algorithm','Trust-Region','MaxIter',5000);

if handles.Plot_Button.Value==1
figure('Name','Fitting and Photon array','NumberTitle','off');
p1=plot(f);
p1.LineWidth=3;

hold on
p2=plot(image(:,1),image(:,2));
legend('Double exponential fitting','Photon array');
xlabel('Time(ns)')
ylabel('Number of photons per time unit, logscale')
bk_txt=('Defined background '+ string(bk));
[txt_max(1),txt_max(2)]=max(image(:,1));
text(image(round(0.5*txt_max(2)),1),pick_max(1)/2,bk_txt);
set(gca, 'YScale', 'log');


hold off
end 

%Find time 
coeff=coeffvalues(f);
t1=pick_max(1,2);
t2=coeff(4);
t3=t2+0.6;
t4=image(size(image,1),1);

handles.ImgTlow_1.String=round(t1,2);
handles.ImgThigh_1.String=round(t2,2);
handles.ImgTlow_2.String=round(t3,2);
handles.ImgThigh_2.String=round(t4,2);


% --- Executes on button press in Decorr_gamma.
function Decorr_gamma_Callback(hObject, eventdata, handles)
% hObject    handle to Decorr_gamma (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% decorrelation resolutions of the STEDD image as a function of gamma.

global bh_info;
idx = get(handles.DataFilesList,'Value');

if (~isempty(idx))
    FrID = str2double(get(handles.FrIDEdit,'String')); % The ID of frame that is going to show.
    AllFr_value = get(handles.AllFrRadio,'Value'); % show all the frames or only show the middle frame.

    % Ask for the range for calculation
    prompt = {'Sampling','Number','Range of gamma: from','Range of gamma: step','Range of gamma: to','Show decorr curves (y/n)?'};
    dlgtitle = 'Decorrelation analysis';
    dims = [1 35];
    definput = {'50','10','1','0.1','4','y'}; % default values
    input_info = inputdlg(prompt, dlgtitle, dims, definput); % cell array containing the inputs

    Nr = str2num(input_info{1});
    Ng = str2num(input_info{2});
    range_from = str2num(input_info{3});
    range_step = str2num(input_info{4});
    range_to = str2num(input_info{5});
    show_plots = input_info{6}; % Whether show the curves. Now  the curves are always shown. Later we may choose to hide them.
    range_gamma = range_from:range_step:range_to;
    neg = get(handles.NegCheck, 'Value'); % whether or not to cut negative intensity.
    resolution = zeros(1, length(range_gamma));
    px_size = str2double(get(handles.PxSizeEdit,'String'));
    
    set(handles.StatusMsgStr,'String','Calculating decorrelation as a function of gamma ...'); 
    drawnow();
    
    tic
    
    if (~isempty(bh_info(idx).lnIdx)) %assign data events to lines
        if(AllFr_value)
            for num = 1:length(range_gamma)
                img_STEDD = bh_info(idx).img_1 - bh_info(idx).img_2*range_gamma(num);
                if(neg)
                    img_STEDD = max(img_STEDD, 0);
                end
                [~,~,resolution(num)] = main_imageDecorr(img_STEDD, px_size, Nr, Ng);
            end
        else
            if(FrID <= size(bh_info(idx).img_1, 3))
                for num = 1:length(range_gamma)
                    img_STEDD = bh_info(idx).img_1(:,:,FrID) - bh_info(idx).img_2(:,:,FrID)*range_gamma(num);
                    if(neg)
                        img_STEDD = max(img_STEDD, 0);
                    end
                    [~,~,resolution(num)] = main_imageDecorr(img_STEDD, px_size, Nr, Ng);
                end
            else
                disp('Error! Frame ID larger than  the number of valid stacks!');
                msgstr = sprintf('Error! Frame ID larger than  the number of valid stacks!'); 
                set(handles.StatusMsgStr,'String',msgstr);
                drawnow();
            end
        end
        
        figure;
        plot(range_gamma, resolution, 's');
        xlabel('\gamma');
        ylabel('Decorr. res. (nm)');
    end
    
    t = toc;
    msgstr = sprintf('Decorrelation finished in %5.1f s',t);
    set(handles.StatusMsgStr,'String',msgstr); 
    
end


% --- Executes on button press in Dcorr_filter2.
function Dcorr_filter2_Callback(hObject, eventdata, handles)
% hObject    handle to Dcorr_filter2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% This button calculates decorrelation resolution of the STEDD image as a
% function of the size of filter applied to STED2 image.
% STED1 and STEDD can be filtered. You should select and apply.

global bh_info;
idx = get(handles.DataFilesList,'Value');

if (~isempty(idx)) % only if the data exist
    % === filter applied to STED2 image
    filter_list_2 = get(handles.FilterType_2, 'String'); % read the list of filters
    filter_value_2 = get(handles.FilterType_2, 'Value'); % which one is selected
    filter_type_2 = char(filter_list_2(filter_value_2));

    if(strcmp(filter_type_2, 'No filter'))
        % if no filter type is selected
        disp('Error! Please choose the type of filter for STED2!');
        msgstr = sprintf('Error! Please choose the type of filter for STED2!'); 
        set(handles.StatusMsgStr,'String',msgstr);
        drawnow();

    else % Only if the type of filter for STED2 is select do we start the following calculations.
        % Ask for the range for calculation
        % Ask for the range for calculation
        prompt = {'Sampling','Number','Range of filter: from','Range of filter: step','Range of filter: to','Show decorr curves (y/n)?'};
        dlgtitle = 'Decorrelation analysis';
        dims = [1 35];
        definput = {'50','10','2','1','10','y'}; % default values
        input_info = inputdlg(prompt, dlgtitle, dims, definput); % cell array containing the inputs

        Nr = str2num(input_info{1});
        Ng = str2num(input_info{2});
        range_from = str2num(input_info{3});
        range_step = str2num(input_info{4});
        range_to = str2num(input_info{5});
        show_plots = input_info{6}; % Whether show the curves. Now  the curves are always shown. Later we may choose to hide them.
        range_filter_size = range_from:range_step:range_to; % calculation range of filter size applied to STED2.
        resolution = zeros(1, length(range_filter_size)); % initialize the resolution.

        set(handles.StatusMsgStr,'String','Calculating decorrelation of STEDD vs. the size of filter applied to STED2 ... '); 
        drawnow();
        tic

        %------ parameters setup --------
        save_res = get(handles.Save_Data_3, 'Value'); % Save the resolution values?
        px_size = str2double(get(handles.PxSizeEdit,'String')); % Pixel size in nm.
        neg = get(handles.NegCheck, 'Value'); % whether or not to neglect negative intensities to do FRC for the STEDD image.
        islift = get(handles.LiftCheck, 'Value'); % whether STED1 image is lifted up to avoid negative counts in STEDD.
        % The amount of baseline added is just enough so that the minimum intensity in image_odd and
        % image_even in STEDD is zero.
        AllFr_value = get(handles.AllFrRadio,'Value'); % show all the frames or only show one of the frames.
        % Decorrelation only works for 2D images.

        gamma = str2double(get(handles.gamma,'String'));
        
        img_STED2 = getImage(handles, 'img2', 'all'); % get STED2 image, which is not filtered. It can be a 3D image.

        % the type of filter applied to STEDD image
        filter_list_3 = get(handles.FilterType_3, 'String'); % read the list of filters
        filter_value_3 = get(handles.FilterType_3, 'Value'); % which one is selected
        filter_type_3 = char(filter_list_3(filter_value_3));
        filter_px_3 = str2double(get(handles.Filter3_px, 'String'));
        % ==== end of parameter setup. ======

        % ------ calculation ----------
        if(~AllFr_value) % if the checkbox is not checked, then work on the specified frame.
            FrID = str2double(get(handles.FrIDEdit,'String')); % The ID of frame that is going to show.
            if(FrID <= size(bh_info(idx).img_1, 3)) % if the frame ID is not out of range.

                % ===== Start start the loop for STED2
                for ii = 1:length(range_filter_size)
                %  =====     filter STED1 image
                    switch filter_type_2
                        case 'Wiener filter'
                            img_STED2_filtered = wiener2(img_STED2(:,:,FrID), [range_filter_size(ii), range_filter_size(ii)]);
                        case 'Gaussian filter'
                            img_STED2_filtered = imgaussfilt(img_STED2(:,:,FrID), range_filter_size(ii));
                        case 'Average filter'
                            img_STED2_filtered = filter2(fspecial('average', range_filter_size(ii)), img_STED2(:,:,FrID));
                        case 'Median filter'
                            img_STED2_filtered = medfilt2(img_STED2(:,:,FrID), [range_filter_size(ii), range_filter_size(ii)]);
                    end

                    img_STEDD = bh_info(idx).img_1(:,:,FrID) - gamma*img_STED2_filtered; % STEDD image. 2D image. Refreshed in every loop.

                    % ===== some operation to STEDD image =====
                    if(neg) % neglect negative intensities to do FRC for the STEDD image.
                        img_STEDD = max(img_STEDD, 0);
                    else
                        if(islift)
                            if(min(min(img_STEDD)) < 0)
                                img_STEDD = img_STEDD - min(min(img_STEDD));
                            end
                        end
                    end

                    % ====== filter STEDD image ===============
                    if(~strcmp(filter_type_3, 'No filter')) % if an option other than "No filter" is selected.
                        switch filter_type_3
                            case 'Wiener filter'
                                img_STEDD = wiener2(img_STEDD, [filter_px_3, filter_px_3]);
                            case 'Gaussian filter'
                                img_STEDD = imgaussfilt(img_STEDD, filter_px_3);
                            case 'Average filter'
                                img_STEDD = filter2(fspecial('average', filter_px_3), img_STEDD);
                            case 'Median filter'
                                img_STEDD = medfilt2(img_STEDD, [filter_px_3, filter_px_3]);
                        end
                    end

                    % ======= Decorrelation calculation for STEDD ===================
                    [~,~,resolution(ii)] = main_imageDecorr(img_STEDD, px_size, Nr, Ng);

                end % End of the loop for filter size 2.

            else % if the FrID is out of range.
                disp('Error! Frame ID larger than  the number of valid stacks!');
                msgstr = sprintf('Error! Frame ID larger than  the number of valid stacks!'); 
                set(handles.StatusMsgStr,'String',msgstr);
                drawnow();
            end
        else % Otherwise, show all the images. If the checkbox is checked, then the frame number is meaningless.

            % ===== Start start the loop for STED2
            for ii = 1:length(range_filter_size)
            %  =====     filter STED1 image
                switch filter_type_2
                    case 'Wiener filter'
                        img_STED2_filtered = wiener2(img_STED2, [range_filter_size(ii), range_filter_size(ii)]);
                    case 'Gaussian filter'
                        img_STED2_filtered = imgaussfilt(img_STED2, range_filter_size(ii));
                    case 'Average filter'
                        img_STED2_filtered = filter2(fspecial('average', range_filter_size(ii)), img_STED2);
                    case 'Median filter'
                        img_STED2_filtered = medfilt2(img_STED2, [range_filter_size(ii), range_filter_size(ii)]);
                end

                img_STEDD = bh_info(idx).img_1 - gamma*img_STED2_filtered; % STEDD image. 2D image. Refreshed in every loop.

                % ===== some operation to STEDD image =====
                    if(neg) % neglect negative intensities to do decorrelation for the STEDD image.
                        img_STEDD = max(img_STEDD, 0);
                    else
                        if(islift)
                            if(min(min(img_STEDD)) < 0)
                                img_STEDD = img_STEDD - min(min(img_STEDD));
                            end
                        end
                    end

                % ====== filter STEDD image ===============
                    if(~strcmp(filter_type_3, 'No filter')) % if an option other than "No filter" is selected.
                        switch filter_type_3
                            case 'Wiener filter'
                                img_STEDD = wiener2(img_STEDD, [filter_px_3, filter_px_3]);
                            case 'Gaussian filter'
                                img_STEDD = imgaussfilt(img_STEDD, filter_px_3);
                            case 'Average filter'
                                img_STEDD = filter2(fspecial('average', filter_px_3), img_STEDD);
                            case 'Median filter'
                                img_STEDD = medfilt2(img_STEDD, [filter_px_3, filter_px_3]);
                        end
                    end

                % ======= Decorrelation calculation for STEDD ===================
                    [~,~,resolution(ii)] = main_imageDecorr(img_STEDD, px_size, Nr, Ng);

            end % End of the loop for filter size 2.
        end % end of "all frames"

        % Now plot decorrelation resolution against filter size.
        f = figure;
        plot(range_filter_size, resolution, 'sr', 'LineWidth', 1.5);
        xlabel(strcat(filter_type_2, ' size applied to STED2 image (px)'));
        ylabel('Decorrelation resolution (nm)');
        title('Decorrelation resolution, STEDD');
        set(findall(f,'-property','FontSize'),'FontSize',14);

        % Now save the decorrelation resolution values
        if (save_res)
            % save decorrelation resolution values.
            res = [range_filter_size' resolution'];
            img_fn = fullfile(fp,strcat(fn,'_STEDD_gamma_',num2str(gamma),'_',t1,'to',t2,'_',t3,'to',t4,...
                '_STED2_', filter_type_2, '_', range_from,'to',range_to,'_dcorr_res','.txt'));
            [img_fn, img_fp] = uiputfile(img_fn,'Save decorrelation ressolutions as...'); % directory and name of the file.
            if (~isequal(img_fn,0) && ~isequal(img_fp,0))
                msgstr = sprintf('Saving decorrelation resolutions ...'); 
                set(handles.StatusMsgStr,'String',msgstr);
                drawnow();
                save(fullfile(img_fp, img_fn), 'res','-ascii'); 
            end

            t = toc;
            msgstr = sprintf('Calculated and saved decorrelation in %5.1f s',t); 
            set(handles.StatusMsgStr,'String',msgstr);
            drawnow();
        else
            t = toc;
            msgstr = sprintf('Calculated decorrelation in %5.1f s',t); 
            set(handles.StatusMsgStr,'String',msgstr);
            drawnow();
        end
    end
end
