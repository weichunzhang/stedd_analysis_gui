%clear all; clc;


%% Einlesen
[files, path] = uigetfile('S:\data\Yuji\Coworkers\Carmen Fricke\Lifetime Fitting\*.dat', 'Pick all dat-files','MultiSelect','on');
load('IRFS.mat')

choice = questdlg('Number of Exponentials','NumExp','1','2','1');

switch choice
    case '1'
        
        numExp = 1;
    case '2'
        
        numExp = 2;
end

maxChan = 3216;  %1564 for 40MHz, 3126 for 20Mhz
%firstChan = 475; %select range 250 for red ex; 475 for green
IRF=IRFyuji(1:maxChan);
%IRF=IRFDetektor2green(1:maxChan);

%IRF=IRFDetektor1red(1:maxChan);


for i=1:length(files)

     fid=fopen([path,files{i}]); %read concentration from 1st header line
    C = textscan(fid,'%*s %f',1);
    data{i}.conc=C{1}; clear C;
    %fclose(fid);
    
 %data{i}=dlmread([path,files{i}], '\t', 'headerlines',9);
 tempdata=textscan(fid, '%f %s %f %s %s');
 data{i}.x=tempdata{1};
 data{i}.y=tempdata{3};
 
%  donorlifetime(:,i)=histc(tempdata(:,4),x);
%  acceptorlifetime(:,i)=histc(tempdata(:,6),x);

% plot(x,donorlifetime(:,i))
% hold on
end

%% Bubble Sort
switchflag=1;
while switchflag==1
    switchflag=0;
    for r=1:(length(data)-1)
        if data{r}.conc > data{r+1}.conc
            store=data{r};
            data{r}=data{r+1};
            data{r+1}=store;
            switchflag=1;
        end
    end
end






%% Background Correction
 bckgrndIRF = mean(IRF(30:140));
% IRF = IRF - bckgrndIRF;
% vnan = isnan(IRF);
% IRF(vnan) = 0;

%% Generate parameters for fit
    %(A,tau,bckgrd,bckgrdIRF,Timeshit IRF)
    %(D1 , D2 , tau_F , tau_R , k1 , k2 , bckgrd1 , bckgrd2 , bckgrdIRF1, Timeshift IRF1, bckgrdIRF2 ,Timeshift IRF2, D3, D4, tau_F2)
    A = 5E2;
    tau = 1;
    bckgrd = 20;
   %bckgrndIRF = 20;
    %tsIRF = 0.04;
    tsIRF =-0.090813334497495;
    
     A2 = 5E2;
    tau2 = 4;
    
%     fitpara=[A, tau, tsIRF ];
%     lb= [0, 0, -2];
%     ub= [1E7 , 5, 2];
%     vary=[1,1,1];
    
%     %------ Fitpara mit Bckgrnd Fit
    if (numExp == 1)
    fitpara=[A, tau, bckgrd, bckgrndIRF, tsIRF ];
	lb= [0, 0, 0, bckgrndIRF-10, -2];
    ub= [1E7 , 5, 200, bckgrndIRF+10, 2];
    vary=[1,1,1,1,1];
    elseif (numExp == 2)
    fitpara=[A, tau, bckgrd, bckgrndIRF, tsIRF, A2, tau2 ];
	lb= [0, 0, 0, bckgrndIRF-10, -2, 0, 0];
    ub= [1E7 , 7, 200, bckgrndIRF+10, 2, 1E7, 7];
    vary=[1,1,1,1,0,1,1];
    end
  
%     %---------
%     
    
%     lb= [0, 0, 0, 0, 0];
%     ub= [1E7 , 5, 90, 1000, 1];
%     %ub= [1E6 , 1E6, 5, 50, 10 , 10, 500 , 500 , 5 , 1 , 10 , 1];
%     vary=[1,1,1,1,1];
    constants=fitpara; 

        


%% Fit & Plot
x=0:0.1:50;
for i=1:length(data)



    
    
options = optimset('MaxIter',1e5, 'MaxFunEval',1e3,'Display','off','TolX',1e-5,'TolFun',1e-5);
    
    exitflag=0;
    data2fittempx=data{i}.x;
    data2fittempy=data{i}.y;
    
    
%     %---- background correction data
      bckgrd = mean(data2fittempy(30:140));
      fitpara(3) = bckgrd;
      lb(3) = bckgrd - 2;
      ub(3) = bckgrd + 2;
%      data2fittempy = data2fittempy - bckgrd;
%      vnan = isnan(data2fittempy);
%      data2fittempy(vnan) = 0;     
%     %-------
    
    data2fitx=data2fittempx(1:maxChan,1); %1564 for 40MHz, 3126 for 20Mhz
    data2fity=data2fittempy(1:maxChan,1);
    
    sqrtw=sqrt(1./max(data2fity,1));
    
    while exitflag==0
        %[fitpara_lin,R,J,CovB] = nlinfit(data2fitx,data2fity,@e_func2,fitpara);
       
            [fitpara,resnorm,residual,exitflag,~,~,Jac] = lsqcurvefit(@IntDecay_ltFit,fitpara,data2fitx,sqrtw.*data2fity,lb,ub,options,constants, vary, IRF, sqrtw, numExp);
      
        %[fitpara,resnorm,residual,exitflag,~,~,Jac] = lsqcurvefit(@e_func2,fitpara,data2fitx,data2fity,lb,ub,options);
    end
    
    data{i}.A=fitpara(1);
    data{i}.tau=fitpara(2);
    data{i}.bckgrd = fitpara(3);
    data{i}.bckgrndIRF = fitpara(4);
    data{i}.tsIRF = fitpara(5);
    if (numExp == 2)
        data{i}.A2=fitpara(6);
        data{i}.tau2=fitpara(7);
    elseif (numExp == 1)
        data{i}.A2=0;
        data{i}.tau2=0;
    end
  
%   bckgrd_array(i) = fitpara(3);
%   bckgrndIRF_array(i) = fitpara(4);
  fit = IntDecay_ltFit(fitpara,data2fitx,constants, vary, IRF,sqrtw, numExp)./sqrtw;
  data{i}.fitx=data2fitx;
  data{i}.fity=fit;
  
  
  subplot(4,3,i)
  semilogy(data2fitx,data2fity,data2fitx,fit);
  xlim([1 35])
  drawnow;
  
  %Fehlerberechnung
  [Q,R] = qr(Jac,0);
mse = sum(abs(residual).^2)/(size(Jac,1)-size(Jac,2));
Rinv = inv(R);
Sigma = Rinv*Rinv'*mse;
se = sqrt(diag(Sigma));
se = full(se);
  
  %error_lin=sqrt(diag(CovB))

%   data{i}.errAmp_lin=error_lin(1);  
%   data{i}.errtau_lin=error_lin(2);
  data{i}.errAmp=se(1);  
  data{i}.errtau=se(2);
  
  if (numExp == 2)
      data{i}.errAmp2=se(6);
      data{i}.errtau2=se(7);
  elseif (numExp == 1)
      data{i}.errAmp2=0;
      data{i}.errtau2=0;
  end
  
end


   for i=1:length(data)
   subplot(4,3,[7 9])
   errorbar(data{i}.conc,data{i}.tau,data{i}.errtau)
   xlim([-5 55]);
   hold on
   
   %prepara Data for linear Fit
   datalinfitx(i)=data{i}.conc;
   datalinfity(i)=data{1}.tau/data{i}.tau;
   end
   
   
   % Linear Fit
   
   [linfit,s]=polyfit(datalinfitx,datalinfity,1);   
   se =sqrt(diag(inv(s.R)*inv(s.R')).*s.normr.^2./s.df);
   linfitplot=linfit(2)+linfit(1)*datalinfitx;
   %%%%%%
   
   
     
   subplot(4,3,[10 12])
   plot(datalinfitx,datalinfity,'ok')
   hold on  
   plot(datalinfitx,linfitplot)
    hold off

%% Save Data
saveFilename = 'LTData.dat';
delete(saveFilename);
fid1 = fopen(saveFilename,'w');

fprintf(fid1,'%s \t','%x');

saveVarData(:,1)=data{1}.x;
for i=1:length(data)
saveVarData(:,i+1)=data{i}.y; %chan1 data

fprintf(fid1,'%s \t',int2str(data{i}.conc));

end
fprintf(fid1,' \r\n');

%fprintf(fid1,'%s \t %f \r\n %s \t %f \r\n %s \t %f \r\n %s \t %f \r\n','%tau_F',lambda(3),'%tau_R',lambda(4),'%k1',lambda(5),'%k2',lambda(6));
%fprintf(fid1,'%s \t %s \t %s \t %s \t %s \t %s \t %s \r\n','%x','IntChan1','FitChan1','ResidualsChan1','IntChan2','FitChan2','ResidualsChan2');
fclose(fid1);

dlmwrite(saveFilename, saveVarData(1:2189,:), 'delimiter', '\t','-append');


%% Save Fit
saveFilename = 'LTFit.dat';
delete(saveFilename);
fid1 = fopen(saveFilename,'w');

fprintf(fid1,'%s \t','%x');

saveVarFit(:,1)=data{1}.fitx;
for i=1:length(data)
saveVarFit(:,i+1)=data{i}.fity; %chan1 data

fprintf(fid1,'%s \t',int2str(data{i}.conc));

end
fprintf(fid1,' \r\n');

%fprintf(fid1,'%s \t %f \r\n %s \t %f \r\n %s \t %f \r\n %s \t %f \r\n','%tau_F',lambda(3),'%tau_R',lambda(4),'%k1',lambda(5),'%k2',lambda(6));
%fprintf(fid1,'%s \t %s \t %s \t %s \t %s \t %s \t %s \r\n','%x','IntChan1','FitChan1','ResidualsChan1','IntChan2','FitChan2','ResidualsChan2');
fclose(fid1);

dlmwrite(saveFilename, saveVarFit, 'delimiter', '\t','-append');
%%
%% Save Fit Parameter
saveFilename = 'LTFitPara.dat';
delete(saveFilename);
fid1 = fopen(saveFilename,'w');

fprintf(fid1,'%s \r\n %s \t %f \t %s \t %f \r\n %s \t %f \t %s \t %f \r\n','%Stern-Volmer-Plot:','%Slope',linfit(1),' +- ',se(1),'%y-intercept',linfit(2),' +-',se(2));


fprintf(fid1,'%s \t %s \t %s \t %s \t %s \t %s \t %s \t %s \t %s \t %s \t %s \t %s \r\n','%Conc','Amp','errAmp','tau','errtau','tau_0/tau','err_tau_0/tau','Fit_tau_0/tau','Amp2','errAmp2','tau2','errtau2');


for i=1:length(data)
%Calc Error in tau_0/tau

errtau_0_tau= sqrt((1/data{i}.tau)^2 * data{i}.errtau^2 +  (data{1}.tau/data{i}.tau^2)^2 * data{1}.errtau^2);
   

fprintf(fid1,'%f \t %f \t %f \t %f \t %f \t %f \t %f \t %f \t %f \t %f \t %f \t %f \r\n',data{i}.conc,data{i}.A,data{i}.errAmp,data{i}.tau,data{i}.errtau,data{1}.tau/data{i}.tau,errtau_0_tau,linfitplot(i),data{i}.A2,data{i}.errAmp2,data{i}.tau2,data{i}.errtau2);

end

%fprintf(fid1,'%s \t %f \r\n %s \t %f \r\n %s \t %f \r\n %s \t %f \r\n','%tau_F',lambda(3),'%tau_R',lambda(4),'%k1',lambda(5),'%k2',lambda(6));
%fprintf(fid1,'%s \t %s \t %s \t %s \t %s \t %s \t %s \r\n','%x','IntChan1','FitChan1','ResidualsChan1','IntChan2','FitChan2','ResidualsChan2');
fclose(fid1);

