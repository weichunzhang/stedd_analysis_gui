function mask = Mask(rt,dim,X,Y,Z)

    if dim == 3
    mask = Z.^2 + Y.^2 + X.^2 < rt^2;

    elseif dim == 2
    mask = Y.^2 + X.^2 < rt^2;

    end
    
end