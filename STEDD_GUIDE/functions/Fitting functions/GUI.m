function varargout = GUI(varargin)
% GUI MATLAB code for GUI.fig
%      GUI, by itself, creates a new GUI or raises the existing
%      singleton*.
%
%      H = GUI returns the handle to a new GUI or the handle to
%      the existing singleton*.
%
%      GUI('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in GUI.M with the given input arguments.
%
%      GUI('Property','Value',...) creates a new GUI or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before GUI_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to GUI_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help GUI

% Last Modified by GUIDE v2.5 10-May-2017 17:24:51


% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
    'gui_Singleton',  gui_Singleton, ...
    'gui_OpeningFcn', @GUI_OpeningFcn, ...
    'gui_OutputFcn',  @GUI_OutputFcn, ...
    'gui_LayoutFcn',  [] , ...
    'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before GUI is made visible.
function GUI_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to GUI (see VARARGIN)

% Choose default command line output for GUI
handles.output = hObject;
handles.path = pwd;
% Update handles structure
guidata(hObject, handles);

% UIWAIT makes GUI wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = GUI_OutputFcn(hObject, eventdata, handles)
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;

% --- Executes on button press in DoFit_pushbutton.
function DoFit_pushbutton_Callback(hObject, eventdata, handles)
% hObject    handle to DoFit_pushbutton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

handles.maxChan = str2double(get(handles.maxChan_edit,'String'));

data2fittempx=handles.x;
data2fittempy=handles.y;

options = optimset('MaxIter',1e5, 'MaxFunEval',1e3,'Display','off','TolX',1e-5,'TolFun',1e-5);
exitflag=0;

xdata=data2fittempx(1:handles.maxChan,1); %1564 for 40MHz, 3126 for 20Mhz; 
ydata=data2fittempy(1:handles.maxChan,1); 
sqrtw=sqrt(1./max(ydata,1)); 

IRF = zeros(handles.maxChan,1);
s_irf = min(length(handles.IRF),handles.maxChan);
IRF(1:s_irf) = handles.IRF(1:s_irf); 

handles.twoexp=get(handles.twoexp_checkbox,'Value');



fix = zeros(1,9);

bgndIRF = str2double(get(handles.bgndIRF_edit,'String'));
fix(1) = get(handles.fix_bgndIRF_checkbox,'Value');

tsIRF = str2double(get(handles.tsIRF_edit,'String'));
fix(2) = get(handles.tsIRF_fix_checkbox,'Value');

bgnd = str2double(get(handles.bgnd_edit,'String'));
fix(3) = get(handles.fix_bgnd_checkbox,'Value');


A = str2double(get(handles.A1_edit,'String')); % A total
fix(4) = 0;

tau1 = str2double(get(handles.tau1_edit,'String'));
fix(5) = get(handles.tau1_fix_checkbox,'Value');

NExpFunc = 5;

if (~handles.twoexp )
    f1 = 1;
    f2 = 0;
    tau2 = 0.1;
    f3 = 0;
    tau3 = 0.1;
    fix(6:9) = 1;
else
 
    f2 = str2double(get(handles.f2_edit,'String'));
    fix(6) = get(handles.A2_fix_checkbox,'Value');

    tau2 = str2double(get(handles.tau2_edit,'String'));
    fix(7) = get(handles.tau2_fix_checkbox,'Value');


    tau3 = str2double(get(handles.tau3_edit,'String'));
    fix(9) = get(handles.tau3_fix_checkbox,'Value');
  
end

set(handles.DoFit_pushbutton,'Enable','off');

while exitflag==0,

    if (get(handles.f3_popup,'Value') == 1) %independent fit of f3
        f3 = str2double(get(handles.f3_edit,'String'));
        fix(8) = get(handles.A3_fix_checkbox,'Value');
        
        %renormalize fractions
        f1 = min(1,max(0,1 - f2 - f3));
        f_s = f1 + f2 + f3;
        f2 = f2/f_s;
        f3 = f3/f_s;

        x0 = [bgndIRF, tsIRF, bgnd,    A,  tau1,  f2, tau2,   f3, tau3];
        lb = [0, -2, 0,                0,  0.1,    0, 0.1,     0, 0.1];
        ub = [1e7, 2, 100,            1e7, 10,     1, 30,      1, 100];

        [x_free,resnorm,residual,exitflag,~,~,Jac] =...
            lsqcurvefit(@(x, xdata)IntDecayMultiExp(interlace(x0,x,fix), xdata, IRF, sqrtw),...
            x0(~fix),xdata,sqrtw.*ydata,lb(~fix),ub(~fix),options);

        x = interlace(x0, x_free, fix);
        handles.fitpara = x;
        
    elseif (get(handles.f3_popup,'Value') == 2)
        k3 = str2double(get(handles.f3_edit,'String'));
        f3 = k3*f2;
        fix(8) = 1;
        
        %renormalize fractions
        f1 = min(1,max(0,1 - f2 - f3));
        f_s = f1 + f2*(1 + k3);
        f2 = f2/f_s;

        x0 = [bgndIRF, tsIRF, bgnd,    A,  tau1,  f2, tau2,   k3, tau3];
        lb = [0, -2, 0,                0,  0.1,    0, 0.1,     0, 0.1];
        ub = [1e7, 2, 100,            1e7, 10,     1, 30,      100, 100];

        [x_free,resnorm,residual,exitflag,~,~,Jac] =...
            lsqcurvefit(@(x, xdata)IntDecayMultiExpBound(interlace(x0,x,fix), xdata, IRF, sqrtw),...
            x0(~fix),xdata,sqrtw.*ydata,lb(~fix),ub(~fix),options);

        x = interlace(x0, x_free, fix);
        handles.fitpara = x;
        handles.fitpara(8) = x(8)*x(6);
        
    end
    
end

set(handles.DoFit_pushbutton,'Enable','on');


Fit_parameter=handles.fitpara; %Fitting parameter;
fit = IntDecayMultiExp(handles.fitpara,xdata,IRF(1:handles.maxChan),sqrtw)./sqrtw; %Fitting curves;

handles.fit = fit;
handles.res = residual;

%Calc Error
[Q,R] = qr(Jac,0);
mse = sum(abs(residual).^2)/(size(Jac,1)-size(Jac,2));
Rinv = inv(R);
Sigma = Rinv*Rinv'*mse;
se = sqrt(diag(Sigma));
se = full(se);
handles.fitpara_se = se;

%% recalc IRF function
IRFc_x = xdata - handles.fitpara(2);
IRFc_y = interp1(IRFc_x,IRF,xdata,'linear')-handles.fitpara(1);
IRFc_y = IRFc_y*(max(ydata)-handles.fitpara(1))/max(IRFc_y)+1;
%%

%Plot Data
axes(handles.axes1);
semilogy(xdata,ydata);
%semilogy(data2fitx,data2fity./max(data2fity)); %wei� den Grund f黵 das
%Teilen durch max(data2fity) nicht mehr. Plot sieht damit schlecht aus...
hold on;
semilogy(xdata,fit,'r', 'LineWidth', 1.5);
%semilogy(data2fitx,IRF(1:handles.maxChan)./max(IRF(1:handles.maxChan)));
semilogy(IRFc_x,IRFc_y,'-k');
hold off;

set(handles.axes1, 'XTick', []);
xlim([0 xdata(handles.maxChan)]);
ylim([0.8*handles.fitpara(3) handles.fitpara(4)]);

axes(handles.axes2);
plot(xdata,residual);
ylim([-7 7]);
xlim([0 xdata(handles.maxChan)]);

% figure(2)
% semilogy(data2fitx,data2fity);
% hold on;
% semilogy(data2fitx,fit,'r', 'LineWidth', 1.5);
% matlab2tikz( 'myfile.tikz' );

%Write Values to GUI
set(handles.bgndIRF_edit,'String',num2str(handles.fitpara(1)));
set(handles.tsIRF_edit,'String',num2str(handles.fitpara(2)));
set(handles.bgnd_edit,'String',num2str(handles.fitpara(3)));

set(handles.A1_edit,'String',num2str(handles.fitpara(4)));
set(handles.tau1_edit,'String',num2str(handles.fitpara(5)));
set(handles.f1_edit,'String',num2str(1 - handles.fitpara(6) - handles.fitpara(8)));




set(handles.A2_edit,'String',num2str(handles.fitpara(4)*handles.fitpara(6)));
set(handles.f2_edit,'String',num2str(handles.fitpara(6),'%6.2f'));
set(handles.tau2_edit,'String',num2str(handles.fitpara(7),'%6.2f'));

set(handles.A3_edit,'String',num2str(handles.fitpara(4)*handles.fitpara(8)));
set(handles.f3_edit,'String',num2str(x(8),'%6.2f'));
set(handles.tau3_edit,'String',num2str(handles.fitpara(9),'%6.2f'));


%Save Data
if (get(handles.SaveFit_checkbox,'value'))
    
    SaveFitData(handles, 1);
%     folder = handles.path;
%     [FileName,PathName] = uiputfile(fullfile(folder,'Fitting_result.mat'));
%     %cd E:\;
%     save(fullfile(PathName,FileName),'Fit_parameter','fit','data2fitx','IRFc_x','IRFc_y');
end

guidata(hObject, handles);




% --- Executes on button press in tau1_fix_checkbox.
function tau1_fix_checkbox_Callback(hObject, eventdata, handles)
% hObject    handle to tau1_fix_checkbox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of tau1_fix_checkbox



function tau1_edit_Callback(hObject, eventdata, handles)
% hObject    handle to tau1_edit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of tau1_edit as text
%        str2double(get(hObject,'String')) returns contents of tau1_edit as a double


% --- Executes during object creation, after setting all properties.
function tau1_edit_CreateFcn(hObject, eventdata, handles)
% hObject    handle to tau1_edit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in A1_fix_checkbox.
function A1_fix_checkbox_Callback(hObject, eventdata, handles)
% hObject    handle to A1_fix_checkbox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of A1_fix_checkbox



function A1_edit_Callback(hObject, eventdata, handles)
% hObject    handle to A1_edit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of A1_edit as text
%        str2double(get(hObject,'String')) returns contents of A1_edit as a double


% --- Executes during object creation, after setting all properties.
function A1_edit_CreateFcn(hObject, eventdata, handles)
% hObject    handle to A1_edit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in fix_bgnd_checkbox.
function fix_bgnd_checkbox_Callback(hObject, eventdata, handles)
% hObject    handle to fix_bgnd_checkbox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of fix_bgnd_checkbox



function bgnd_edit_Callback(hObject, eventdata, handles)
% hObject    handle to bgnd_edit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of bgnd_edit as text
%        str2double(get(hObject,'String')) returns contents of bgnd_edit as a double


% --- Executes during object creation, after setting all properties.
function bgnd_edit_CreateFcn(hObject, eventdata, handles)
% hObject    handle to bgnd_edit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in fix_bgndIRF_checkbox.
function fix_bgndIRF_checkbox_Callback(hObject, eventdata, handles)
% hObject    handle to fix_bgndIRF_checkbox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of fix_bgndIRF_checkbox



function bgndIRF_edit_Callback(hObject, eventdata, handles)
% hObject    handle to bgndIRF_edit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of bgndIRF_edit as text
%        str2double(get(hObject,'String')) returns contents of bgndIRF_edit as a double


% --- Executes during object creation, after setting all properties.
function bgndIRF_edit_CreateFcn(hObject, eventdata, handles)
% hObject    handle to bgndIRF_edit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in tsIRF_fix_checkbox.
function tsIRF_fix_checkbox_Callback(hObject, eventdata, handles)
% hObject    handle to tsIRF_fix_checkbox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of tsIRF_fix_checkbox



function tsIRF_edit_Callback(hObject, eventdata, handles)
% hObject    handle to tsIRF_edit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of tsIRF_edit as text
%        str2double(get(hObject,'String')) returns contents of tsIRF_edit as a double


% --- Executes during object creation, after setting all properties.
function tsIRF_edit_CreateFcn(hObject, eventdata, handles)
% hObject    handle to tsIRF_edit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in tau2_fix_checkbox.
function tau2_fix_checkbox_Callback(hObject, eventdata, handles)
% hObject    handle to tau2_fix_checkbox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of tau2_fix_checkbox



function tau2_edit_Callback(hObject, eventdata, handles)
% hObject    handle to tau2_edit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of tau2_edit as text
%        str2double(get(hObject,'String')) returns contents of tau2_edit as a double


% --- Executes during object creation, after setting all properties.
function tau2_edit_CreateFcn(hObject, eventdata, handles)
% hObject    handle to tau2_edit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in A2_fix_checkbox.
function A2_fix_checkbox_Callback(hObject, eventdata, handles)
% hObject    handle to A2_fix_checkbox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of A2_fix_checkbox



function A2_edit_Callback(hObject, eventdata, handles)
% hObject    handle to A2_edit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of A2_edit as text
%        str2double(get(hObject,'String')) returns contents of A2_edit as a double


% --- Executes during object creation, after setting all properties.
function A2_edit_CreateFcn(hObject, eventdata, handles)
% hObject    handle to A2_edit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in load_pushbutton.
function load_pushbutton_Callback(hObject, eventdata, handles)
% hObject    handle to load_pushbutton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

%\\aphc206w01.aph.kit.edu\LabC206$\Microtime\

if (~isempty(handles.path))
    folder = handles.path;
else
    folder = pwd;
end
[handles.file, handles.path] = uigetfile(fullfile(folder,'*.dat'), 'Pick Data File');

fid=fopen(fullfile(handles.path,handles.file),'r');
tempdata=textscan(fid, '%f %s %f %s %s', 'HeaderLines',18);
fclose(fid);

if (fid >= 0)
    set(handles.DataFilename,'String',handles.file); 
else
    set(handles.DataFilename,'String','error to open file'); 
end

handles.x=tempdata{1};
handles.y=tempdata{3};

%handles.maxChan = str2double(get(handles.maxChan_edit,'String'));

%if (size(handles.y,1) < handles.maxChan)
    handles.maxChan = size(handles.y,1);
    set(handles.maxChan_edit,'String',sprintf('%d',handles.maxChan));
%end

handles.bgnd = mean(handles.y(1:30));
%handles.Index=str2double(get(handles.trNum,'String'));
%set(handles.Background_blue,'String',num2str(handles.AverageGR));


set(handles.bgnd_edit,'String',num2str(handles.bgnd));

data2fittempx=handles.x;
data2fittempy=handles.y;

axes(handles.axes1);
semilogy(data2fittempx(1:handles.maxChan),data2fittempy(1:handles.maxChan));
if (strcmp(fieldnames(handles),'IRF'))
    hold on
    semilogy(handles.x(1:length(handles.IRF)),handles.IRF,'-k'); 
    hold off
end
set(handles.axes1, 'XTick', []);

set(handles.loadIRF_pushbutton,'Enable','on');

guidata(hObject, handles);



% --- Executes on button press in loadIRF_pushbutton.
function loadIRF_pushbutton_Callback(hObject, eventdata, handles)
% hObject    handle to loadIRF_pushbutton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

%\\aphc206w01.aph.kit.edu\LabC206$\Microtime\

if (~isempty(handles.path))
    folder = handles.path;
else
    folder = pwd;
end

[file, path] = uigetfile(fullfile(folder,'*.dat'), 'Pick IRF');
fid=fopen(fullfile(path,file));
tempdata=textscan(fid, '%f %s %f %s %s', 'HeaderLines',18);
fclose(fid);

if (fid >= 0)
    set(handles.IRFFilename,'String',file); 
else
    set(handles.IRFFilename,'String','error to open IRF file'); 
end


handles.IRF = zeros(handles.maxChan,1);
irf_len = length(tempdata{3});
if (irf_len > handles.maxChan)
    irf_len = handles.maxChan;
end
handles.IRF(1:irf_len)=tempdata{3}(1:irf_len);

handles.bgndIRF = mean(handles.IRF(1:30));
%figure(100); semilogy(handles.x(1:handles.maxChan),handles.IRF(1:handles.maxChan)); title('IRF');

axes(handles.axes1);
semilogy(handles.x(1:handles.maxChan),handles.y(1:handles.maxChan));
hold on
semilogy(handles.x(1:handles.maxChan),handles.IRF(1:handles.maxChan),'-k'); 
hold off
set(handles.axes1, 'XTick', []);



set(handles.bgndIRF_edit,'String',num2str(handles.bgndIRF));
guidata(hObject, handles);


% --- Executes on button press in twoexp_checkbox.
function twoexp_checkbox_Callback(hObject, eventdata, handles)
% hObject    handle to twoexp_checkbox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of twoexp_checkbox



function maxChan_edit_Callback(hObject, eventdata, handles)
% hObject    handle to maxChan_edit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of maxChan_edit as text
%        str2double(get(hObject,'String')) returns contents of maxChan_edit as a double


% --- Executes during object creation, after setting all properties.
function maxChan_edit_CreateFcn(hObject, eventdata, handles)
% hObject    handle to maxChan_edit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on slider movement.
function slider1_Callback(hObject, eventdata, handles)
% hObject    handle to slider1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider


% --- Executes during object creation, after setting all properties.
function slider1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to slider1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes on button press in tau3_fix_checkbox.
function tau3_fix_checkbox_Callback(hObject, eventdata, handles)
% hObject    handle to tau3_fix_checkbox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of tau3_fix_checkbox



function tau3_edit_Callback(hObject, eventdata, handles)
% hObject    handle to tau3_edit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of tau3_edit as text
%        str2double(get(hObject,'String')) returns contents of tau3_edit as a double


% --- Executes during object creation, after setting all properties.
function tau3_edit_CreateFcn(hObject, eventdata, handles)
% hObject    handle to tau3_edit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in A3_fix_checkbox.
function A3_fix_checkbox_Callback(hObject, eventdata, handles)
% hObject    handle to A3_fix_checkbox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of A3_fix_checkbox



function A3_edit_Callback(hObject, eventdata, handles)
% hObject    handle to A3_edit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of A3_edit as text
%        str2double(get(hObject,'String')) returns contents of A3_edit as a double


% --- Executes during object creation, after setting all properties.
function A3_edit_CreateFcn(hObject, eventdata, handles)
% hObject    handle to A3_edit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in SaveFit_checkbox.
function SaveFit_checkbox_Callback(hObject, eventdata, handles)
% hObject    handle to SaveFit_checkbox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of SaveFit_checkbox

    

    
% --- Executes on button press in SaveFitBtn.
function SaveFitBtn_Callback(hObject, ~, handles)
% hObject    handle to SaveFitBtn (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

SaveFitData(handles, 0);

function SaveFitData(handles, autosave)

PathName = handles.path;
FileName = 'Fitting_result.mat';
saveFileName = fullfile(PathName,FileName);

if (~autosave)
    [FileName,PathName] = uiputfile(saveFileName,'save data as');
    saveFileName = fullfile(PathName,FileName);

end


fitpara = handles.fitpara;
fitpara_se = handles.fitpara_se;

FitData = zeros(handles.maxChan,5);

FitData(:,1)=handles.x(1:handles.maxChan);
%saveVarData(:,1)=data2fitx;
FitData(:,2)=handles.y(1:handles.maxChan); %chan1 data
FitData(:,3)=handles.fit(1:handles.maxChan); %fit curve
FitData(:,4)=handles.res(1:handles.maxChan); %residuals
FitData(:,5)=handles.IRF(1:handles.maxChan); %IRF

save(saveFileName,'fitpara','FitData');



% save fit data
[fp,fn,fext] = fileparts(handles.file);
FileName = strcat(fn,'_FitData.dat');
saveFileName = fullfile(PathName,FileName);

if (~autosave)
    [FileName,PathName] = uiputfile('*.dat','Save Fit Data',saveFileName);
    saveFileName = fullfile(PathName,FileName);
end

%delete(saveFilename);

fid1 = fopen(saveFileName,'w');
fprintf(fid1,'time \t data \t fit \t residuals \t IRF\r\n');
fclose(fid1);

dlmwrite(saveFileName, FitData, 'delimiter', '\t','precision','%.4f','newline','pc','-append');


% Save Fit Parameter
saveFileName = fullfile(PathName,strcat(fn,'_FitPara.dat'));

%delete(saveFilename);
if (~autosave)
    [FileName,PathName] = uiputfile('*.dat','Save Fit Parameters',saveFileName);
    saveFileName = fullfile(PathName,FileName);
end

fid1 = fopen(saveFileName,'w');

if (handles.twoexp)
fprintf(fid1,'%s \t %s \t %s \t %s \t %s \t %s \t %s \t %s \t \r\n','A1','errA1','tau1','errtau1','A2','errA2','tau2','errtau2','A3','errA3','tau3','errtau3');
fprintf(fid1,'%f \t %f \t', fitpara(1), fitpara_se(1) , fitpara(2), fitpara_se(2) , fitpara(6), fitpara_se(6) , fitpara(7), fitpara_se(7), fitpara(8), fitpara_se(8) , fitpara(9), fitpara_se(9));
else    
fprintf(fid1,'%s \t %s \t %s \t %s \t \r\n','A1','errA1','tau1','errtau1');
fprintf(fid1,'%f \t %f \t', fitpara(1), fitpara_se(1) , fitpara(2), fitpara_se(2));
end

fprintf(fid1,' \r\n');
%fprintf(fid1,'%s \t %f \r\n %s \t %f \r\n %s \t %f \r\n %s \t %f \r\n','%tau_F',lambda(3),'%tau_R',lambda(4),'%k1',lambda(5),'%k2',lambda(6));
%fprintf(fid1,'%s \t %s \t %s \t %s \t %s \t %s \t %s \r\n','%x','IntChan1','FitChan1','ResidualsChan1','IntChan2','FitChan2','ResidualsChan2');
fclose(fid1);



function f1_edit_Callback(hObject, eventdata, handles)
% hObject    handle to f1_edit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of f1_edit as text
%        str2double(get(hObject,'String')) returns contents of f1_edit as a double


% --- Executes during object creation, after setting all properties.
function f1_edit_CreateFcn(hObject, eventdata, handles)
% hObject    handle to f1_edit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function f2_edit_Callback(hObject, eventdata, handles)
% hObject    handle to f2_edit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of f2_edit as text
%        str2double(get(hObject,'String')) returns contents of f2_edit as a double


% --- Executes during object creation, after setting all properties.
function f2_edit_CreateFcn(hObject, eventdata, handles)
% hObject    handle to f2_edit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function f3_edit_Callback(hObject, eventdata, handles)
% hObject    handle to f3_edit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of f3_edit as text
%        str2double(get(hObject,'String')) returns contents of f3_edit as a double


% --- Executes during object creation, after setting all properties.
function f3_edit_CreateFcn(hObject, eventdata, handles)
% hObject    handle to f3_edit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in f3_popup.
function f3_popup_Callback(hObject, eventdata, handles)
% hObject    handle to f3_popup (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns f3_popup contents as cell array
%        contents{get(hObject,'Value')} returns selected item from f3_popup


% --- Executes during object creation, after setting all properties.
function f3_popup_CreateFcn(hObject, eventdata, handles)
% hObject    handle to f3_popup (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function tau4_edit_Callback(hObject, eventdata, handles)
% hObject    handle to tau4_edit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of tau4_edit as text
%        str2double(get(hObject,'String')) returns contents of tau4_edit as a double


% --- Executes during object creation, after setting all properties.
function tau4_edit_CreateFcn(hObject, eventdata, handles)
% hObject    handle to tau4_edit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function tau5_edit_Callback(hObject, eventdata, handles)
% hObject    handle to tau5_edit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of tau5_edit as text
%        str2double(get(hObject,'String')) returns contents of tau5_edit as a double


% --- Executes during object creation, after setting all properties.
function tau5_edit_CreateFcn(hObject, eventdata, handles)
% hObject    handle to tau5_edit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in tau4_fix_checkbox.
function tau4_fix_checkbox_Callback(hObject, eventdata, handles)
% hObject    handle to tau4_fix_checkbox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of tau4_fix_checkbox


% --- Executes on button press in tau5_fix_checkbox.
function tau5_fix_checkbox_Callback(hObject, eventdata, handles)
% hObject    handle to tau5_fix_checkbox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of tau5_fix_checkbox



function A4_edit_Callback(hObject, eventdata, handles)
% hObject    handle to A4_edit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of A4_edit as text
%        str2double(get(hObject,'String')) returns contents of A4_edit as a double


% --- Executes during object creation, after setting all properties.
function A4_edit_CreateFcn(hObject, eventdata, handles)
% hObject    handle to A4_edit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function A5_edit_Callback(hObject, eventdata, handles)
% hObject    handle to A5_edit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of A5_edit as text
%        str2double(get(hObject,'String')) returns contents of A5_edit as a double


% --- Executes during object creation, after setting all properties.
function A5_edit_CreateFcn(hObject, eventdata, handles)
% hObject    handle to A5_edit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function f4_edit_Callback(hObject, eventdata, handles)
% hObject    handle to f4_edit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of f4_edit as text
%        str2double(get(hObject,'String')) returns contents of f4_edit as a double


% --- Executes during object creation, after setting all properties.
function f4_edit_CreateFcn(hObject, eventdata, handles)
% hObject    handle to f4_edit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function f5_edit_Callback(hObject, eventdata, handles)
% hObject    handle to f5_edit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of f5_edit as text
%        str2double(get(hObject,'String')) returns contents of f5_edit as a double


% --- Executes during object creation, after setting all properties.
function f5_edit_CreateFcn(hObject, eventdata, handles)
% hObject    handle to f5_edit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in A5_fix_checkbox.
function A5_fix_checkbox_Callback(hObject, eventdata, handles)
% hObject    handle to A5_fix_checkbox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of A5_fix_checkbox


% --- Executes on button press in A4_fix_checkbox.
function A4_fix_checkbox_Callback(hObject, eventdata, handles)
% hObject    handle to A4_fix_checkbox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of A4_fix_checkbox
