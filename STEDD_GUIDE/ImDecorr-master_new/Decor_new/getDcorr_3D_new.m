% [d,im,r] = getDcorr_3D_new(im,pix,func,e,n)
% ---------------------------------------
%
% Estimate the image cut-off frequency based on decorrelation analysis
%
% Inputs:
%  im        	2D image to be analyzed
%  r           	Fourier space sampling of the analysis (default: r = linspace(0,1,50)
%  func			choose 0 or 1, do high pass filtering in the real space or
%               frequency space?
%  e			epsilon
%  n            Nr
%
% Outputs:
%  kcMax        Estimated cut-off frequency of the image in normalized frequency
%  A0			Amplitude of the local maxima of d0
%  kcGM			Estimated cut-off frequency using Geometric-Mean metric
%  d0 			Decorrelation function before high-pass filtering
%  d			All decorrelation functions


function [d,im,r] = getDcorr_3D_new(im,pix,func,e,n)

dim = length(size(im));%3D or 2D 
im = double(im);

%further check for 3D and 2D images 
s = size(im);
im = check_odd(dim,s,im); %check odd number of pixels in the image 
s = size(im); %size of the image after pixel number check

%Meshgrid
[X,Y,Z] = mesh(dim,s);

%Fourier Transform with shift to zero frequencies
I = Fourier(im); 

%Normalisation of Fourier Transform
I = Normalization(I);

%Binary Mask 
mask0 = Mask(1,dim,X,Y,Z);

%Make a circular in Fourier Space with radius 1
I = mask0.*I; 

%Smoothing ??? 
ims = (im-mean(im(:)))/std(im(:));
Ik = mask0.*Fourier(ims);

%Inverse Fourier Transform with shift
imr = Real(Ik);

%Compute dcorr 0 and find its maxima
imt = imr; % array that is used for computation exact this curve
count = 0; %number of coefficients computed 

%First curve computation
Ir = mask0.*Fourier(imt);
Ir = Ir(1:(end-1)/2); % remove the mean & speed up computation
c = sqrt(sum(sum(abs(Ir).^2))); % 2 sums???? module value for coefficient computation

%dc - number of curves, d - curve, kc - frequency of the maximum, 
%SNR - cross-cor. coefficient at the max position, 
%gm - geometrical mean value

res_all=zeros(3,1);
tic

kc = []; SNR = []; 



%%
r = linspace(0,1,n);
r(2,:) = linspace(0,1,n);

d = [];
dc = 0;
h = 1; 
snr = 0; 
dt = 0.01;

g(1) = 0; 
g(2) = 2/r(2,2); 



for i = 1:2 
    
 if func == 0
    
 imt = Ik.*Gaus_high_fourier(dim,Z,Y,X,g(i),h); 
 Ir = mask0.*imt;
 
 elseif func == 1 
     
 imt = Gaus_high_real(dim,imr,g(i),h);
 Ir = mask0.*Fourier(imt);
 
 end

 Ir = Ir(1:(end-1)/2);
 dc = dc + 1;
    
 [d,count] = decor_curve_new(I,Ir,count,dim,Z,Y,X,r(i,:),dc,c,d);
 [ind,snr] = getDcorrMax_new(d(:,dc),dt);
 kc(i) = r(i,ind);
 SNR(i) = snr;

 ID(i) = ind;

end 

%find the dt from the curve; suggest find min position of the first curve and after get dt
%from points around min position 
[~,idx] = min(d(ID(1):end,1));
dif = 0.008*n*mean(abs(diff(d(idx - round(0.1*n):idx,1)))); %set increase for dt depending on number of points n
dt = dif; 
%dt = 0.001;


while abs(g(i) - g(i-1)) > e
      
    i = i + 1;
    
    %may be changed to increase speed of computation
    if max(kc) > 2*max(kc) && max(kc) < 2*max(kc)
    r(i,1:n) = linspace(max(kc) - 2*max(kc),max(kc) + 2*max(kc),n);
    else 
    r(i,1:n) = linspace(0,1,n);
    end
    
    if snr ~= 0
        
    g(i) = g(i-1) - abs((g(i-1) - g(i-2)))/2;
        
    elseif snr == 0 
            
    g(i) = g(i-1) + abs((g(i-1) - g(i-2)))/2;
        
    end
    
    %Home-made Gaussian Filter Fourier Space 
    if func == 0 
        
    imt = Ik.*Gaus_high_fourier(dim,Z,Y,X,g(i),h); 
    Ir = mask0.*imt;
    
    %In-built Gaussian Filter Real Space unlock for usage in-built function
    
    elseif func == 1
        
    imt = Gaus_high_real(dim,imr,g(i),h);
    Ir = mask0.*Fourier(imt);
    
    end 
    
    Ir = Ir(1:(end-1)/2);
    dc = dc + 1;
    
    [d,count] = decor_curve_new(I,Ir,count,dim,Z,Y,X,r(i,:),dc,c,d);
    [ind,snr] = getDcorrMax_new(d(:,dc),dt);
    kc(i) = r(i,ind);
    SNR(i) = snr;
   
    ID(i) = ind;

end 

res = 2*pix/max(kc);
[~,curve] = max(kc);

figure(2)

hold on 

for k = 1:length(d(1,1:end))
    plot(r(k,:),d(:,k));
end


for k = 1:length(kc)
    if SNR(k)~=0
    plot(kc(k),SNR(k),'bx','linewidth',1)
    end
end 

%find error 
[er,er_res] = err(d,kc,r,ID,pix,dt);
er_rel = 100*er_res/res;

xlabel('Normalized spatial frequencies')
ylabel('C.c. coefficients')
title(['Dcor analysis : res ~ '+string(round(res,1))+' +- '+string(round(er_res,1))+' nm',string(n)+' points',string(i)+' filter',string(curve)+' curve']);

hold off

figure(3);
plot(g);

title(['Dcor analysis : res ~ '+string(round(res,1))+' +- '+string(round(er_res,1))+' nm',string(n)+' points',string(i)+' filter',string(curve)+' curve']);
xlabel('Number of the filter')
ylabel('Sigma,nm')

figure(4);
plot(g,SNR);
title(['Dcor analysis : res ~ '+string(round(res,1))+' +- '+string(round(er_res,1))+' nm',string(n)+' points',string(i)+' filter',string(curve)+' curve']);
xlabel('Sigma,nm')
ylabel('SNR')
toc

figure(5);
plot(r(curve,:),d(:,curve));
hold on 
plot(kc(curve),SNR(curve),'bx','linewidth',1)
hold off
xlabel('Normalized spatial frequencies')
ylabel('C.c. coefficients')
title(['Dcor analysis : res ~ '+string(round(res,1))+' +- '+string(round(er_res,1))+' nm',string(n)+' points',string(i)+' filter',string(curve)+' curve']);

res_all(1,end+1) = res;
res_all(2,end) = i;
res_all(3,end) = curve;

%f = fit_pol(d,r,kc);

end
    