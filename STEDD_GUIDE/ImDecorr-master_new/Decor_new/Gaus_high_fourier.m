function gauss_high=Gaus_high_fourier(dim,Z,Y,X,g,h,Ng)

    if g~=0
    
    if dim == 3
     
    %gauss_high = 1 - exp(-(Z.^2+Y.^2+X.^2)/(2*g_f(h)^2)); 
    gauss_high = 1 - exp(-2*pi^2*g(h)^2*(Z.^2+Y.^2+X.^2)); 
    
    elseif dim == 2
    
    gauss_high = 1 - exp(-2*pi^2*g(h)^2*(Y.^2+X.^2));    
    
    end
    
    else
        
    gauss_high = 1;
    
    end
    
end