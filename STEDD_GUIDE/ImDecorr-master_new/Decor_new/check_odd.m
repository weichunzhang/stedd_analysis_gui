function im=check_odd(dim,s,im)

    if dim==3
    im = im(1:end-not(mod(s(1),2)),1:end-not(mod(s(2),2)),1:end-not(mod(s(3),2))); % odd number of pixels

    elseif dim==2
    im = im(1:end-not(mod(s(1),2)),1:end-not(mod(s(2),2)));

    end
    
end