function y = gauss_1d(beta,x)
y = beta(1).*exp(-(x-beta(2)).^2./(2*beta(3).^2))+beta(4);