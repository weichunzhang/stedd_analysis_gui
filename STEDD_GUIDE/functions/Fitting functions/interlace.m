function a = interlace( a, x, fix )
a(~fix) = x;
end