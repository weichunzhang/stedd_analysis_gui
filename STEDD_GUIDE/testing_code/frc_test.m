clc
clear
close all

addpath('S:\AG Nienhaus\data\Weichun\Matlab_Program\Data_Analysis\STEDD_analysis_GUI\STEDD_GUIDE\Delft_FRC_matlabdistribution\FRCresolutionfunctions');
image_odd = imread('S:\AG Nienhaus\data\Weichun\Measurements\2019-01-19 stedd drb nuclei\nucleus\90C_1_54_confocal_m1_odd.tif');
image_even = imread('S:\AG Nienhaus\data\Weichun\Measurements\2019-01-19 stedd drb nuclei\nucleus\90C_1_54_confocal_m1_even.tif');

px_size = 20; % pixel size in nm

show_frc = 0; % whether or not show FRC curve
[res_value, frc_curve, res_high, res_low] = imres_ims(image_odd,image_even,px_size,show_frc);

qmax = 0.5/(px_size);
figure
hold on
plot([0 qmax],[0 0],'k') % zero line
plot(linspace(0,qmax*sqrt(2), length(frc_curve)), frc_curve,'-')
plot([0 qmax],[1/7 1/7],'m')
plot(1/(res_value),1/7,'rx'); % res_value has already taken pixel size into account
plot(1/(res_value)*[1 1],[-0.2 1/7],'r')
hold off
xlim([0,qmax]);
ylim([-0.2 1.2])
xlabel('Spatial frequency (nm^{-1})');
ylabel('FRC')
txt = sprintf('FRC resolution: %2.1f +/- %2.2f nm.\n', res_value, (res_low - res_high)/2);
text(qmax/3, 0.5, txt);