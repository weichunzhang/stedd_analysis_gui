% [out,mask] = apodImRect(in,N)
% ---------------------------------------
%
% Apodize the edges of a 2D image and a 3D image
%
% Inputs:
%  in        	Input image
%  N            Number of pixels of the apodization
%
% Outputs:
%  out        	Apodized image
%  mask         Mask used to apodize the image

function [out,mask] = apodImRect(in,N)


n=size(size(in));
s=size(in);
out=zeros(size(in));

%2D case 
if n(2)==2 
    
    Nx = min(size(in,1),size(in,2));

    x = abs(linspace(-Nx/2,Nx/2,Nx));
    map = x > Nx/2 - N;


    d = (-abs(x)- mean(-abs(x(map)))).*map;
    d = linmap(d,-pi/2,pi/2);
    d(not(map)) = pi/2;
    mask = (sin(d)+1)/2;

    
% make mask 2D
    if size(in,1) > size(in,2)
        mask = mask.*imresize(mask',[size(in,1) 1],'bilinear');
    elseif size(in,1) < size(in,2)
        mask = imresize(mask,[1 size(in,2)],'bilinear').*mask';
    else
        mask = mask.*mask';
    end

val = mean(mean(in(:)));
out = (in-val).*mask + val;


%3D case 
elseif n(2)==3
    
    Nx = min([size(in,1),size(in,2)]);

    x = abs(linspace(-Nx/2,Nx/2,Nx));
    map = x > Nx/2 - N;
    %d=(-x+mean(x(map)))??
    d = (-abs(x)- mean(-abs(x(map)))).*map;
    d = linmap(d,-pi/2,pi/2);
    d(not(map)) = pi/2;
    mask = (sin(d)+1)/2;

    % make mask 2D
    if size(in,1) > size(in,2)
        mask = mask.*imresize(mask',[size(in,1) 1],'bilinear');
    elseif size(in,1) < size(in,2)
        mask = imresize(mask,[1 size(in,2)],'bilinear').*mask';
    else
        mask = mask.*mask';
    end
    
    %make mask 3D
    %Apodization in axial plane covers 10% of slices from each side due to
    %high difference in number of pixels and number of slices
    Nz = size(in,3);
    mask_z = zeros(1,1,size(in,3));
    
    z = abs(linspace(-Nz/2,Nz/2,Nz));
    map = z > Nz/2 - 0.1*size(in,3);
    
    d_z = (-abs(z)-mean(-abs(z(map)))).*map;
    d_z = linmap(d_z,-pi/2,pi/2); 
    
    d_z(not(map)) = pi/2; 
    mask_z(1,1,:) = (sin(d_z)+1)/2;
    mask=mask.*mask_z;
  
    
    val = mean(mean(mean(in)));
    out = (in - val).*mask+val;
    
end
end

