function imt=Gaus_high_real(dim,imr,g,h)
    
if g ~=0
    
    if dim == 3

    imt = imr - imgaussfilt3(imr,g(h));

    elseif dim == 2
    
    imt = imr - imgaussfilt(imr,g(h));

    end
    
else
    
    imt = imr;
    
end 

end