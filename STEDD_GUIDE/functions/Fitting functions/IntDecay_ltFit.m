function F = IntDecay_ltFit(lambda,Xdatashort,constants,vary, IRF1, sqrtw, numExp)

%global sqrtw scale xstart;
%global IRF1 IRF2 sqrtw scale;


param=vary.*lambda + (1-vary).*constants;

%CONSTANTS :
%lambda(3)=3.7;


%Bring IRF1 into shape====================================
% param=param./scale;

%Calc. background corrected IRF1 
bIRF1 = IRF1 - param(4);

%Calc. time-shifted IRF1
IRF1_Xdata = Xdatashort + param(5);     
tsIRF1 = interp1(Xdatashort,bIRF1,IRF1_Xdata,'linear');
vnan = isnan(tsIRF1);
tsIRF1(vnan) = 0;

%Calc. normalized IRF1
nIRF1 = tsIRF1/sum(tsIRF1);

j = find(nIRF1 == max(nIRF1),1,'first');
DataT1 = Xdatashort - Xdatashort(j);

%Same procedure for IRF2=================================
%bIRF2 = IRF2 - param(11);

% %Calc. time-shifted IRF2
% IRF2_Xdata = Xdata + param(12);     
% tsIRF2 = interp1(Xdata,bIRF2,IRF2_Xdata,'linear');
% vnan = isnan(tsIRF2);
% tsIRF2(vnan) = 0;
% 
% %Calc. normalized IRF2
% nIRF2 = tsIRF2/sum(tsIRF2);
% 
% k = find(nIRF2 == max(nIRF2),1,'first');
% DataT2 = Xdata - Xdata(k);

nIRF1(nIRF1<0)=0;
% nIRF2(nIRF2<0)=0;

% 	Idecay(1,:)= (param(1)/3* exp(-DataT1/param(3))) .* (1+2/5*(2-3*param(5))*exp(-DataT1/param(4)));  %test mit nur einer Amplitude
%     Idecay(2,:)= (param(1)/3* exp(-DataT2/param(3))) .* (1-2/5*(1-3*param(6))*exp(-DataT2/param(4)));
    
    if (numExp == 1)
    Idecay = param(1)* exp(-Xdatashort/param(2));
    elseif (numExp == 2)
    Idecay = param(1)* exp(-Xdatashort/param(2)) + param(6)* exp(-Xdatashort/param(7)) + param(8)* exp(-Xdatashort/param(9));    
    end
    
    %Idecay(1,:)= (param(1)/3* exp(-DataT1/param(3))) .* (1+2/5*(2-3*param(5))*exp(-DataT1/param(4))); %code mit zwei Amplituden
%     Idecay(2,:)= (param(2)/3* exp(-DataT2/param(3))) .* (1-2/5*(1-3*param(6))*exp(-DataT2/param(4)));
    
%     Idecay(1,:)= (param(1)/3* exp(-DataT1/param(3))+param(13)/3* exp(-DataT1/param(15))) .* (1+2/5*(2-3*param(5))*exp(-DataT1/param(4)));
%     Idecay(2,:)= (param(2)/3* exp(-DataT2/param(3))+param(14)/3* exp(-DataT2/param(15))) .* (1-2/5*(1-3*param(6))*exp(-DataT2/param(4)));
    
    DataL = conv(nIRF1,Idecay);
   
%     DataL(2,:) = conv(nIRF2,Idecay(2,:));
    
    %Correct Ch1 and Ch2 for Background
	F = DataL(1:length(DataT1)) + param(3);
%     F(2,:) = DataL(2,1:length(DataT2)) + param(7);
    
%     F(1,:) = DataL(1,1:length(DataT1)) + param(7); orig
%     F(2,:) = DataL(2,1:length(DataT2)) + param(8);
    
     F=sqrtw.*F;
    
    
    



