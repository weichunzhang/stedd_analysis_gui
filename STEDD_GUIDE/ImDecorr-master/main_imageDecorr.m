function [kcMax,A0,res] = main_imageDecorr(image,pix,Nr,Ng)

    %addpath('S:\AG Nienhaus\data\Alex\matlab_program\stedd_analysis_gui\STEDD_GUIDE\ImDecorr-master');
    %addpath('.\ImDecorr-master\');
    %addpath('.\ImDecorr-master\funcs\');
    % image = double(image); % this line is unneccessary because our input is
    % double.
    %???????
    % pps = 5; % projected pixel size of 15nm

    % typical parameters for resolution estimate
    %Nr = 50;%sampling decorrelation curve (normalised to 30 in code anyway)
    %Ng = 10;%number of high-pass filter anlyzed
    % pix: pixel size in nm.
    
    % Outputs:
    % res: resolution value in the unit of nm.

    r = linspace(0,1,Nr);
    n_GPU = gpuDeviceCount; % Number of GPUs on the computer.

    %% apodize image edges with a cosine function over 20 pixels
    image = apodImRect(image,20);

    %% compute resolution

    figID = 100;
    if(n_GPU) 
        [kcMax,A0,res,f] = getDcorr(gpuArray(image), r, Ng, figID, pix);
        % gpuDevice(1); % This line doesn't look neccessary.
    else
        [kcMax,A0,res,f] = getDcorr(image, r, Ng, figID, pix);
    end

    disp(['kcMax : ',num2str(kcMax,3),', A0 : ',num2str(A0,3)]);
    
%     %% sectorial resolution
% 
%     Na = 8; % number of sectors
%     figID = 101;
%     if GPU
%         %[kcMax,A0] = getDcorrSect(gpuArray(image),r,Ng,Na,figID); gpuDevice(1);
%         [kcMax,A0] = getDcorrSect(image,r,Ng,Na,figID,pix,Fourier);
%     else
%         [kcMax,A0] = getDcorrSect(image,r,Ng,Na,figID,pix,Fourier);
%     end
% 
%     %% Local resolution map
% 
%     tileSize = 200; % in pixels
%     tileOverlap = 0; % in pixels
%     figId = 103;
% 
%     if GPU 
%         %[kcMap,A0Map] = getLocalDcorr(gpuArray(image),tileSize,tileOverlap,r,Ng,figID);gpuDevice(1);
%         [kcMap,A0Map] = getLocalDcorr(image,tileSize,tileOverlap,r,Ng,figID,pix,Fourier);
%     else
%         [kcMap,A0Map] = getLocalDcorr(image,tileSize,tileOverlap,r,Ng,figID,pix,Fourier);
%     end
end