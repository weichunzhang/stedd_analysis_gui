% [im,path] = loadData(path)
% ---------------------------------------
%
% Generic function to load arbitrary image stacks in the workspace
%
% Inputs:
%  path        	Full path of the file, if not provided the function will call uigetfile
%
% Outputs:
%  im        	Loaded image
%  path 		Image full path


function [im,path] = loadData(path)
if nargin < 1
    [fname,pname] = uigetfile('*.*');
    path = [pname,filesep,fname];
end
    
info = imfinfo(path);
w = info(1).Width; h = info(1).Height;
bd = info(1).BitDepth/8;
nf = floor(info(1).FileSize/(bd*w*h));
keepReading = 1; k = 1;
im = zeros(h,w,nf);
warning('off')
while keepReading 
    try
        im(:,:,k) = imread(path,k);
        k = k+1;
    catch
        keepReading = 0;
        disp('Finished reading... ')
        disp(['Stack size : ',num2str(size(im))])
    end
end
warning('on')