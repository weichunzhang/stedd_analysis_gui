% r = getRadAvg(im)
% ---------------------------------------
%
% Return the radial average of input square image im
%
% Inputs:
%  im        	Square image
%
% Outputs:
%  r        	Radial average


function [r,pol,im] = getRadAvg(im)

%if size(im,1) ~= size(im,2)
if length(size(im))==3
    
    if max(size(im))<400
        im = imresize3(im,[max(size(im)) max(size(im)) max(size(im))],'cubic');
    
    else
        im = imresize3(im,[400 400 400],'cubic');
    
    end
    
    pol=im;
    %pol=im2pol(im); 
    r = mean(pol,1);
    r = mean(r,3);
    r = squeeze(r);
    r = imresize(r,[1 ceil(size(im,3)/2)],'cubic');
    
elseif length(size(im))==2
    im = imresize(im,[max(size(im)) max(size(im))]);
    pol=im2pol(im);
    r = mean(im2pol(im),1);
    r = imresize(r,[1 ceil(size(im,2)/2)],'bilinear');
    
end 
    %elseif length(size(im)) ~= 2
%    error('getRadAvg supports only 2D matrix as input');
%end

