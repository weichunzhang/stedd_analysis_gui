function F = IntDecayMultiExpBound(X, tdata, IRF, weights)

%parameters
% X(1) IRF background
% X(2) IRF time shift
% X(3) intensity background
%-----------
% X(4) total amplitude
% X(5) f1
% X(6) tau1
%-----------
% X(7) A2
% X(8) f2
% X(9) tau2
%-----------
% X(10) A3
% X(11) f3
% X(12) tau3

%Calc. background corrected IRF1 
bIRF = IRF - X(1);

%Calc. time-shifted IRF1
IRF_tdata = tdata + X(2);     
tsIRF = interp1(tdata,bIRF,IRF_tdata,'linear');
vnan = isnan(tsIRF);
tsIRF(vnan) = 0;

%Calc. normalized IRF1
nIRF = tsIRF/sum(tsIRF);
nIRF(nIRF<0)=0;

% select parameters to fit
A = X(4);
tau1 = X(5);
f2 = X(6);
tau2 = X(7);
f3 = X(8)*f2;
tau3 = X(9);

f1 = min(1,max(0, 1 - f2 - f3));

f_s = f1+f2+f3;
f1 = f1/f_s;
f2 = f2/f_s;
f3 = f3/f_s;

%disp([f1, f2, f3, f1+f2+f3]);
% intensity decay function
Idecay = A * (f1* exp(-tdata/tau1) + f2*exp(-tdata/tau2) + f3*exp(-tdata/tau3));    

% output data
ydata = conv(nIRF,Idecay) + X(3);
F = weights.*(ydata(1:length(tdata)));
   
  
    



