function [d,count] = decor_curve_new(I,Ir,count,dim,Z,Y,X,r,dc,c,d)

for k = length(r):-1:1
    rt = r(k);
    
    mask = Mask(rt,dim,X,Y,Z);
    
    temp = mask.*I;
    temp = temp(1:(end-1)/2); % remove the mean
    
    cc = getCorrcoef(Ir,temp,c);
    if isnan(cc); cc = 0; end
    d(k,dc) = gather(cc); % gather if input image is gpuArray 
    count = count +1;
       
end

end