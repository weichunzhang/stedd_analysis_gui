function [X,Y,Z] = mesh(dim,s)

    if dim == 3
        
    [X,Y,Z] = meshgrid(linspace(-1,1,s(2)),linspace(-1,1,s(1)),linspace(-1,1,s(3)));

    elseif dim == 2
        
    [X,Y] = meshgrid(linspace(-1,1,s(2)),linspace(-1,1,s(1)));
    Z = 1;
    
    end

end 