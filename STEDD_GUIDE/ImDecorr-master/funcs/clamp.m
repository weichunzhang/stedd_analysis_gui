% val = clamp(val,minval,maxval)
% ---------------------------------------
%
% Clamp an nd-array between minval and maxval
%
% Inputs:
%  val        	Input matrix
%  minval		Any value of val smaller than minval will be made equal to minval
%  maxval       Any value of val larger than maxval will be made equal to maxval
%
% Outputs:
%  vak        	Clamped value

function val = clamp(val,minval,maxval)

if isempty(minval) % no lower bound
    map = val > maxval;
	val(map) = maxval;
end

if isempty(maxval) % no upper bound
	map = val < minval;
	val(map) = minval;
end

if ~isempty(minval) && ~isempty(maxval)
    map = val > maxval;
    val(map) = maxval;
    map = val < minval;
    val(map) = minval;
end