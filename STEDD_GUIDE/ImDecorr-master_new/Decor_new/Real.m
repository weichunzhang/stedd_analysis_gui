function I=Real(I)

I=real(ifftshift(ifftn(ifftshift(I))));

end