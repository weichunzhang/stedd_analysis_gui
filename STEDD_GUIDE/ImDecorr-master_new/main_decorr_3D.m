%% set path and load some data

%typical parameters for resolution estimate

function main_decorr_3D(image,pix,func,n,e) 
%% apodize image edges with a cosine function over 20 pixels
image = apodImRect(image,20);

if length(size(image)) == 3
image_2D_mean = mean(image,3);
imwrite(image_2D_mean,hot,'S:\AG Nienhaus\data\Alex\Decor_3D\sample_mean.tiff','tiff')
image_2D = image(:,:,30);
imwrite(image_2D,hot,'S:\AG Nienhaus\data\Alex\Decor_3D\sample_2D.tiff','tiff')
end

%% compute resolution
    [d,im,r] = getDcorr_3D_new(image,pix,func,e,n);
    if length(size(image))==3
    [d,im,r] = getDcorr_3D_new(image_2D_mean,pix,func,e,n);
    [d,im,r] = getDcorr_3D_new(image_2D,pix,func,e,n);
    end
    
end
    


%disp(['kcMax : ',num2str(kcMax,3),', A0 : ',num2str(A0,3)])
%% sectorial resolution

%iNa = 8; % number of sectors
%figID = 101;
%if GPU
    %[kcMax,A0] = getDcorrSect(gpuArray(image),r,Ng,Na,figID); gpuDevice(1);
%    [kcMax,A0] = getDcorrSect(image,r,Ng,Na,figID,pix);
%else
%    [kcMax,A0] = getDcorrSect(image,r,Ng,Na,figID,pix);
%end

%% Local resolution map

%tileSize = 200; % in pixels
%tileOverlap = 0; % in pixels
%figId = 103;

%if GPU 
    %[kcMap,A0Map] = getLocalDcorr(gpuArray(image),tileSize,tileOverlap,r,Ng,figID);gpuDevice(1);
%    [kcMap,A0Map] = getLocalDcorr(image,tileSize,tileOverlap,r,Ng,figID,pix,Fourier);
%else
%    [kcMap,A0Map] = getLocalDcorr(image,tileSize,tileOverlap,r,Ng,figID,pix,Fourier);
%end
