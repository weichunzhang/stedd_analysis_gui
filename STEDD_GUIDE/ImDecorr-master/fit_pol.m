function f = fit_pol(d0,d,r0,r,kc)

x = linspace(1, max(size(d(1,:))),max(size(d(1,:))));

f = polyfit(r0,d(:,5),3);

func = 'a*x^3+b*x^2+c*x+d';

%pol = fit(r0',d(:,6),func,'StartPoint',[ f(1) f(2) f(3) f(4)]);
pol = fit(r0',d(:,5),func,'StartPoint',[ 1 1 1 1]);

figure(5);

plot(pol);

hold on

%figure(6);

plot(r0,d(:,5));

end 