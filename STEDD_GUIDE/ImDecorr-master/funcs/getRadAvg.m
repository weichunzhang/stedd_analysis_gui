% r = getRadAvg(im)
% ---------------------------------------
%
% Return the radial average of input square image im
%
% Inputs:
%  im        	Square image
%
% Outputs:
%  r        	Radial average


function [r,im,pol] = getRadAvg(im)

if size(im,1) ~= size(im,2)
    im = imresize(im,[max(size(im)) max(size(im))],'bilinear');
elseif length(size(im)) ~= 2
    error('getRadAvg supports only 2D matrix as input');
end

pol=im2pol(im);
r = mean(im2pol(im),1);
r = imresize(r,[1 ceil(size(im,2)/2)],'bilinear');