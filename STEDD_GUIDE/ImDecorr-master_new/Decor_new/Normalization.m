function I = Normalization(I) 

I = I./abs(I); I(isinf(I)) = 0; I(isnan(I)) = 0; 

end 