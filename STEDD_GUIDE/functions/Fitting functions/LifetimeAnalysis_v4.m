%fname = '500nM_R6G_40pro glycerol_ch1.dat';
%Needed arrays: IRF1, IRF2 ch1, ch2,
%function [F, G]=LifetimeAnalysis_v4(x,IRF1,IRF2,lambda_0)


%data = textread(fname,'%s','whitespace','\n');
%ch1 x(1:2344,2,1)
%ch2 x(1:2344,3,1)


global sqrtw scale xstart;
%global IRF1 IRF2 sqrtw scale
xstart=1;
G=1.08; %Korrekturfaktor unterschiedliche Detektionseffizienzen Wert fuer Chan 3+4 mit mitgeliefertem Pol Cube

% IRF1=IRF1(1:3126);
% IRF2=IRF2(1:3126); %3126
IRF1=IRFChan320MHz(1:3126);
IRF2=IRFChan420MHz(1:3126); %3126

x(:,1)=Alexa546Aniso10min5LBChan30x2DCopy(:,1);
x(:,2)=Alexa546Aniso10min5LBChan30x2DCopy(:,2);
x(:,3)=Alexa546Aniso10min5LBChan40x2DCopy(:,2);
%%Calculate G-Faktor
G=sum(x(1000:1200,2))/sum(x(1000:1200,3));
x(:,3)=G*x(:,3);

% x(:,1)=SNAP0x2DAlexa546Aniso10min6LBChan30x2DCopy(:,1);
% x(:,2)=SNAP0x2DAlexa546Aniso10min6LBChan30x2DCopy(:,2);
% x(:,3)=SNAP0x2DAlexa546Aniso10min6LBChan40x2DCopy(:,2);

t=x(1:3126,1)';
y=[x(1:3126,2)' ; x(1:3126,3)'];

options = optimset('MaxIter',1e3, 'MaxFunEval',1e3,'Display','off','TolX',1e-5,'TolFun',1e-5);


            
% (D1 , D2 , tau_F , tau_R , k1 , k2 , bckgrd1 , bckgrd2 , bckgrdIRF1, Timeshift IRF1, bckgrdIRF2 ,Timeshift IRF2) 
 	%lambda_0=[1E4,8E3, 3.7 , 1 , 0.667 , 0.333 , 25, 30 , 5 , 0.037 , 10 , 0.037, 150000, 150000, 4];
%     lambda_0=[4.7E4,4.5E4, 3.78 , 0.71 , 0.6667 , 0.3333 , 32.3, 44.5 , 2.5 , 0.037 , 2.5 , 0.037];
% 	lb= [0, 0, 0, 0, 0, 0, 0 ,0 ,0 , -1 ,0 , -1];
%     ub= [1E7 , 1E7, 10, 50, 5 , 5, 500 , 500 , 2000 , 1 , 2000 , 1];
%     %ub= [1E6 , 1E6, 5, 50, 10 , 10, 500 , 500 , 5 , 1 , 10 , 1];
%     vary=[1,1,1,1,0,0,1,1,1,1,1,1];
    
    
    %%%%Code mit doppelt exponentiellem Fluoreszenzzerfall
    %(D1 , D2 , tau_F , tau_R , k1 , k2 , bckgrd1 , bckgrd2 , bckgrdIRF1, Timeshift IRF1, bckgrdIRF2 ,Timeshift IRF2, D3, D4, tau_F2) 
     lambda_0=[4.7E4,4.5E4, 3.59 , 0.71 , 0.6667 , 0.3333 , 32.3, 44.5 , 2.5 , 0.037 , 2.5 , 0.037, 4.7E4, 4.7E4, 1.44 ];
	lb= [0, 0, 0, 0, 0, 0, 0 ,0 ,0 , -1 ,0 , -1, 0, 0, 0];
    ub= [1E7 , 1E7, 10, 50, 5 , 5, 500 , 500 , 2000 , 1 , 2000 , 1, 1E7, 1E7, 3];
    %ub= [1E6 , 1E6, 5, 50, 10 , 10, 500 , 500 , 5 , 1 , 10 , 1];
    vary=[1,1,1,1,1,1,0,0,0,1,0,1,1,1,1];
    %%%%
    
    lambda_0(1)=max(x(:,2));  %Startwerte Amplituden aus Daten absch�tzen
    lambda_0(2)=max(x(:,3));
    
    lambda_0(7)=mean(x(2900:3100,2));        %Startwerte Hintergrund aus Daten absch�tzen
    lambda_0(8)=mean(x(2900:3100,3));
    
    lambda_0(9)=mean(IRF1(1200:2500));        %Startwerte Hintergrund aus Daten absch�tzen
    lambda_0(11)=mean(IRF2(1200:2500));
    
    
    sqrtw=sqrt(1./max(y,1));
  lambda=lambda_0;  
  constants=lambda;  
  % scale=100./lambda;
% lambda=lambda_0.*scale;

exitflag=0;
while exitflag == 0
     gaopeng=100
     [lambda,resnorm,residuals,exitflag] = lsqcurvefit(@IntDecay_v4,lambda,t(xstart:end),sqrtw(:,xstart:end).*y(:,xstart:end),lb,ub,options,constants,vary, IRF1, IRF2, t);
end


fit=IntDecay_v4(lambda,t,constants,vary, IRF1, IRF2, t)./sqrtw(:,xstart:end);
% y(1,:)=y(1,:)-lambda(7);
% y(2,:)=y(2,:)-lambda(8);
% fit(1,:)=fit(1,:)-lambda(7);
% fit(2,:)=fit(2,:)-lambda(8);

subplot(2,1,1);
semilogy(t,y,t(xstart:end),fit);
legend('ch1','ch2','fit1','fit2');
subplot(2,1,2);
plot(t(xstart:end),residuals(1,:),'r',t(xstart:end),residuals(2,:),'c');
ylim([-7 7]);
% lambda=lambda./scale;
%disp(lambda);
F=lambda;
G=[fit(1,:);fit(2,:);t(xstart:end)];

 tau_F1=lambda(3)
 tau_R=lambda(4)
 k1=lambda(5)
 k2=lambda(6)
    
%%Save to File
saveVar(:,1)=t';
saveVar(:,2)=y(1,:)'; %chan1 data
saveVar(:,3)=fit(1,:)'; 
saveVar(:,4)=residuals(1,:)';
saveVar(:,5)=y(2,:)';
saveVar(:,6)=fit(2,:)';
saveVar(:,7)=residuals(2,:)';


saveFilename = 'AnisoFit.dat';
delete(saveFilename);
fid1 = fopen(saveFilename,'w');
fprintf(fid1,'%s \t %f \r\n %s \t %f \r\n %s \t %f \r\n %s \t %f \r\n','%tau_F',lambda(3),'%tau_R',lambda(4),'%k1',lambda(5),'%k2',lambda(6));
fprintf(fid1,'%s \t %s \t %s \t %s \t %s \t %s \t %s \r\n','%x','IntChan1','FitChan1','ResidualsChan1','IntChan2','FitChan2','ResidualsChan2');
fclose(fid1);

dlmwrite(saveFilename, saveVar, 'delimiter', '\t','-append')




%end
