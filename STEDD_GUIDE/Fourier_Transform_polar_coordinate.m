

%Function: calculate the spectrum in r-theta system
%'Inputs', 'I_Image',input image;
%'Outputs': 'Radus_coor,Thita_coor,Fp', radius, azimuth and spectrum of spectrum output;

function [Radius_coor,Theta_coor,Fp]=Fourier_Transform_polar_coordinate(I_Image)
        
        Freq=fftshift(fft2(fftshift(I_Image))); %Spectrum; 
        % fftshift shifts zero-frequency component to center of spectrum
        % fft2: 2D fast Fourier transform.
        
        [y_length,x_length] = size(Freq); 
        x=linspace(-0.5,0.5,x_length); %x coordinate; 
        y=linspace(-0.5,0.5,y_length); %y coordinate; 
        [xx,yy] = meshgrid(x,y); %2D coordinate; 
        
        %Convert to polar coordinates: 
        % figure(100); imagesc(x,y,abs(Freq)); title('Fourier transform x radius'); 
        
        Radius_coor = linspace(0,sqrt(max(x)^2 + max(y)^2),y_length); %Radius;
        % Radius_coor = linspace(0,min([max(x) max(y)]),y_length); %Radius;
        Radius_coor = Radius_coor/max(Radius_coor);
        Theta_coor = linspace(0,2*pi,x_length); %Angle;
        [Radius_coor2,Theta_coor2] = meshgrid(Radius_coor,Theta_coor); %2D coordinates;
        
        [x_polar,y_polar] = pol2cart(Theta_coor2,Radius_coor2); 
        Fp = interp2(xx,yy,abs(Freq),x_polar,y_polar); 
%         Norm_factor=mean(mean(Fp(:,1:100)))/0.13;
%         Fp=Fp/Norm_factor; 
%         figure(200); imagesc(Radius_coor,Theta_coor,Fp); title('Spectrum');
%         % caxis([0,1]); 
%         xlabel('Spatial frequency (r/rmax)'); ylabel('Spatial frequency Theta'); axis([0 0.35 0 2*pi]); colorbar; colormap('hot');
%         
end
