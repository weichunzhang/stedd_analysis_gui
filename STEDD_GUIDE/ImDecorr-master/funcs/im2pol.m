% imP = im2pol(imC)
% ---------------------------------------
%
% Transform an image from carthesian to polar coordinate
%
% Inputs:
%  imC        	Input image in carthesian coordinate
%
% Outputs:
%  imP        	Output image in polar coordinate

function imP = im2pol(imC)

rMin=0; 
rMax=1;

[Ny,Nx] = size(imC); 
xc = (Ny+1)/2; yc = (Nx+1)/2; 
sx = (Ny-1)/2; sy = (Nx-1)/2;

Nr = 2*Ny; Nth = 2*Nx;

dr = (rMax - rMin)/(Nr-1); 
dth = 2*pi/Nth;


r = rMin:dr:rMin+(Nr-1)*dr; 
th = (0:dth:(Nth-1)*dth)'; 
[r,th]=meshgrid(r,th); 

x = r.*cos(th); y = r.*sin(th); 
xR = x*sx + xc; yR = y*sy + yc;

imP = interp2(imC, xR, yR,'cubic'); 

imP(isnan(imP)) = 0;

end