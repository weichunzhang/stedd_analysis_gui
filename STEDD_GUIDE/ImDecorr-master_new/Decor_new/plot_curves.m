function plot_curves(figID,r0,d,Ng,res,kcMax,A0,kc,snr,r)

if figID
    lnwd = 1.5;
    figure(figID);
    %plot(r0,d(:,1:Ng),'color',[0.2 0.2 0.2 0.5]);
    plot(r0,d(:,2:Ng+1),'color',[0.2 0.2 0.2 0.5]);
    hold on
    plot(r,d(:,Ng+2:end),'color',[0.2 0.2 0.2 0.5]);
    hold on
    
    %radAv(1) = radAv(2); %for plot 
    %radAv(end) = radAv(end-1);
    %plot(linspace(0,1,length(radAv)),linmap(radAv,0,1),'linewidth',lnwd,'color',[1 0 1])
    %for n = 1:Ng
    %    plot(r2,d(:,n+Ng:end),'color',[0 0 (n-1)/Ng])
    %end
    plot(r0,d(:,1),'linewidth',lnwd,'color','g')
    plot([kcMax kcMax],[0 1],'k')
    %for k = 1:length(kc)
    for k = 1:length(kc)
        if kc(k)~=0
        plot(kc(k),snr(k),'bx','linewidth',1)
        end 
    end
    hold off
    title(['Dcor analysis : res ~ ',num2str(res,4),'nm',', kcMax ~ ',num2str(kcMax,4),', SNR ~ ',num2str(A0,4)])
    xlim([0 1]); ylim([0 1])
    xlabel('Normalized spatial frequencies')
    ylabel('C.c. coefficients')
end
end