%FRCRESOLUTION   Compute the resolution from 2 images
%
% SYNOPSIS:
% Old: [resolution frc_out resolution_high resolution_low] = frcresolution(in1,in2)
% New: [resolution frc_out resolution_high resolution_low frc_smooth] = frcresolution(in1,in2)
%
% NOTES:
%   Non-square images are zero padded to make them square.

% (C) Copyright 2012               Quantitative Imaging Group
%     All rights reserved          Faculty of Applied Physics
%                                  Delft University of Technology
%                                  Lorentzweg 1
%                                  2628 CJ Delft
%                                  The Netherlands
% Robert Nieuwenhuizen, Oct 2012

function varargout = frcresolution(varargin)

d = struct('menu','FRC resolution',...
           'display','Resolution from images',...
           'inparams',struct('name',       {'in1',      'in2',},...
                             'description',{'Image 1',  'Image 2',},...
                             'type',       {'image',    'image',},...
                             'dim_check',  {2,          2,},...
                             'range_check',{[],         [],},...
                             'required',   {1,          1,},...
                             'default',    {[],         [],}...
                              ),...
           'outparams',struct('name',{'resolution','frc_out','resolution_high','resolution_low','frc_smooth'},...
                              'description',{'Resolution','FRC curve','Upper bound','Lower bound','Smoothed FRC curve'},...
                              'type',{'array','array','array','array','array'}...
                              )...
           );       
       
if nargin == 1
   s = varargin{1};
   if ischar(s) & strcmp(s,'DIP_GetParamList')
      varargout{1} = struct('menu','none');
      return
   end
end

try
   [in1, in2] = getparams(d,varargin{:});
catch
   if ~isempty(paramerror)
      error(paramerror)
   else
      error('Parameter parsing was unsuccessful.')
   end
end

% Calculate FRC curve
frc_out = frc_old(in1,in2);
% It used to be using frc, changed by W.Z on Mar 2019

% Calculate the resolution
sz = max(imsize(in1)); % imsize is a function in DIPimage. sz the image size (length or width) in pixels.

if (~sum(in1)) || (~sum(in2))
    varargout{1} = NaN;
    varargout{2} = radialsum(0*in1); % frc_out
    varargout{3} = NaN;
    varargout{4} = NaN;
    varargout{5} = radialsum(0*in1); % frc_smooth
    return;
end

switch nargout
    case 1
        resolution = frctoresolution(frc_out,sz);
        varargout{1} = resolution;
        return
    case 2
        resolution = frctoresolution(frc_out,sz);
        varargout{1} = resolution;
        varargout{2} = frc_out;
        return        
    case 3
        [resolution resolution_high] = frctoresolution(frc_out,sz);
        varargout{1} = resolution;
        varargout{2} = frc_out;
        varargout{3} = resolution_high;
        return
    case 4
        [resolution resolution_high resolution_low] = frctoresolution(frc_out,sz);
        varargout{1} = resolution;
        varargout{2} = frc_out; % frc_out is from frc(in1,in2)? W.Z.
        varargout{3} = resolution_high;
        varargout{4} = resolution_low;
        return
    case 5
        [resolution resolution_high resolution_low frc_smooth] = frctoresolution(frc_out,sz);
        varargout{1} = resolution;
        varargout{2} = frc_out; % frc_out is from frc(in1,in2)? W.Z.
        varargout{3} = resolution_high;
        varargout{4} = resolution_low;
        varargout{5} = frc_smooth;
        return
end

% switch nargout
%     case 1
%         resolution = frctoresolution_unsmoothed(frc_out,sz);
%         varargout{1} = resolution;
%         return
%     case 2
%         resolution = frctoresolution_unsmoothed(frc_out,sz);
%         varargout{1} = resolution;
%         varargout{2} = frc_out;
%         return        
%     case 3
%         [resolution resolution_high] = frctoresolution_unsmoothed(frc_out,sz);
%         varargout{1} = resolution;
%         varargout{2} = frc_out;
%         varargout{3} = resolution_high;
%         return
%     case 4
%         [resolution resolution_high resolution_low] = frctoresolution_unsmoothed(frc_out,sz);
%         varargout{1} = resolution;
%         varargout{2} = frc_out; % frc_out is from frc(in1,in2)? W.Z.
%         varargout{3} = resolution_high;
%         varargout{4} = resolution_low;
%         return
% end
