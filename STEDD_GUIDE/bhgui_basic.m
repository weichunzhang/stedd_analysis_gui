function varargout = bhgui(varargin)
% BHGUI MATLAB code for bhgui.fig
%      BHGUI, by itself, creates a new BHGUI or raises the existing
%      singleton*.
%
%      H = BHGUI returns the handle to a new BHGUI or the handle to
%      the existing singleton*.
%
%      BHGUI('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in BHGUI.M with the given input arguments.
%
%      BHGUI('Property','Value',...) creates a new BHGUI or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before bhgui_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to bhgui_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help bhgui

% Last Modified by GUIDE v2.5 18-Oct-2019 15:48:25

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @bhgui_OpeningFcn, ...
                   'gui_OutputFcn',  @bhgui_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
               
global bh_data;
global bh_info;
global bh_photons;


%add bio-formats path 
bf_path = 's:\AG Nienhaus\_exchange\internal\Software\ProgrammScripts\MatlabScripts\BH_spc_dataread\bfmatlab\';
if (exist(bf_path,'dir') == 7)
    addpath(bf_path);
end


if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end

% End initialization code - DO NOT EDIT


% --- Executes just before bhgui is made visible.
function bhgui_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to bhgui (see VARARGIN)

% Choose default command line output for bhgui
handles.output = hObject;

set(handles.StatusMsgStr,'String','Ready'); 
set(handles.DataFolderStr,'String',pwd);
set(handles.ImgRowTrig,'Value',1);
UpdateFileList(handles);

% Update handles structure
guidata(hObject, handles);



% UIWAIT makes bhgui wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = bhgui_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in DataPathBtn.
function DataPathBtn_Callback(hObject, eventdata, handles)
% hObject    handle to DataPathBtn (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

%rdir = pwd;
FolderName = get(handles.DataFolderStr,'String');

if (exist(FolderName,'dir') ~= 7)
    FolderName = pwd;
end

FolderName = uigetdir(FolderName);
if (FolderName ~= 0)
    set(handles.DataFolderStr,'String',FolderName);
end

UpdateFileList(handles);
%cd(rdir);




function DataFolderStr_Callback(hObject, eventdata, handles)
% hObject    handle to DataFolderStr (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of DataFolderStr as text
%        str2double(get(hObject,'String')) returns contents of DataFolderStr as a double


% --- Executes during object creation, after setting all properties.
function DataFolderStr_CreateFcn(hObject, eventdata, handles)
% hObject    handle to DataFolderStr (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function UpdateFileList(handles)

global bh_info;

DataFiles = {};
FolderName = get(handles.DataFolderStr,'String');

FileExtList = get(handles.DataFilesExtList,'String');
FileExtValue = get(handles.DataFilesExtList,'Value');

filelist = dir(char(fullfile(FolderName,FileExtList(FileExtValue))));

if (length(filelist) > 0)
    bh_info = bhcorefunc('init_info',length(filelist));

    for f_idx = 1:length(filelist)
        bh_info(f_idx).filename = fullfile(FolderName,filelist(f_idx).name);
        DataFiles{f_idx} = filelist(f_idx).name;

        bh_info(f_idx).timebase = 25;
        bh_info(f_idx).timebins = pow2(12);
    end

    % if (~isempty(filelist))
    %     
    %     DataFiles = {};
    % end

    % if (get(handles.DataFilesSubDirChk,'Value'))
    %     folderlist = dir(FolderName);
    %     if (~isempty(folderlist.folder))
    %         
    %     end
    % end

    UpdateDataPlot(handles, bh_info(1));
end
set(handles.DataFilesList,'String',DataFiles);




% --- Executes on button press in LoadDataBtn.
function LoadDataBtn_Callback(hObject, eventdata, handles)
% hObject    handle to LoadDataBtn (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global bh_data;
global bh_info;

t0 = tic;
%folder = get(handles.DataFolderStr,'String');
filelist = get(handles.DataFilesList,'String');
fileidx = get(handles.DataFilesList,'Value');


data_file = bh_info(fileidx).filename;%fullfile(folder,char(filelist(fileidx)));


if (exist(data_file,'file') == 2)

    msgstr = sprintf('Read data from %s ...',char(filelist(fileidx))); 
    set(handles.StatusMsgStr,'String',msgstr); 
    drawnow();

    tic;
    bh_data = bhcorefunc('bhreaddata',data_file,'uint32');
    t1 = toc;
    
    tic;
    bh_info(fileidx) = bhcorefunc('bhgetinfo',bh_data, bh_info(fileidx));
       
    bh_info(fileidx).ltData = bhcorefunc('bhgetlthist',bh_info(fileidx));
    
%     bh_info(fileidx).ltData = zeros(bh_info(fileidx).timebins,2);
%     bh_info(fileidx).ltData(:,1) = (0:bh_info(fileidx).timebins-1)*bh_info(fileidx).timebase/bh_info(fileidx).timebins;
% 
%     if (~isempty(bh_info(fileidx).tADC))
%         bh_info(fileidx).ltData(:,2) = histc(bh_info(fileidx).tADC(bh_info(fileidx).phIdx),0:bh_info(fileidx).timebins-1);
%     end
%     
    [i_max, t_max] = max(bh_info(fileidx).ltData(:,2));
    t_left = find(bh_info(fileidx).ltData(1:t_max,2) < 0.25*mean(bh_info(fileidx).ltData(:,2)), 1, 'last');
    t_right = t_max + find(bh_info(fileidx).ltData(t_max:end,2) < 0.25*mean(bh_info(fileidx).ltData(:,2)), 1, 'first');
    
    bh_info(fileidx).ltPeriod(1) = t_left*bh_info(fileidx).timebase/bh_info(fileidx).timebins;
    bh_info(fileidx).ltPeriod(2) = t_right*bh_info(fileidx).timebase/bh_info(fileidx).timebins;



    UpdateDataPlot(handles, bh_info(fileidx));
    t2 = toc;

    msgstr = sprintf('Finished reading in %5.1f s; get data info in %5.1f s',t1, t2); 
    set(handles.StatusMsgStr,'String',msgstr); 
  
end

t0 = toc(t0);
msgstr = get(handles.StatusMsgStr,'String'); 
msgstr = sprintf('%s, total time: %5.2f s',msgstr, t0);
set(handles.StatusMsgStr,'String',msgstr); 



%----------------------
function UpdateDataPlot(handles, info)


set(handles.DataNPh,'String',num2str(length(info.phIdx)));
set(handles.DataNPhV,'String',num2str(length(info.phIdxV)));
set(handles.DataNPx,'String',num2str(length(info.pxIdx)));
set(handles.DataNLn,'String',num2str(length(info.lnIdx)));
set(handles.DataNFr,'String',num2str(length(info.frIdx)));

set(handles.DataTBase,'String',num2str(info.timebase,'%8.4f'));
% set(handles.ImgThigh,'String',num2str(info.timebase,'%8.4f'));

set(handles.ImgTlow,'String',num2str(info.ltPeriod(1),'%6.2f'));
set(handles.ImgThigh,'String',num2str(info.ltPeriod(2),'%6.2f'));

if (~isempty(info.ltData))
    subplot(handles.tHistAxis);
    plot(info.ltData(:,1),info.ltData(:,2));
    xlim([info.ltData(1,1) info.ltData(end,1)]);

    line([info.ltPeriod(1) info.ltPeriod(1)],[0 max(info.ltData(:,2))], 'Color','red','LineStyle','--')
    line([info.ltPeriod(2) info.ltPeriod(2)],[0 max(info.ltData(:,2))], 'Color','red','LineStyle','--')
end

subplot(handles.imgAxis); cla;
if (~isempty(info.img))
    %imagesc(info.img);
    DisplayImage(handles);
end


% --- Executes on selection change in DataFilesList.
function DataFilesList_Callback(hObject, eventdata, handles)
% hObject    handle to DataFilesList (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global bh_info;

idx = get(handles.DataFilesList,'Value');
if (~isempty(idx))
    % UpdateDataInfo(handles, bh_info(idx));
    UpdateDataPlot(handles, bh_info(idx)); % Modified by WZ.
end


% Hints: contents = cellstr(get(hObject,'String')) returns DataFilesList contents as cell array
%        contents{get(hObject,'Value')} returns selected item from DataFilesList


% --- Executes during object creation, after setting all properties.
function DataFilesList_CreateFcn(hObject, eventdata, handles)
% hObject    handle to DataFilesList (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in DataFilesSubDirChk.
function DataFilesSubDirChk_Callback(hObject, eventdata, handles)
% hObject    handle to DataFilesSubDirChk (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of DataFilesSubDirChk


% --- Executes on selection change in DataFilesExtList.
function DataFilesExtList_Callback(hObject, eventdata, handles)
% hObject    handle to DataFilesExtList (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns DataFilesExtList contents as cell array
%        contents{get(hObject,'Value')} returns selected item from DataFilesExtList


% --- Executes during object creation, after setting all properties.
function DataFilesExtList_CreateFcn(hObject, eventdata, handles)
% hObject    handle to DataFilesExtList (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function DataTBase_Callback(hObject, eventdata, handles)
% hObject    handle to DataTBase (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of DataTBase as text
%        str2double(get(hObject,'String')) returns contents of DataTBase as a double

global bh_info;

idx = get(handles.DataFilesList,'Value');
bh_info(idx).timebase = str2double(get(handles.DataTBase,'String'));


% --- Executes during object creation, after setting all properties.
function DataTBase_CreateFcn(hObject, eventdata, handles)
% hObject    handle to DataTBase (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pushbutton3.
function pushbutton3_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in imgMTBtn.
function imgMTBtn_Callback(hObject, eventdata, handles)
% hObject    handle to imgMTBtn (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global bh_data;
global bh_info;

idx = get(handles.DataFilesList,'Value');
bImgSplit = get(handles.ImgSplitChk,'Value');
bImgSplitLn = str2double(get(handles.ImgSplitLines,'String'));

if (~isempty(idx))
    % micro time interval
    %t1sel = [0 bh_info(idx).timebins-1];
   
    t1sel(1) = str2double(get(handles.ImgTlow,'String'))/bh_info(idx).timebase;
    t1sel(2) = str2double(get(handles.ImgThigh,'String'))/bh_info(idx).timebase;

    t1sel(1) = max(t1sel(1),0);
    t1sel(2) = min(t1sel(2),bh_info(idx).timebins-1);

    bh_info(idx).ltPeriod = t1sel*bh_info(idx).timebase;

    %UpdateDataPlot(handles, bh_info(idx));

    % number of pixels per line
    bh_info(idx).gridx = str2double(get(handles.ImgGridX,'String'));

    if (get(handles.ImgRowTrig,'Value') == 1) %line
        bh_info(idx).rowIdx = bh_info(idx).lnIdx;
    elseif (get(handles.ImgRowTrig,'Value') == 2) %frame
        bh_info(idx).rowIdx = bh_info(idx).frIdx;
    else
        bh_info(idx).rowIdx = [];
    end
    
    set(handles.StatusMsgStr,'String','Calc MT values and show image...'); 
    drawnow();
    
    tic
    
    if (~isempty(bh_info(idx).rowIdx)) %assign data events to lines
        
        bh_info(idx).img = uint16(bhcorefunc('bhgetimage',bh_data, bh_info(idx), round(t1sel*bh_info(idx).timebins), 1));
       
        if (bImgSplit)
            bh_info(idx).gridy = bImgSplitLn;
            bh_info(idx).gridz = floor(size(bh_info(idx).img,1)/bImgSplitLn);
            
            
            bh_info(idx).img = permute(...
                reshape(bh_info(idx).img(1:bh_info(idx).gridy*bh_info(idx).gridz, 1:bh_info(idx).gridx),...
                [bh_info(idx).gridy, bh_info(idx).gridz, bh_info(idx).gridx]),...
                [3 1 2]);
            set(handles.ImgSplitFrame,'string','1');
            set(handles.ImgSplitGridZ,'string',sprintf('of %d', bh_info(idx).gridz));
            
        else
            set(handles.ImgSplitFrame,'string','0');
            set(handles.ImgSplitGridZ,'string','of 0');
            bh_info(idx).gridz = 0;
        end
                
        
        DisplayImage(handles);
    else
        
    end
    if (~isempty(bh_info(idx).frIdx)) %assign photons to frames
        
    end
    
    
    t = toc;
    msgstr = sprintf('Get image in %5.1f s',t); 
    set(handles.StatusMsgStr,'String',msgstr); 
    
    
    
end




function DisplayImage(handles)
    global bh_info;
    
    idx = get(handles.DataFilesList,'Value');

    fr = str2double(get(handles.ImgSplitFrame,'string'));
    fr = min(max(logical(bh_info(idx).gridz), fr), bh_info(idx).gridz);
    set(handles.ImgSplitFrame,'string',num2str(fr,'%d'))
    
    
    if (fr > 0 && fr < bh_info(idx).gridz)
        imgframe = bh_info(idx).img(:,:,fr)';
    else
        imgframe = bh_info(idx).img;
    end
    
    
    subplot(handles.imgAxis);
    imagesc(imgframe);



function ImgGridX_Callback(hObject, eventdata, handles)
% hObject    handle to ImgGridX (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of ImgGridX as text
%        str2double(get(hObject,'String')) returns contents of ImgGridX as a double


% --- Executes during object creation, after setting all properties.
function ImgGridX_CreateFcn(hObject, eventdata, handles)
% hObject    handle to ImgGridX (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function ImgTlow_Callback(hObject, eventdata, handles)
% hObject    handle to ImgTlow (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of ImgTlow as text
%        str2double(get(hObject,'String')) returns contents of ImgTlow as a double


% --- Executes during object creation, after setting all properties.
function ImgTlow_CreateFcn(hObject, eventdata, handles)
% hObject    handle to ImgTlow (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function ImgThigh_Callback(hObject, eventdata, handles)
% hObject    handle to ImgThigh (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of ImgThigh as text
%        str2double(get(hObject,'String')) returns contents of ImgThigh as a double


% --- Executes during object creation, after setting all properties.
function ImgThigh_CreateFcn(hObject, eventdata, handles)
% hObject    handle to ImgThigh (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in imgSaveBtn.
function imgSaveBtn_Callback(hObject, eventdata, handles)
% hObject    handle to imgSaveBtn (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global bh_info;

fidx = get(handles.DataFilesList,'Value');
[fp,fn,~] = fileparts(bh_info(fidx).filename);

img_fn = fullfile(fp,strcat(fn,'.tiff'));

[img_fn, img_fp] = uiputfile(img_fn,'Save image as...');
if (~isequal(img_fn,0) && ~isequal(img_fp,0))
    if (bh_info(fidx).gridz > 0)
        %write multipage TIFF
        imwrite(bh_info(fidx).img(:,:,1),fullfile(img_fp,img_fn),'tiff');
        for fr_idx = 2:bh_info(fidx).gridz
            imwrite(bh_info(fidx).img(:,:,fr_idx),fullfile(img_fp,img_fn),'tiff','writemode','append');
        end
    else
        imwrite(bh_info(fidx).img,fullfile(img_fp,img_fn),'tiff');
    end
    
end


% --- Executes on selection change in ImgRowTrig.
function ImgRowTrig_Callback(hObject, eventdata, handles)
% hObject    handle to ImgRowTrig (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns ImgRowTrig contents as cell array
%        contents{get(hObject,'Value')} returns selected item from ImgRowTrig


% --- Executes during object creation, after setting all properties.
function ImgRowTrig_CreateFcn(hObject, eventdata, handles)
% hObject    handle to ImgRowTrig (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in ImgSplitChk.
function ImgSplitChk_Callback(hObject, eventdata, handles)
% hObject    handle to ImgSplitChk (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of ImgSplitChk



function ImgSplitLines_Callback(hObject, eventdata, handles)
% hObject    handle to ImgSplitLines (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of ImgSplitLines as text
%        str2double(get(hObject,'String')) returns contents of ImgSplitLines as a double


% --- Executes during object creation, after setting all properties.
function ImgSplitLines_CreateFcn(hObject, eventdata, handles)
% hObject    handle to ImgSplitLines (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function ImgSplitFrame_Callback(hObject, eventdata, handles)
% hObject    handle to ImgSplitFrame (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of ImgSplitFrame as text
%        str2double(get(hObject,'String')) returns contents of ImgSplitFrame as a double


% --- Executes during object creation, after setting all properties.
function ImgSplitFrame_CreateFcn(hObject, eventdata, handles)
% hObject    handle to ImgSplitFrame (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in ImgFrPrev.
function ImgFrPrev_Callback(hObject, eventdata, handles)
% hObject    handle to ImgFrPrev (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

fr = str2double(get(handles.ImgSplitFrame,'string'));
set(handles.ImgSplitFrame,'string',num2str(fr-1,'%d'))

DisplayImage(handles);

% --- Executes on button press in ImgFrNext.
function ImgFrNext_Callback(hObject, eventdata, handles)
% hObject    handle to ImgFrNext (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

fr = str2double(get(handles.ImgSplitFrame,'string'));
set(handles.ImgSplitFrame,'string',num2str(fr+1,'%d'))
DisplayImage(handles);
