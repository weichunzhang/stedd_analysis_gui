% --------------------------------------------------------------------
function varargout = bhcorefunc(varargin)


    if (nargin > 0)
        if ischar(varargin{1}) % INVOKE NAMED SUBFUNCTION OR CALLBACK
            try
                if (nargout)
                    [varargout{1:nargout}] = feval(varargin{:}); % FEVAL switchyard
                else
                    feval(varargin{:}); % FEVAL switchyard
                end
                return;
            catch
                disp(lasterr);
            end
        end
    end

end

%---------------------------------------------
%                   initialize info structure
%------------------------------------------------------------
function info = init_info(nfiles)

    s = struct('filename','','phIdx',[],'mtovIdx',[],'pxIdx',[],'lnIdx',[],'frIdx',[], 'tADC',[],...
            'timebase',25, 'timebins',pow2(12),'gridx',0,'gridy',0,'gridz',0,'img',[],'ltPeriod',[0 25],'ltData',[],...
            'rowIdx',[], 'rowMT',[],'rowTime',0,'rowImg',[],'rowTrace',[]);

    info = repmat(s,[nfiles,1]);
end

%---------------------------------------------
%                   read spc data
%------------------------------------------------------------
function data = bhreaddata(filename, mode)

    data = [];
    
    fid=fopen(filename,'r+');
    if (fid ~= -1)
        fseek(fid,4,-1);

        if (isempty(mode) || strcmp(mode,'uint32'))
            data = uint32(fread(fid,inf,'uint32','ieee-be'));
        elseif (strcmp(mode,'*ubit1'))
            data = fread(fid,[32,inf],'*ubit1','ieee-be')';
        end

        fclose(fid);
    end
end


%------------------------------------------------------------
%                   get data info
%------------------------------------------------------------
function info = bhgetinfo(spcdata, info)

    % -------------- metadata from sdt-file ------------------
    
    %default values
    info.timebase = 25;
    info.timebins = pow2(12);

    [fp,fn,~] = fileparts(info.filename);
    str_p = strfind(fn,'_m');
    fn_sdt = fullfile(fp,strcat(fn(1:str_p-1),'.sdt'));


    if (exist(fn_sdt,'file') == 2 && exist('bfGetReader.m','file') == 2)
         reader = bfGetReader(fn_sdt);
         omeMeta = reader.getMetadataStore();
         glbMeta = reader.getGlobalMetadata();


         tb = glbMeta.get('time base');
         if (~isempty(tb))
             info.timebase = tb;
         end
         tb = glbMeta.get('time bins');
         if (~isempty(tb))
             info.timebins = tb;
         end
    end

    
    % -------------------- info from data -----------------------
    info.phIdx = [];
    %info.mtovIdx = [];
    info.pxIdx = [];
    info.lnIdx = [];
    info.frIdx = [];
    info.tADC = [];
    
    info.img = [];

    if (isa(spcdata,'uint8') && size(spcdata,2) == 32)
        % bit position 1:32
        bINV = 25;
        bMTOV = 26;
        bFLAG = 28;
        bFR = 10;
        bLN = 11;
        bPX = 12;

        info.phIdx = find(spcdata(:,bINV) ~= 1 & spcdata(:,bFLAG) ~= 1); % all valid photons 

        info.pxIdx = find(spcdata(:,bFLAG) == 1 & spcdata(:,bPX) == 1); % pixel marker
        info.lnIdx = find(spcdata(:,bFLAG) == 1 & spcdata(:,bLN) == 1); % line marker
        info.frIdx = find(spcdata(:,bFLAG) == 1 & spcdata(:,bFR) == 1); % frame marker

        info.tADC = pow2(12) - uint16(bi2de([spcdata(:,29:32) spcdata(:,17:24)],'left-msb')) - 1;

    elseif (isa(spcdata,'uint32'))
        % bit interpretation from 0:31, reverse order byte-wise
        bINV = 7;
        bFLAG = 4;
        bMTOV = 6;
        bFR = 22;
        bLN = 21;
        bPX = 20;

        info.phIdx = uint32(find(bitand(spcdata,pow2(bINV) + pow2(bFLAG),'uint32') == 0)); % all valid photons 
        %info.mtovIdx = uint32(find(bitand(spcdata,pow2(bMTOV),'uint32')));
        
        info.pxIdx = uint32(find(bitand(spcdata,pow2(bFLAG),'uint32') & bitand(spcdata,pow2(bPX),'uint32'))); % pixel marker
        info.lnIdx = uint32(find(bitand(spcdata,pow2(bFLAG),'uint32') & bitand(spcdata,pow2(bLN),'uint32'))); % line marker 
        info.frIdx = uint32(find(bitand(spcdata,pow2(bFLAG),'uint32') & bitand(spcdata,pow2(bFR),'uint32'))); % frame marker 
        

        info.tADC = pow2(12) - 1 -...
            bitshift(bitand(spcdata, pow2(4)-1),8) - ...
            bitand(bitshift(spcdata,-8), pow2(8)-1);

    end

end
%---------------------------------------------
%                   get tcspc histogram
%------------------------------------------------------------
function ltData = bhgetlthist(spcinfo)
    if (~isempty(spcinfo.tADC) && ~isempty(spcinfo.phIdx))
        ltData = zeros(spcinfo.timebins, 2);
        ltData(:,1) = (0:spcinfo.timebins-1)*spcinfo.timebase/spcinfo.timebins;
        ltData(:,2) = histc(spcinfo.tADC(spcinfo.phIdx),0:spcinfo.timebins-1);
    end
end


function ltperiod = bhgetltperiod(ltdata, threshold)

    ltperiod = [1 size(ltdata,1)];
    [~, t_max] = max(ltdata(:,2));
    
    ltperiod(1) = find(ltdata(1:t_max,2) < threshold*mean(ltdata(:,2)), 1, 'last');
    ltperiod(2) = t_max + find(ltdata(t_max:end,2) < threshold*mean(ltdata(:,2)), 1, 'first');

end
%-----------------------------------------
% get macro times from data
%-----------------------------------------


function MT = bhmtcalc(spcdata)

    if (isa(spcdata,'uint8') && size(spcdata,2) == 32)
        % bit position 1:32
        bINV = 25;
        bMTOV = 26;
        bFLAG = 28;


        b_inv = uint32(spcdata(:,bINV));
        b_ov = uint32(spcdata(:,bMTOV));
        b_fl = uint32(spcdata(:,bFLAG));

        % macro time value (except cases with INV = 1, MTOV = 1 and FLAG = 0)
        mt_nov = (1 - b_inv.*b_ov).*uint32(bi2de([spcdata(:,13:16) spcdata(:,1:8)],'left-msb'));

        % single overflow MTOV = 1(except cases with INV = 1, MTOV = 1 and FLAG = 0) 
        mt_sov = cumsum((1 - b_inv.*b_ov).*(1-b_fl).*b_ov.*pow2(12));

        % multiple overflows: INV = 1, MTOV = 1 and FLAG = 0
        mt_mov = b_inv.*b_ov.*(1-b_fl).*uint32(bi2de([spcdata(:,29:32) spcdata(:,17:24) spcdata(:,9:16) spcdata(:,1:8)],'left-msb'))*pow2(12);

        % relative macro time between events
        mt_sovf = b_inv.*b_ov.*b_fl.*pow2(12);
        
    elseif (isa(spcdata,'uint32'))
        % bit interpretation from 0:31, reverse order byte-wise
        bINV = 7;
        bMTOV = 6;
        bFLAG = 4;
        
        b_inv = bitand(bitshift(spcdata,-bINV),1,'uint32');
        b_ov = bitand(bitshift(spcdata,-bMTOV),1,'uint32');
        b_fl = bitand(bitshift(spcdata,-bFLAG),1,'uint32');

        d_l = bitshift(bitand(bitshift(spcdata, -16), pow2(4)-1, 'uint32'),8); %calc bits 9:12
        d_h = bitand(bitshift(spcdata,-24), pow2(8)-1, 'uint32'); %calc bits 1:8
 
        % macro time value (except cases with INV = 1, MTOV = 1)
        % d_l = bitshift(bitand(bitshift(spcdata, -16), pow2(8)-1, 'uint32'),8); %calc bits 9:16
        mt_nov = (1 - (b_inv - 1).*b_ov).*(d_l+d_h);
        %mt_nov = (1 - b_inv.*b_ov).*(d_h);
        
        % single overflow MTOV = 1(except cases with INV = 1, MTOV = 1 and FLAG = 0) 
        %mt_sov = cumsum((1 - b_inv.*b_ov).*(1-b_fl).*b_ov.*pow2(12));
        mt_sov = (1 - b_inv.*b_ov).*(1-b_fl).*b_ov.*pow2(12);
        
        % multiple overflows: INV = 1, MTOV = 1 and FLAG = 0
        %mt_mov = b_inv.*b_ov.*(1-b_fl).*(d_l + d_h)*pow2(12);
        %mt_mov = cumsum(b_inv.*b_ov.*(1-b_fl).*(d_l + d_h)*pow2(12));
        mt_mov = b_inv.*b_ov.*(1-b_fl).*(d_l + d_h)*pow2(12);

        % single overflow MTOV = 1 with FLAG = 1 
        mt_sovf = b_inv.*b_ov.*b_fl.*pow2(12);
    end
    
    mt_ov = cumsum(b_ov.*(mt_sov + mt_mov + mt_sovf));
    
    %MT = uint32(mt_nov + mt_sov + mt_mov + mt_sovf);   
    MT = uint32(mt_nov + mt_ov); 
%     MT = MT - MT(1);
%     MT(1) = [];
end


%-----------------------------------------
% find data events of valid photons
%-----------------------------------------
function bph = bhphidx(eventdata, eventtime)

    if (isa(eventdata,'uint32'))
        bINV = 7;
        bMTOV = 6;
        bFLAG = 4;

        b_inv = bitand(bitshift(eventdata,-bINV),1,'uint32');
        b_fl = bitand(bitshift(eventdata,-bFLAG),1,'uint32');
        
        %dph = bitand(eventdata,pow2(bINV)+pow2(bFLAG),'uint32'); % all valid photons 

        tph = pow2(12)-1-...
            bitshift(bitand(eventdata, pow2(4)-1),8) - ...
            bitand(bitshift(eventdata,-8), pow2(8)-1);

        bph = find(b_inv == 0 & b_fl == 0 & tph >= eventtime(1) & tph <= eventtime(2)); 


    end
end


%-----------------------------------------
% get the photon microtime for all photons in the line
%-----------------------------------------
function pht = phmtime(eventtime, phindex, b_reltime)
    
    if (~isempty(phindex))
%         pdc = mat2cell(eventtime, [phindex(1); diff(phindex); length(eventtime) - phindex(end)]);
%         pht = cellfun(@sum, pdc);
        pht = eventtime(phindex)-b_reltime*eventtime(1); %first event is line trigger
    else
        pht = 0;
    end
 

end

%-----------------------------------------
% get the macrotime data of photons in each row
%-----------------------------------------

% function rowMT = bhgetrowmt(data, params, ltrange)
%     rowMT = bhgetrowmt(data, params, ltrange, 1);
% end

function rowMT = bhgetrowmt(data, params, ltrange, b_reltime)

    if (isempty( b_reltime) )
        b_reltime = 1;
    end
    % b_reltime = 0: absolute time; 1: time relative to the row trigger
    
    %remove data frames without photons
    drow = diff(params.rowIdx);
    ddata = data(params.rowIdx(1):params.rowIdx(end)-1);
    
    zp_idx = find(drow == 1);
    cs_drow = cumsum(drow);
    
    ddata(cs_drow(zp_idx)) = [];
    drow(zp_idx) = [];
    
    %arrange data events on rows
    lnCell = mat2cell(ddata, drow);
    
    % get mt times without calculating all overflows in preceding rows
    lnmtCell = cellfun(@bhmtcalc,lnCell,'UniformOutput',0);
        
    %select photons by lifetime range
    tCell = num2cell(repmat(ltrange,[length(lnmtCell),1]),  2);
    lnphCell = cellfun(@bhphidx, lnCell, tCell, 'UniformOutput',0);
    %imgCell = cellfun(@(mt, phidx) mt(phidx), lnmtCell, lnphCell ,'UniformOutput',0);
    
    %get relative time of photons with respect to the trigger time
    rowmtCell = cellfun(@phmtime, lnmtCell, lnphCell, repmat({b_reltime}, size(lnmtCell,1),1),'UniformOutput',0);
    %rowMT contains only lines with photons, 
    %lines without photons are removed and have to be inserted later
    
%     
%     rowIdx = uint32((0:length(params.rowIdx)-1)'*params.rowTime);
%     rowIdx(zp_idx) = [];
%     rowIdx = rowIdx - rowIdx(1) + lnmtCell{1}(1);
%     
%     rowMT{1,1} = rowmtCell;
%     rowMT{1,2} = mat2cell(rowIdx, ones([length(rowIdx),1]));
   rowMT = rowmtCell; 
    
end


%-----------------------------------------
% get the line period microtime 
%-----------------------------------------
function period = bhgetrowperiod(data, lnIdx)

    if (isa(data,'uint32'))
        bINV = 7;
        bMTOV = 6;
        bFLAG = 4;


        data_p = zeros(length(data),3,'uint32');
        data_p(:,1)  = data;
        data_p(lnIdx,2) = 1;

        mtovIdx = uint32(find(bitand(data,pow2(bMTOV),'uint32')));
        data_p(mtovIdx,2) = data_p(mtovIdx,2) + 2;

        data_p(data_p(:,2) == 0,:) = [];
       
        data_p(:,3) = bhmtcalc(data_p(:,1)); %line trigger macrotime
        data_p(data_p(:,2) == 2,:) = [];
       
        %remove outliers 
        %period = mean(diff(data_p(:,3)));
        data_t = double(diff(data_p(:,3)));
        j = find(diff(data_t) > 0, 1, 'last');
        data_t(j:end) = [];
        [mu,sg] = normfit(data_t);
        data_t(data_t < mu-1.5*sg | data_t > mu+1.5*sg) = [];
        
        period = mean(data_t);
        
    end

end


%-----------------------------------------
% calc image from data
%-----------------------------------------

%function img = bhgetimage(data, params, lifetime_range, bTime, binlines)
function img = bhgetimage(data, params, lifetime_range, binlines)

   bRelTime = 1; %time of photons relative to the frame trigger
   bAbsTime = 0; %absolute time of photons
   
    %lnmtCell =  bhgetmtrowdata(data, params);
    
%     %distribute data events by lines
%     lnCell = mat2cell(data(params.rowIdx(1):params.rowIdx(end)-1), diff(params.rowIdx));
% 
%     %remove lines without photons
%     data_per_row = cell2mat(cellfun(@length, lnCell, 'UniformOutput',0));
%     lnCell(data_per_row == 1) = [];
% 
%     % get mt times
%     lnmtCell = cellfun(@bhmtcalc,lnCell,'UniformOutput',0);
%         
%     %select photons by lifetime range
%     tCell = num2cell(repmat(lifetime_range,[length(lnmtCell),1]),  2);
%     lnphCell = cellfun(@bhphidx, lnCell, tCell, 'UniformOutput',0);
%     %imgCell = cellfun(@(mt, phidx) mt(phidx), lnmtCell, lnphCell ,'UniformOutput',0);
%     imgCell = cellfun(@phmtime, lnmtCell, lnphCell ,'UniformOutput',0);
% 
    % get the average line period
    if (isempty(params.rowTime) || params.rowTime == 0)
        params.rowTime = bhgetrowperiod(data(params.rowIdx(1):params.rowIdx(end)-1), params.rowIdx - params.rowIdx(1)+1);
    end
    
    
    grid_dt = params.rowTime/params.gridx;

%     if (bTime == bRelTime) %relative time
        
        if (isempty(params.rowMT))
            params.rowMT = bhgetrowmt(data, params, lifetime_range, bRelTime);
        end
        
        grid = {1:grid_dt:params.gridx*grid_dt+1};
        %bnCell = repmat(grid, length(params.rowMT{1}), 1);
        %phlines = cell2mat(cellfun(@histcounts,params.rowMT,bnCell,'UniformOutput',0)); %bin photons with timebins

        rowmtfull = repmat({[]}, [length(params.rowIdx), 1]);
        rowmtfull(diff(params.rowIdx) ~= 1) = params.rowMT;
        
        params.gridy = floor(length(rowmtfull)/binlines);
        rowmtbin = reshape(rowmtfull(1:params.gridy*binlines), [binlines, params.gridy])';
        
        rowmtcat = repmat({[]},[params.gridy,1]);
        
        for i = 1:params.gridy
            rowmtcat{i} = cat(1, rowmtbin{i, :});
        end
        bnCell = repmat(grid, [params.gridy, 1]);
        phlines = cell2mat(cellfun(@histcounts,rowmtcat,bnCell,'UniformOutput',0)); %bin photons with timebins

        %img  = zeros(params.gridy, params.gridx, 'uint16');
        img = uint16(phlines);
        
%     elseif (bTime == bAbsTime) %absolute time
%         params_t = params;
%         params_t.rowIdx = [1,length(data)];
%         params.rowMT = bhgetrowmt(data, params_t, lifetime_range, bTime);
%         
%         params_t.rowIdx = [1,length(params.rowIdx)];
%         frMT = bhgetrowmt(params.rowIdx, params_t, lifetime_range, bTime);
%         
%         nph = sum(cell2mat(cellfun(@length, params.rowMT,'UniformOutput',0)));
%         
%         phbank = zeros(nph,1);
%         i_s = 1;
%         for i = 1:length(params.rowMT)
%             i_f = i_s + length(params.rowMT{i}) - 1;
%             phbank(i_s:i_f,1) = params.rowMT{i};
%             i_s = i_f + 1;
%         end
%         phbank(phbank==0) = [];
%         
%         
%         
%         nlines = ceil(phbank(end,1)/params.rowTime); 
%         img  = zeros(nlines, params.gridx, 'uint16');
%         
%         t_idx = ceil(phbank/params.rowTime); 
%         
% %         frp = 274.53;
% %         frbank = round(frp*ceil(phbank(1:1000)/frp)) - 251; 
%         
%         a_idx = ceil(mod(phbank,params.rowTime)/grid_dt); 
%         img(t_idx,a_idx) = 1;
%         
%     end
    
end

%-----------------------------------------
% calc image from data with line accumulation
%-----------------------------------------

function img = bhgetimage_acclines(data, params, lifetime_range, nlines)


        img_full = bhgetimage(data, params, lifetime_range);
        
        dim_new = floor(size(img_full,1)/nlines);

        img_rs = reshape(img_full(1:dim_new*nlines, :),[dim_new, nlines, size(img_full,2)]);
        img = squeeze(sum(img_rs,2));
   
end

%--------------------------------------------------------------------------
function bindata = GetBinnedData(data, binpts)

%     linetime = 1e-9*imgdata.timebase*data.rowTime;
%     binlines = floor(bintime/linetime);
%     
    dimBinned = floor(size(data.rowImg,1)/binpts);

    rowImgReshaped = reshape(data.rowImg(1:dimBinned*binpts, :),...
        [dimBinned, binpts, size(data.rowImg,2)]);

    bindata = squeeze(sum(rowImgReshaped,2));





end

% 
% //////////////////////////////////////////////////////////////
% ///    FIFO Data Files (SPC-130(1), SPC-140, SPC-150(1), SPC-830, SPC-160 )
% //////////////////////////////////////////////////////////////
% 
% /*
% 
% The information about the subsequent photons is stored one after another 
%  in the measurement data file. For each photon 4 bytes are used. 
%  The structure of these data is shown in the table below.
% 
%            Bit 7    Bit 6    Bit 5    Bit 4    Bit 3    Bit 2    Bit 1    Bit 0
% -------------------------------------------------------------------------------         
% Byte 0     MT[7]    MT[6]    MT[5]    MT[4]    MT[3]    MT[2]    MT[1]    MT[0]
% Byte 1     R[3]     R[2]     R[1]     R[0]     MT[11]   MT[10]   MT[9]    MT[8]
% Byte 2     ADC[7]   ADC[6]   ADC[5]   ADC[4]   ADC[3]   ADC[2]   ADC[1]   ADC[0]
% Byte 3     INVALID  MTOV     GAP      0        ADC[11]  ADC[10]  ADC[9]   ADC[8]
% 
% ADC[11:0]  ADC Data (reversed Micro Time) 
%            Due to reversed start-stop acquisition mode used in all SPC modules 
%              real Micro Time = 4095 - ADC
% R [3:0]    Routing Signals (inverted). 
%            Bit 3 is not available for early SPC-130/134 modules
% MT[16:0]   Macro Time (50 ns intervals for internal Macro time oscillator, 
%                         for Sync clock see below)
% GAP        1 = Possible recording gap due to FIFO Full. 
%            There may be (and most likely is) a gap in the re-cording preceding
%              this photon.
% MTOV       1 = Macro Timer Overflow. 
%            Since the capacity of the macro timer is limited to 12 bit 
%            it will overflow every 2**12th macro time period. 
%            For the internal macro time clock of 50 ns overflows occur every 204.8 �s. 
%            For synchronisation of the macro-time clock with the SYNC signal 
%              this interval may even be shorter. 
%            Therefore there may even be multiple overflows without any photons detected (see below).
%            The software which processes the data file has to add the times 
%              between overflows to its internal macro time value on each MTOV =1
% INVALID    1 = Data Invalid. All data for this photon except the MTOV bit is invalid. 
%            The INVALID bit is set if the 'Count Enable' bit at the SPC routing connector was '0',
%             i.e. if a router is connected and there is no valid routing information 
%             for this photon.
% 
% The INVALID bit is used to mark photons for which no correct detector channel number, 
%    micro time, or macro time could be determined. 
%    This happens if several detectors in a multi-detector system detected photons within 
%    the response time of the routing electronics, the sum of the TAC output and the dither signal
%    was outside the conversion range of the ADC, or in the (very unlikely) case 
%    that a photon appears within the generation of a macro time overflo entry (see below). 
%    
% The first photon frame (4 bytes) in the .spc file is added by the software and contains information 
% about the Macro Time clock and the number of routing channels used during the measurement:
%   byte 0,1,2    = macro time clock in 0.1 ns units ( for 50ns value 500 is set )
%   byte 3 : bit 7 = 1 ( Invalid photon ),  bits 3-6 = number of routing bits, 
%            bit 2 = 1 file with raw data ( for diagnostic mode only, 
%                      such file contains pure data read from FIFO without special meaning 
%                        of the entries with 'MTOV = 1' and 'INVALID = 1' described below )
%            bit 1 = 1 file with markers ( new SPC-140, SPC-150, see below)
%            bit 0 reserved  
% 
% Internal Macro Time Clock is equal 50ns for all modules.
% The only exception is the newest SPC-140 ( FPGA v. > B0 ) and SPC-150, where it is equal 25 ns.
% 
% Macro Time Overflows
%   Since the capacity of the macro timer is limited to 12 bit it will overflow 
%    at each 2**12-th macro time period. 
%   In most cases the overflow is marked by the 'MTOV' bit of the next valid photon. 
%   However, the internal macro time clock of 50 ns overflows occur every 204.8 �s.
%   For synchronisation of the macro-time clock with the SYNC signal this interval may 
%     even be shorter. 
%   Therefore, it can happen that no valid photon is recorded between two subsequent 
%     macro time overflows. 
%   To enable the processing software to maintain a correct macro time for 
%     the rest of the measurement an entry in the measurement data file is written 
%     if several overflows occurred between two subsequent photons. 
%   This entry is marked by 'MTOV = 1' and 'INVALID = 1' and contains 
%    the number of macro time overflows which occurred since the last photon was recorded.
% 
%   The structure of  this entry is shown below.
% 
%            Bit 7    Bit 6    Bit 5    Bit 4    Bit 3    Bit 2    Bit 1    Bit 0
% -------------------------------------------------------------------------------         
% Byte 0    CNT[7]   CNT[6]   CNT[5]   CNT[4]   CNT[3]   CNT[2]   CNT[1]   CNT[0]
% Byte 1    CNT[15]  CNT[14]  CNT[13]  CNT[12]  CNT[11]  CNT[10]  CNT[9]   CNT[8]
% Byte 2    CNT[23]  CNT[23]  CNT[21]  CNT[20]  CNT[19]  CNT[18]  CNT[17]  CNT[16]
% Byte 3    INVALID  MTOV     0        0        CNT[27]  CNT[26]  CNT[25]  CNT[24]
% 
% CNT[27:0] Number of macro time overflows which occurred without recording photons
% 
% when programming, the photon frame is treated as one 32-bit word
% 
% */
    
% The structure of the marker frame is shown in the table below.
% 
%            Bit 7    Bit 6    Bit 5    Bit 4    Bit 3    Bit 2    Bit 1    Bit 0
% -------------------------------------------------------------------------------         
% Byte 0     MT[7]    MT[6]    MT[5]    MT[4]    MT[3]    MT[2]    MT[1]    MT[0]
% Byte 1     M[3]     M[2]     M[1]     M[0]     MT[11]   MT[10]   MT[9]    MT[8]
% Byte 2     x        x        x        x        x        x        x        x     
% Byte 3     1        MTOV     GAP      1        x        x        x        x     
% 
% for SPC-160
% Byte 2     INT[7]   INT[6]   INT[5]   INT[4]   INT[3]   INT[2]   INT[1]   INT[0]
% Byte 3     1        MTOV     GAP      1        INT[11]  INT[10]  INT[9]   INT[8]
% 
% x - not used   
% M [3:0]   Marker's type:
%           M[0] = 1   Marker0 = Pixel Clock Marker
%           M[1] = 1   Marker1 = Line  Clock Marker
%           M[2] = 1   Marker2 = Frame Clock Marker
%           M[3] = 1   Marker3, not used in FIFO_IMAGE mode, user defined
% 
% It can happen that in the same frame more than 1 marker M[] bits are set.
% It would mean that different external clock pulses appeared in the current
%   macro time period
% */



